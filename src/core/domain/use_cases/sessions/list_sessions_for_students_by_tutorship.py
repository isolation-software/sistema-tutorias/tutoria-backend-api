from src.config import LoggerFactory
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class GetLastSessionsForStudentsByIndividualTutorshipUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_student=None, repo_session=None):
        super().__init__(errors, req)
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        self.repo_session = repo_session or self.RepoFactory.SessionRepo(session=self.repo_student.session)
        self.id_tutorship = req.id_tutorship

    @catch_errors
    def execute(self):
        students_and_sessions = self.repo_session.get_last_sessions_of_students_in_tutorship(self.id_tutorship)
        self.res = Response(students_and_sessions=students_and_sessions)
        self.success = GetSessionsForStudentsByTutorshipSuccess


class GetSessionsForStudentsByTutorshipSuccess(Success):
    code = info_success_logic.SESSIONS_FOR_STUDENTS_BY_TUTORSHIP_SEARCH_OK_CODE
    message = info_success_logic.SESSIONS_FOR_STUDENTS_BY_TUTORSHIP_SEARCH_OK_MESSAGE
