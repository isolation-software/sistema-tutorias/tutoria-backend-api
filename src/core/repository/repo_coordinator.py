from sqlalchemy import or_

from src.config import LoggerFactory
from src.core.repository import UserRepo
from src.core.repository.common import Repository
from src.core.repository.database.models import CoordinatorModel, UserModel
from src.core.repository.errors.handler import catch_repo_errors
from src.core.repository.utils import paginate
from src.helpers.formater import no_double_space


class CoordinatorRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def save_coordinator_with_user(self, coordinator):
        repo_user = UserRepo()
        user = repo_user.save_user(coordinator.user)

        row_coordinator = CoordinatorModel(id_user=user.idx)
        self.session.add(row_coordinator)
        self.session.commit()
        self.session.refresh(row_coordinator)
        coordinator.idx = row_coordinator.id_coordinator
        return coordinator

    @catch_repo_errors
    def save_coordinator_without_user(self, coordinator):
        row_user = self.session.query(UserModel).filter(UserModel.email == coordinator.user.email).one()
        row_coordinator = CoordinatorModel(id_user=row_user.id_user)
        self.session.add(row_coordinator)
        self.session.commit()
        self.session.close()

    @catch_repo_errors
    def list_coordinators(self, params):
        self.logger.debug("Iniciando la búsqueda de coordinadores en repositorio")
        search, page, pagesize = params
        search = "%{}%".format(no_double_space(search))
        rows = paginate(query=self.session.query(CoordinatorModel)
                                    .join(UserModel)
                                    .filter(or_(
                                        UserModel.names.ilike(search),
                                        UserModel.father_lastname.ilike(search),
                                        UserModel.mother_lastname.ilike(search),
                                        UserModel.code.ilike(search)))
                                    .order_by(UserModel.register_date.desc()),
                        page=page, pagesize=pagesize)\
            .all()
        self.logger.debug("Búsqueda de coordinadores terminada en repositorio")
        self.logger.debug(f"Resultados: {rows}")
        return [r.to_entity() for r in rows]

    @catch_repo_errors
    def read_data_coordinator_id(self, id_coord):
        row_coordinator = self.session.query(CoordinatorModel).join(UserModel).filter(
            CoordinatorModel.id_coordinator == id_coord
        ).first()
        return row_coordinator.to_entity()

    @catch_repo_errors
    def update_coordinator(self, coordinator):
        row_user = self.session.query(UserModel).filter(UserModel.email == coordinator.user.email).one()
        row_user.names = coordinator.user.names if coordinator.user.names else row_user.names
        row_user.father_lastname = coordinator.user.father_lastname if coordinator.user.father_lastname \
            else row_user.father_lastname
        row_user.mother_lastname = coordinator.user.mother_lastname if coordinator.user.mother_lastname \
            else row_user.mother_lastname
        row_user.phone_number = coordinator.user.phone_number if coordinator.user.phone_number \
            else row_user.phone_number
        # row_user.hash_password = coordinator.user.hash_password if coordinator.user.hash_password \
        # else row_user.hash_password
        # row_user.hash_salt = coordinator.user.hash_salt if coordinator.user.hash_salt \
        # else row_user.hash_salt
        self.session.commit()
        return coordinator
