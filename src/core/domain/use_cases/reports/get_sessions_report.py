from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.core.repository.report import Report
from src.helpers import info_success_logic


class GetSessionsUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_session=None, repo_tutorship=None,repo_unit=None):
        super().__init__(errors, req)

        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()

        self.start_date = req.start_date
        self.end_date = req.end_date
        self.unit = req.unit

    @catch_errors
    def execute(self):
        tutorships = self.repo_tutorship.get_tutorships_by_unit(self.unit)
        unidad = self.repo_unit.get_unit_by_id(self.unit.idx)

        cant_atendida = []
        cant_cancelada = []
        for a in range(len(tutorships)):
            aux1, aux2 = self.repo_session.get_sessions_by_range(self.start_date, self.end_date, tutorships[a])
            cant_atendida.append(aux1)
            cant_cancelada.append(aux2)

        #stream = Report.create_report_session(tutorships, cant_atendida, cant_cancelada)
        #self.res = Response(stream=stream)
        self.res = Response(tutorships=tutorships,cant_atendida= cant_atendida,cant_cancelada= cant_cancelada,
                            unidad=unidad)
        self.success = GetSessionsSuccess


class GetSessionsSuccess(Success):
    code = info_success_logic.SEARCH_TUTORSHIP_OK_CODE
    message = info_success_logic.SEARCH_TUTORSHIP_OK_MESSAGE
