from flask_restful import Resource
from flask import request

from src.app.auth.handler import jwt_required
from src.controllers import TutorshipController
from src.controllers.session.controller_session import SessionController
from src.helpers import constants


class SessionsResource(Resource):
    @jwt_required
    def post(self):
        json = request.get_json()
        response = SessionController.register_session(json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def get(self):
        id_tutor = request.args.get('idTutor', None)
        search = request.args.get('search', '')
        id_student = request.args.get('idAlumno', None)
        id_tutorship = request.args.get('idTutoria', None)

        if id_tutor and search:
            response = TutorshipController.list_tutorship_sessions_by_tutor(id_tutor, search)
        elif id_student and id_tutorship:
            response = SessionController.list_sessions_for_student(id_student, id_tutorship)
        elif id_student and id_tutor:
            response = SessionController.list_sessions_by_student_and_tutor(id_student, id_tutor)
        else:
            raise NotImplementedError
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class SessionResource(Resource):

    @jwt_required
    def get(self, id_session=None):
        response = SessionController.request_student_data_from_session(id_session)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def put(self, id_session=None):
        json = request.get_json()
        response = SessionController.update_session(id_session, json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class SessionStatesResource(Resource):
    @jwt_required
    def get(self):
        response = SessionController.request_session_states()
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class SessionsIndividualTutorshipResource(Resource):
    @jwt_required
    def get(self, id_tutorship):
        response = SessionController.request_sessions_by_tutorship_for_students(id_tutorship)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class SessionsIndividualStudentTutorshipResource(Resource):
    @jwt_required
    def get(self, id_tutorship, id_student):
        response = SessionController.request_sessions_by_tutorship_by_student(id_tutorship, id_student)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class AppointmentRecordResource(Resource):
    @jwt_required
    def get(self):
        id_tutor = request.args.get('idTutor', None)
        search = request.args.get('search', '')

        response = TutorshipController.list_tutorship_sessions_by_tutor(id_tutor, search, show_all=True)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class SessionIndicatorResource(Resource):
    def post(self):
        data = request.get_json()
        response = SessionController.register_indicator_of_session(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST
