import os
import openpyxl
from flask import make_response
from openpyxl.writer.excel import save_virtual_workbook

from src.config import LoggerFactory
from src.config.config_general import ReportFiles


class ReportFactory:
    logger  = LoggerFactory.get_logger(__name__)
    sql_issues_moment_tutorship = None
    sql_issues_tutorships_without_moment = None
    sql_session_frequency_moment_tutorship = None
    sql_session_frequency_without_moment = None
    sql_tutor_satisfaction = None
    sql_tutor_satisfaction_moment_tutorship = None
    sql_tutoship_sessions = None

    @classmethod
    def get_workbook(cls, filename):
        xlsx_path = os.path.join(ReportFiles.PATH_REPORT, filename)
        return openpyxl.load_workbook(xlsx_path)

    @classmethod
    def make_response(cls, workbook, filename="reporte.xlsx"):
        virtual_wb = save_virtual_workbook(workbook)
        output = make_response(virtual_wb)
        output.headers["Content-Disposition"] = f"attachment; filename={filename}"
        output.headers["Content-type"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        return output

    @classmethod
    def _get_content(cls, path):
        with open(path, mode='r', encoding='UTF-8') as content_file:
            return content_file.read()

    @classmethod
    def config(cls):
        sql = ReportFiles.PATH_REPORT_SQL
        cls.sql_issues_moment_tutorship = cls._get_content(os.path.join(sql, "issues_moment_tutorship.sql"))
        cls.sql_issues_tutorships_without_moment = cls._get_content(os.path.join(sql, "issues_tutorships_without_moment.sql"))
        cls.sql_session_frequency_moment_tutorship = cls._get_content(os.path.join(sql, "session_frequency_moment_tutorship.sql"))
        cls.sql_session_frequency_without_moment = cls._get_content(os.path.join(sql, "session_frequency_tutorship_without_moment.sql"))
        cls.sql_tutor_satisfaction = cls._get_content(os.path.join(sql, "tutor_satisfaction.sql"))
        cls.sql_tutor_satisfaction_moment_tutorship = cls._get_content(os.path.join(sql, "tutor_satisfaction_moment_tutorship.sql"))
        cls.sql_tutoship_sessions = cls._get_content(os.path.join(sql, "tutorship_sessions.sql"))


ReportFactory.config()