import datetime
import jwt

from src.app.auth.handler import UserToken
from src.config import LoggerFactory, FlaskConfig
from src.core.domain.entities import User
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import UserNotFoundInSystemError, UnauthenticatedUserAccount, IncorrectPassword
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import constants, info_success_logic
from src.helpers.security import check_password, hash_password
from src.helpers.serializer import id_to_str


class ValidateLoginUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_user=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo(session=self.repo_user.session)
        self.email = req.email
        self.password = req.password

    @catch_errors
    def execute(self):
        self.logger.info(f'Inicio de validación de login con correo {self.email}')
        user, hashed, salt = self.repo_user.read_user_by_email_if_exists(User(email=self.email), with_password=True)
        if not user:
            raise UserNotFoundInSystemError(email=self.email)
        if not user.is_activate:
            raise UnauthenticatedUserAccount(email=self.email)
        else:
            is_correct_password = check_password(self.password, hashed, salt)
            if is_correct_password:
                roles = self.repo_user.read_roles(user.idx)
                id_admin       = id_to_str(roles[constants.POSITION_ADMIN].idx) if roles[constants.POSITION_ADMIN] else None
                id_coordinator = id_to_str(roles[constants.POSITION_COORD].idx) if roles[constants.POSITION_COORD] else None
                id_tutor       = id_to_str(roles[constants.POSITION_TUTOR].idx) if roles[constants.POSITION_TUTOR] else None
                id_student     = id_to_str(roles[constants.POSITION_STUDENT].idx) if roles[constants.POSITION_STUDENT] else None
                coord_units = None
                if roles[constants.POSITION_COORD]:
                    coord_units = self.repo_unit.list_units_by_coordinator_id(roles[constants.POSITION_COORD].idx)
                ids_unit = [id_to_str(cur.idx) for cur in coord_units] if coord_units else None
                token = UserToken(
                    names=user.fullname(),
                    email=user.email,
                    id_admin=id_admin,
                    id_coordinator=id_coordinator,
                    id_tutor=id_tutor,
                    id_student=id_student,
                    ids_unit=ids_unit
                )
                token.update_expiration_time()
                self.logger.debug(f'Token enviado: {token.encode_to_utf8()}')
                self.res = Response(token=token.encode_to_utf8())
                self.success = ValidateLoginSuccess
            else:
                raise IncorrectPassword


class ValidateLoginSuccess(Success):
    code = info_success_logic.VALIDATE_LOGIN_OK_CODE
    message = info_success_logic.VALIDATE_LOGIN_OK_MESSAGE
