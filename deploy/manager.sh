#!/usr/bin/env bash
WORKDIR=/home/ubuntu
USERNAME=$1
TOKEN_CRED=$2
TOKEN_BACK=$3

function killback {
    echo "[UPDATE]: KILLALL - matando los workers de gunicorn en tutoria-backend"
    cd ${WORKDIR}/sistema-tutoria/tutoria-backend
    pkill gunicorn
}

function updatecredential {
    # update dependencies and script
    echo "[UPDATE]: CREDENTIALS - actualizando repositorio de tutoria-credentials"
    cd ${WORKDIR}/sistema-tutoria/tutoria-credentials
    git reset && git pull origin master
    echo "[UPDATE]: CREDENTIALS - actualizando dependencias pip de tutoria-credentials"
    /usr/local/bin/pipenv install

    # update config.yml file
    echo "[UPDATE]: CREDENTIALS - copiando archivo config.yml"
    rm -rf config.yml
    cp ${WORKDIR}/config.yml .

    # run script
    echo "[UPDATE]: CREDENTIALS - corriendo el script origin.py"
    /usr/local/bin/pipenv run python origin.py
    cd ${WORKDIR}
}

function updatebackend {
    echo "[UPDATE]: BACKEND - actualizando repositorio de tutoria-backend"
    cd ${WORKDIR}/sistema-tutoria/tutoria-backend
    git reset && git pull origin master

    echo "[UPDATE]: BACKEND - actualizando dependencias pip de tutoria-backend"
    /usr/local/bin/pipenv install
    cd ${WORKDIR}
}

function update {
    echo "[UPDATE]: INICIANDO UPDATE"
    updatecredential
    killback
    updatebackend
}

function clonerepo {
    echo "[MANAGER]: CLONE - inicio del proceso de clonacion"
    # ARCHIVO PARA COPIA DEL PROYECTO EN EL SERVIDOR JUNTO A ARCHIVOS DE CONFIGURACION
    echo "[MANAGER]: CLONE - creando carpeta sistema-tutoria"
    cd ${WORKDIR}
    mkdir sistema-tutoria
    cd    sistema-tutoria
    # preparando credenciales y backend con token de gitlab
    echo "[MANAGER]: CLONE - clonando repositorio tutoria-credentials"
    git clone https://${USERNAME}:${TOKEN_CRED}@gitlab.com/isolation-software/sistema-tutorias/tutoria-credentials.git "tutoria-credentials"

    echo "[MANAGER]: CLONE - clonando repositorio tutoria-backend"
    git clone https://${USERNAME}:${TOKEN_BACK}@gitlab.com/isolation-software/sistema-tutorias/tutoria-backend-api.git "tutoria-backend"

    cd ${WORKDIR}
    echo "[MANAGER]: CLONE - fin del proceso de clonacion"
}

function updaterepo {
    echo "[MANAGER]: UPDATE REPO - ejecutando update.sh"
    update
}

function run {
    echo "[MANAGER]: RUN - ejecutando run.sh"
    cd ${WORKDIR}/sistema-tutoria/tutoria-backend
    /usr/local/bin/pipenv run bash run.sh
    echo "[MANAGER]: RUN - run.sh ejecutado"
    cd ${WORKDIR}
}

function check {
    DIR_BACK="${WORKDIR}/sistema-tutoria/tutoria-backend"
    DIR_CRED="${WORKDIR}/sistema-tutoria/tutoria-credentials"

    echo "[MANAGER]: INICIANDO MANAGER"
    echo "[MANAGER]: buscando carpetas de los repositorios"
    if [[ -d "${DIR_BACK}" ]] && [[ -d "${DIR_CRED}" ]]
    then
        echo "[MANAGER]: repositorios encontrados"
        updaterepo
    else
        echo "[MANAGER]: repositorios no encontrados"
        clonerepo
        updaterepo
    fi
}

function main {
    check
    run
}

main
