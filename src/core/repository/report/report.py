from openpyxl.chart import BarChart, Reference
from openpyxl import Workbook
import tempfile


class Report:

    @classmethod
    def create_report_session(self, tutorships, cant_atendido, cant_cancelado):
        wb = Workbook()
        ws = wb.active
        cant = 0
        #Example

        ws.append(["Tutoria", "Atendidas", "Canceladas"])
        for a in range(len(tutorships)):
            ws.append([tutorships[a].name, cant_atendido[a], cant_cancelado[a]])
            cant = cant + 1

        values = Reference(ws, min_col=2, min_row=2, max_col=3, max_row=cant + 1)
        chart = BarChart()
        chart.add_data(values)
        cats = Reference(ws, min_col=1, min_row=2, max_row=cant + 1)
        chart.set_categories(cats)
        ws.add_chart(chart, "F02")

        with tempfile.NamedTemporaryFile() as tmp:
            wb.save(tmp.name)
            tmp.seek(0)
            stream = tmp.read()

        return stream


