from sqlalchemy import and_

from src.config import LoggerFactory
from src.core.domain.entities.unit_action_plan import UnitActionPlan
from src.core.repository.common import Repository
from src.core.repository.database.models.model_action_plan import ActionPlanModel
from src.core.repository.errors.handler import catch_repo_errors


class ActionPlanRepo(Repository):

    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def save_action_plan(self, id_sessions_serie, action_plan):
        self.logger.debug("Repo - Iniciando actualización de plan de acción")
        self.session.query(ActionPlanModel).filter(ActionPlanModel.id_session_set == id_sessions_serie).delete()
        action_plan_rows = [ActionPlanModel(
                description=ap.description,
                state=ap.state,
                id_session_set=id_sessions_serie
            ) for ap in action_plan.accion_units]
        self.session.bulk_save_objects(action_plan_rows)
        self.session.commit()
        self.logger.debug("Repo - Terminando actualización de plan de acción")

    def read_action_plan(self, id_sessions_serie):
        self.logger.debug(f"Repo - iniciando búsqueda de plan de acción por serie de sesión: id => {id_sessions_serie}")
        rows_action_plan = self.session\
            .query(ActionPlanModel)\
            .filter(ActionPlanModel.id_session_set == id_sessions_serie)\
            .all()
        action_plan = [self._build_action_plan(sr) for sr in rows_action_plan]
        return action_plan

    def has_valid_action_plan(self, id_sessions_serie):
        self.logger.debug(f"Repo - iniciando búsqueda de plan de acción por serie de sesión: id => {id_sessions_serie}")
        rows_action_plan = self.session\
            .query(ActionPlanModel)\
            .filter(and_(ActionPlanModel.id_session_set == id_sessions_serie, ActionPlanModel.state == False))\
            .all()
        action_plan = [self._build_action_plan(sr) for sr in rows_action_plan]
        return action_plan

    @staticmethod
    def _build_action_plan(sr):
        unit_action_plan = UnitActionPlan()
        unit_action_plan.idx = sr.id_action_plan
        unit_action_plan.description = sr.description
        unit_action_plan.state = sr.state
        return unit_action_plan
