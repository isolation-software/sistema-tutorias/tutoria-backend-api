from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class GetUniversityUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_university=None):
        super().__init__(errors, req)
        self.repo_university = repo_university or self.RepoFactory.UniversityRepo()

    @catch_errors
    def execute(self):
        university = self.repo_university.get_university()
        self.res = Response(university=university)
        self.success = GetUniversitySuccess


class GetUniversitySuccess(Success):
    code = info_success_logic.UNIVERSITY_SEARCH_OK_CODE
    message = info_success_logic.UNIVERSITY_SEARCH_OK_MESSAGE
