import uuid

import pytest

from src.core.domain.entities.session import Session


def test_does_not_require_idx_session_entity():
    session = Session()
    assert session.idx is None, "Constructor de clase Session está creando idx a los objetos," \
                                "constructor no debe crear idx"


@pytest.mark.skip(reason="Plan de acción ya no necesita de un idx")
def test_comparison_session_same_idx():
    idx = uuid.uuid4()
    origin = Session(idx=idx)
    copy   = Session(idx=idx)
    assert origin == copy, "Método comparación no implementado correctamente"


def test_comparison_method_session_different_idx():
    idx_origin = uuid.uuid4()
    idx_copy   = uuid.uuid4()
    origin = Session(idx=idx_origin)
    copy   = Session(idx=idx_copy)
    assert origin != copy, "Método comparación no implementado correctamente"
