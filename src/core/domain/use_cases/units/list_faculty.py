from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ListFacultiesUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()

        self.search = req.search
        self.limit = req.limit

    @catch_errors
    def execute(self):
        faculties = self.repo_unit.read_faculties(self.search, self.limit)
        self.res = Response(faculties=faculties)
