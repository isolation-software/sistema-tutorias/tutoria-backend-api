from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import UniversityModel
from src.core.repository.errors.handler import catch_repo_errors


class UniversityRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def register_university(self, university):
        row_university = UniversityModel(name=university.name,
                                         domain=university.domain,
                                         logo=university.logo)
        self.session.add(row_university)
        self.session.commit()
        self.session.refresh(row_university)
        university.idx = row_university.id_university

        return university

    @catch_repo_errors
    def update_university(self, university):
        row_university = self.session.query(UniversityModel).one()
        row_university.name = university.name if university.name else row_university.name
        row_university.domain = university.domain if university.domain else row_university.domain
        row_university.logo = university.logo if university.logo else row_university.logo

        self.session.commit()

    @catch_repo_errors
    def get_university(self):
        row_univ = self.session.query(UniversityModel).one()
        return row_univ.to_entity()
