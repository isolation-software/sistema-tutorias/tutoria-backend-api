SELECT DISTINCT
        MU.code,
        MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names AS NAMES,
       COALESCE(A.ATENDIDAS,0) AS ATENDIDAS,COALESCE(B.RESPONDIDAS,0) AS RESPONDIDAS,
       COALESCE(C.PROMEDIO_SATISFACCION,0) AS PROM_SATISFACCION
FROM "MT_SESSION" MS
INNER JOIN "MT_SESSION_SET" MSS ON MS.id_session_set = MSS.id_session_set
INNER JOIN "MT_TUTORSHIP" MTU ON MSS.id_tutorship = MTU.id_tutorship
INNER JOIN "MT_TUTOR_PER_UNIT" MTPU on MS.id_tutor_per_unit = MTPU.id_tutor_per_unit
INNER JOIN "MT_TUTOR" MT on MTPU.id_tutor = MT.id_tutor
INNER JOIN "MT_USER" MU on MT.id_user = MU.id_user
LEFT OUTER JOIN (
    SELECT MU.code, MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names AS NAMES, COUNT(*) AS ATENDIDAS
    FROM "MT_SESSION" MS
    INNER JOIN "MT_SESSION_SET" MSS ON MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MTU ON MSS.id_tutorship = MTU.id_tutorship
    INNER JOIN "MT_TUTOR_PER_UNIT" MTPU on MS.id_tutor_per_unit = MTPU.id_tutor_per_unit
    INNER JOIN "MT_TUTOR" MT on MTPU.id_tutor = MT.id_tutor
    INNER JOIN "MT_USER" MU on MT.id_user = MU.id_user
    WHERE MTU.id_unit = :id_unit AND --OR M.id_father_unit = 'd655b1b2-bff1-4a70-bfe2-4d0cc08bbbf4') AND
          MS.state = 'Atendido'
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
    GROUP BY MU.code, MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names
) A ON MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names = A.NAMES
LEFT OUTER JOIN (
    SELECT MU.code, MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names AS NAMES, COUNT(*) AS RESPONDIDAS
    FROM "MT_SESSION" MS
    INNER JOIN "MT_INDICATOR" MI ON MS.id_session = MI.id_session
    INNER JOIN "MT_SESSION_SET" MSS ON MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MTU ON MSS.id_tutorship = MTU.id_tutorship
    INNER JOIN "MT_TUTOR_PER_UNIT" MTPU on MS.id_tutor_per_unit = MTPU.id_tutor_per_unit
    INNER JOIN "MT_TUTOR" MT on MTPU.id_tutor = MT.id_tutor
    INNER JOIN "MT_USER" MU on MT.id_user = MU.id_user
    WHERE MTU.id_unit = :id_unit AND --OR M.id_father_unit = 'd655b1b2-bff1-4a70-bfe2-4d0cc08bbbf4')AND
    MS.state = 'Atendido'
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
    GROUP BY MU.code, MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names
) B ON MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names = B.NAMES
LEFT OUTER JOIN (
    SELECT A.code AS CODE, A.NAMES AS NAMES, A.PROMEDIO_INDICADOR1 AS PROMEDIO_SATISFACCION
    FROM
    (SELECT  MU.code, MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names AS NAMES,
            AVG(MI.indicator_1)::FLOAT AS PROMEDIO_INDICADOR1
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS ON MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MTU ON MSS.id_tutorship = MTU.id_tutorship
    INNER JOIN "MT_TUTOR_PER_UNIT" MTPU on MS.id_tutor_per_unit = MTPU.id_tutor_per_unit
    INNER JOIN "MT_TUTOR" MT on MTPU.id_tutor = MT.id_tutor
    INNER JOIN "MT_USER" MU on MT.id_user = MU.id_user
    INNER JOIN "MT_UNIT" M on MTPU.id_unit = M.id_unit
    WHERE MTPU.id_unit = :id_unit
    AND MS.state = 'Atendido'
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
    GROUP BY MU.code, MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names) A
    )C ON MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names = C.NAMES
WHERE MTPU.id_unit = :id_unit
AND MS.state = 'Atendido'
AND MS.start_time>=:start_time AND MS.start_time<=:end_time
UNION ALL
SELECT DISTINCT  M.code, M.father_lastname || ' ' || M.mother_lastname || ', ' || M.names AS NAMES,
       0 AS ATENDIDAS, 0 AS RESPONDIDAS, 0 AS PROM_SATISFACCION
FROM "MT_TUTOR_PER_UNIT" MTPU
INNER JOIN "MT_TUTOR" MTU ON MTPU.id_tutor = MTU.id_tutor
INNER JOIN "MT_USER" M ON MTU.id_user = M.id_user
INNER JOIN "MT_TUTORSHIP" MT on MTPU.id_unit = MT.id_unit
INNER JOIN "MT_SESSION_SET" MSS on MT.id_tutorship = MSS.id_tutorship
LEFT OUTER JOIN "MT_SESSION" MS on MSS.id_session_set = MS.id_session_set
WHERE M.father_lastname || ' ' || M.mother_lastname || ', ' || M.names NOT IN (
    select DISTINCT MU.father_lastname || ' ' || MU.mother_lastname || ', ' || MU.names AS NAMES
    FROM "MT_SESSION" MS
    INNER JOIN "MT_SESSION_SET" MSS ON MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MTU ON MSS.id_tutorship = MTU.id_tutorship
    INNER JOIN "MT_TUTOR_PER_UNIT" MTPU on MS.id_tutor_per_unit = MTPU.id_tutor_per_unit
    INNER JOIN "MT_TUTOR" MT on MTPU.id_tutor = MT.id_tutor
    INNER JOIN "MT_USER" MU on MT.id_user = MU.id_user
    WHERE MTPU.id_unit = :id_unit
    AND MS.state = 'Atendido'
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
    )
    AND  MTPU.id_unit = :id_unit
    AND (MS.id_session IS NULL OR MS.state <> 'Atendido')
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time;
