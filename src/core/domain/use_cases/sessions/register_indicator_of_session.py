import jwt

from src.config import FlaskConfig
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import TokenRegisterInvalid, TokenRegisterExpired, IndicatorAlreadyRegister
from src.core.domain.use_cases.common import UseCase, Success, Response
from src.helpers import info_success_logic


class RegisterIndicatorOfSessionUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_indicator=None):
        super().__init__(errors, req)
        self.user = None
        self.repo_indicator = repo_indicator or self.RepoFactory.IndicatorsRepo()
        self.token = req.token
        self.indicators = req.indicators

    @catch_errors
    def execute(self):
        try:
            decoded = jwt.decode(self.token, FlaskConfig.SECRET_KEY)
        except jwt.DecodeError:
            raise TokenRegisterInvalid
        except jwt.ExpiredSignatureError:
            raise TokenRegisterExpired
        id_sesion = decoded['idSesion']
        indicator = self.repo_indicator.get_indicator_by_session(id_sesion)
        if indicator is None:
            indicators_saved = self.repo_indicator.register_indicators(id_sesion, self.indicators)
        else:
            raise IndicatorAlreadyRegister
        self.res = Response(indicators_updated=indicators_saved)
        self.success = RegisterIndicatorsSuccess


class RegisterIndicatorsSuccess(Success):
    code = info_success_logic.REGISTER_INDICATORS_OK_CODE
    message = info_success_logic.REGISTER_INDICATORS_OK_MESSAGE
