from src.core import repository
from src.helpers import constants
from src.helpers.dynamic_class import DynamicClass


class UseCase:
    """
        Class that manages the execution of a business logic part.
        - When instanciated an object, request object must be decomposed into
          attributes in constructor class (in the class that implements UseCase class)
        - If an error occurs during execution, it's kept it in errors list.
        - During the execution, if all goes ok, it creates a response object
          and keep it in response attribute.
        - Every use-case uses repository module as a factory to persist data.
          It must be used to instantiate repositories that a use-case could need it
        - If an execute finished successfully, it's necessary to declare the success object
          that specifies the code and message that will be sent to the user.
    """
    RepoFactory = repository

    def __init__(self, errors=None, req=None):
        self.errors = errors or []
        self.req = req
        self.res = None
        self.success = None

    def execute(self):
        raise NotImplementedError("Caso de uso no implementado")

    def has_errors(self):
        return len(self.errors) != constants.EMPTY_LIST

    def get_last_error(self):
        return self.errors[constants.LAST_ITEM_INDEX] if self.has_errors() else None

    def get_success(self):
        return self.success

    def close_all(self):
        for attr in dir(self):
            if attr.find("repo_") == 0:
                self.__getattribute__(attr).close()


class Request(DynamicClass):
    pass


class Response(DynamicClass):
    pass


class Success:
    code = None
    message = None
