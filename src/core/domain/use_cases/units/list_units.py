from src.config import LoggerFactory
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ListUnitsUseCase(UseCase):

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()
        self.tutor = req.tutor

    @catch_errors
    def execute(self):
        if self.tutor:
            units = self.repo_unit.list_units_per_tutor(self.tutor)
        else:
            units = self.repo_unit.read_units()
        self.res = Response(units=units)
