from uuid import uuid4

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import Student
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class StudentModel(Base):
    __tablename__ = cons.TABLE_NAME_STUDENT

    id_student = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_user = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_USER}.{cons.TABLE_ID_USER}"))
    id_unit = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_UNIT}.{cons.TABLE_ID_UNIT}"))

    user = relationship("UserModel")
    unit = relationship("UnitModel")

    def to_entity(self):
        """
        Método que convierte el objeto StudentModel a un objeto entidad Student.
        No se está tomando en cuenta los siguientes atributos:
            - unit (objeto unidad de la entidad alumno)
        :return: Student object
        """
        return Student(
            idx=self.id_student,
            user=self.user.to_entity(),
            unit=self.unit.to_entity()
        )
