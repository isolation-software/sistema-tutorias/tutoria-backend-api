from sqlalchemy import text

from src.config import EngineFactory
from src.config.config_report import ReportFactory
from src.core.repository.common import Repository
from src.core.repository.errors import catch_repo_errors


class ReportRepo(Repository):

    @catch_repo_errors
    def list_issues_moment_tutorship(self, id_unit, start_date, end_date):
        s = text(ReportFactory.sql_issues_moment_tutorship)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_date, end_time=end_date).fetchall()
        return result

    @catch_repo_errors
    def list_issues_tutorships_without_moment(self, id_unit, start_date, end_date):
        s = text(ReportFactory.sql_issues_tutorships_without_moment)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_date, end_time=end_date).fetchall()
        return result

    @catch_repo_errors
    def list_sessions_frequency_moment_tutorship(self, id_unit, start_date, end_date):
        s = text(ReportFactory.sql_session_frequency_moment_tutorship)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_date, end_time=end_date).fetchall()
        return result

    @catch_repo_errors
    def list_sessions_frequency_tutorship_without_moment(self, id_unit, start_date, end_date):
        s = text(ReportFactory.sql_session_frequency_without_moment)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_date, end_time=end_date).fetchall()
        return result

    @catch_repo_errors
    def list_tutors_satisfactions(self, id_unit, start_date, end_date):
        s = text(ReportFactory.sql_tutor_satisfaction)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_date, end_time=end_date).fetchall()
        return result

    @catch_repo_errors
    def list_tutor_satisfaction_moment_tutorship(self, id_unit, start_date, end_date):
        s = text(ReportFactory.sql_tutor_satisfaction_moment_tutorship)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_date, end_time=end_date).fetchall()
        return result

    @catch_repo_errors
    def list_tutorship_sessions(self, id_unit, start_time, end_time):
        s = text(ReportFactory.sql_tutoship_sessions)
        connection = EngineFactory.get_engine()
        result = connection.execute(s, id_unit=id_unit, start_time=start_time, end_time=end_time).fetchall()
        return result
