from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class GetLastSessionsByStudentsByIndividualTutorshipUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None, repo_session=None):
        super().__init__(errors, req)
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        self.repo_session = repo_session or self.RepoFactory.SessionRepo(session=self.repo_student.session)
        self.id_tutorship = req.id_tutorship
        self.id_student = req.id_student

    @catch_errors
    def execute(self):

        student = self.repo_student.read_data_student_id(self.id_student)
        session = self.repo_session.get_last_session_by_tutorship_by_student(self.id_tutorship, self.id_student)

        self.res = Response(student=student, session=session)
        self.success = GetLastSessionsByStudentsByIndividualTutorshipSuccess


class GetLastSessionsByStudentsByIndividualTutorshipSuccess(Success):
    code = info_success_logic.SESSIONS_FOR_STUDENTS_BY_TUTORSHIP_SEARCH_OK_CODE
    message = info_success_logic.SESSIONS_FOR_STUDENTS_BY_TUTORSHIP_SEARCH_OK_MESSAGE
