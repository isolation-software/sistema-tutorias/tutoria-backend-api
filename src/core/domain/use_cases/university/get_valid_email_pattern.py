from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class GetValidEmailPatternsUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_valid_email=None):
        super().__init__(errors, req)
        # repos
        self.repo_valid_email = repo_valid_email or self.RepoFactory.ValidEmailRepo()
        # request data

    @catch_errors
    def execute(self):
        emails = self.repo_valid_email.get_valid_email_pattern()
        self.res = Response(emails_saved=emails)
        self.success = GetValidEmailPatternsSuccess


class GetValidEmailPatternsSuccess(Success):
    code = info_success_logic.VALID_EMAIL_SEARCH_OK_CODE
    message = info_success_logic.VALID_EMAIL_SEARCH_OK_MESSAGE
