from src.config import LoggerFactory
from src.helpers.serializer import id_to_str


class UniversitySerializer:

    logger = LoggerFactory.get_logger(__name__)

    @staticmethod
    def university_data(response):
        university = response.university
        return {
            "universidad": {
                "idUniversidad": id_to_str(university.idx),
                "nombre": university.name,
                "dominio": university.domain,
                "logo": {
                    "url": university.logo
                }
            }
        }

    @staticmethod
    def domains(response):
        domains = response.domains
        return {
            "dominios": [domain.pattern() for domain in domains]
        }