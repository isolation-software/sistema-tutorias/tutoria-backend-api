from src.core.domain.entities.enum.session_states import SessionStates
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class GetSessionsStateUseCase(UseCase):
    def __init__(self, errors=None, req=None):
        super().__init__(errors, req)

    @catch_errors
    def execute(self):
        states = [s for s in SessionStates]
        self.res = Response(states=states)
