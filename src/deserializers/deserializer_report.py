import dateutil

from src.core.domain.entities import Unit
from src.helpers import constants
from src.helpers.serializer import str_to_id


class ReportDeserializer:

    @staticmethod
    def get_dates(json):
        data = json["data"]
        start_date = dateutil.parser.parse(data["Fechaini"])
        end_date = dateutil.parser.parse(data["Fechafin"])
        unit = Unit(idx=str_to_id(data["idUnidad"]))

        return start_date, end_date, unit
