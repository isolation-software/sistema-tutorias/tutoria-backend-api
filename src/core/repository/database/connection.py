from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from src.config import EngineFactory

DBSession = sessionmaker(bind=EngineFactory.get_engine())
Base = declarative_base()
