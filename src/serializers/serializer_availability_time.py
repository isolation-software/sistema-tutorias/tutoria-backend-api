from src.config import LoggerFactory
from src.helpers.serializer import id_to_str


class AvailabilityTimeSerializer:

    logger = LoggerFactory.get_logger(__name__)

    @classmethod
    def schedule(cls, response):
        dates = response.schedule
        return [
            {
                "idDisponibilidad": id_to_str(date.idx),
                "fecha": date.time.isoformat(),
                "estado": date.state,
                "sesion": {
                    "idSesion": id_to_str(date.session.idx),
                    "nombreTutoria": date.session.sessions_serie.tutorship.name,
                    "estado": date.session.state.value
                } if date.session else None,
                "aprobado": date.approved
            } for date in dates] if dates else []

    @classmethod
    def schedule_by_unit(cls, response):
        dates = response.schedule
        return [
            {
                "idDisponibilidad": id_to_str(date.idx),
                "unidad": {
                    "idUnidad": id_to_str(date.unit.idx),
                    "nombre": date.unit.name
                },
                "fecha": date.time.isoformat(),
                "estado": date.state,
                "sesion": {
                    "idSesion": id_to_str(date.session.idx),
                    "nombreTutoria": date.session.sessions_serie.tutorship.name,
                    "estado": date.session.state.value
                } if date.session else None,
                "aprobado": date.approved
            } for date in dates] if dates else []