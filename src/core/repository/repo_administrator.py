from src.core.repository.common import Repository
from src.core.repository.database.models import UserModel, AdministratorModel
from src.core.repository.errors import catch_repo_errors


class AdministratorRepo(Repository):

    @catch_repo_errors
    def read_data_administrator_id(self, id_administrator):
        row_administrator = self.session.query(AdministratorModel).join(UserModel).filter(
            AdministratorModel.id_administrator == id_administrator
        ).first()
        return row_administrator.to_entity()
