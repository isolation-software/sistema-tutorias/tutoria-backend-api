# Tutoría back-end / API REST
[![pipeline status](https://gitlab.com/isolation-software/sistema-tutorias/tutoria-backend-api/badges/develop/pipeline.svg)](https://gitlab.com/isolation-software/sistema-tutorias/tutoria-backend-api/-/commits/develop)
[![coverage report](https://gitlab.com/isolation-software/sistema-tutorias/tutoria-backend-api/badges/develop/coverage.svg)](https://gitlab.com/isolation-software/sistema-tutorias/tutoria-backend-api/-/commits/develop)

## QuickStart

### Variables de entorno
El proyecto utiliza variables de entorno para realizar una configuración automática:

|    Nombre   |                         Valores                        |                                                                                                      Descripción                                                                                                     |
|:-----------:|:------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ENVIRONMENT | `DEVELOPMENT` `STAGING` `PRODUCTION`                   | Describe el ambiente en el que nos encontramos. - `DEVELOPMENT`: ambiente de desarrollo local - `STAGING`: ambiente para pruebas de integración y aceptación - `PRODUCTION`: ambiente de producción, usuarios reales |
| CONFIG_PATH | `/home/daniel/software/config` `D:software/config` etc | Ruta de la carpeta que contiene los archivos de configuración y credenciales `.ini`. Colocar la ruta de acuerdo al sistema operativo en el que se encuentra ejecutando.                                              |
| LOGS_PATH   | `/home/daniel/software/logs` `D:software/logs`         | Ruta de la carpeta en donde se guardarán los logs que el Sistema genere. Si el ambiente es de Desarollo (`DEVELOPMENT`), se grabarán los logs de DEBUG.                                                              |

Luego de ello, debe configurar los archivos de `database.ini` y `api.ini` en caso se encuentre en un ambiente de Desarrollo.

**OBSERVACIÓN:** Puede no requerir modificar estos archivos ya que vienen con valores predeterminados. Asegúrese que los puertos
y direcciones no se encuentren ocupados o inexistentes.

**OBSERVACIÓN:**

### Backend
Instalar todas las dependencias en el ambiente virtual de python en modo desarrollador
```bash
pipenv install --dev
```

**OBSERVACIÓN:**
Antes de correr la aplicación, se debe tener en cuenta que el gestor postgres se encuentre
instalado y corriendo. Lo puede realizar de la manera directa descargando el gestor por su
página web, o usar comandos docker para correrlo como contenedor. Para mayor detalle, ver la
sección **Base de datos**.

### Base de datos
en progreso