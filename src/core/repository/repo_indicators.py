from src.core.repository.common import Repository
from src.core.repository.database.models import IndicatorModel
from src.core.repository.errors import catch_repo_errors


class IndicatorsRepo(Repository):

    @catch_repo_errors
    def register_indicators(self, id_session, indicators):
        row_indicators = IndicatorModel(
            indicator_1=indicators.indicator1,
            indicator_2=indicators.indicator2,
            indicator_3=indicators.indicator3,
            indicator_4=indicators.indicator4,
            indicator_5=indicators.indicator5,
            indicator_6=indicators.indicator6,
            id_session=id_session
        )
        self.session.add(row_indicators)
        self.session.commit()
        self.session.refresh(row_indicators)
        return row_indicators.to_entity()

    @catch_repo_errors
    def get_indicator_by_session(self, id_session):
        row_indicator = self.session.query(IndicatorModel).filter(IndicatorModel.id_session == id_session).one_or_none()
        return row_indicator.to_entity() if row_indicator else None
