from flask_restful import Resource

from src.app.auth.handler import jwt_required
from src.controllers import AdministratorController

from src.helpers import constants


class AdministratorResource(Resource):
    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class AdministratorGetResource(Resource):
    @jwt_required
    def get(self, id_administrator):
        response = AdministratorController.get_administrator(id_administrator=id_administrator)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST
