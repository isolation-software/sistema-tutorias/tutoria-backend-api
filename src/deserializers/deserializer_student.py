from src.core.domain.entities import Unit, Student, User, AdditionalInformation
from src.helpers import constants
from src.helpers.serializer import str_to_id


class StudentDeserializer:

    @staticmethod
    def student(json):
        data = json["data"]
        unit = Unit(idx=str_to_id(data["alumno"]["idUnidad"]))
        user = User(names=data["alumno"]["nombres"],
                    code=data["alumno"]["codigo"],
                    father_lastname=data["alumno"]["apellidoPat"],
                    mother_lastname=data["alumno"]["apellidoMat"],
                    email=data["alumno"]["correo"],
                    phone_number=data["alumno"]["telefono"])
        student = Student(user=user, unit=unit)

        return student

    @classmethod
    def info_file(cls, json, id_student):
        student = Student(idx=str_to_id(id_student))
        file_extension = json.get("extension") or json.get("nombre")[json.get("nombre").rfind(constants.DOT) + 1:] or \
                         constants.OCTECT_STREAM

        return AdditionalInformation(name=json.get("nombre"), student=student), file_extension

    @staticmethod
    def students(json):
        unitCoordinator = Unit(idx=str_to_id(json["idUnidad"]))
        students = []
        for json_student in json["alumnos"]:
            user = User(names=json_student["nombres"],
                        code=json_student["codigo"],
                        father_lastname=json_student["apellidoPat"],
                        mother_lastname=json_student["apellidoMat"],
                        email=json_student["correo"],
                        phone_number=json_student["telefono"])
            unit = Unit(code=json_student["codUnidad"])
            student = Student(user=user, unit=unit)
            students.append(student)
        return unitCoordinator, students

