from src.helpers.serializer import id_to_str


class TutorSerializer:

    @staticmethod
    def tutor_list_by_unit(tutors_by_unit):
        return [{"id_tutor": id_to_str(t.idx),
                 "codigo": t.user.code,
                 "nombres": f"{t.user.names} {t.user.father_lastname} {t.user.mother_lastname}"}
                for t in tutors_by_unit]

    @staticmethod
    def serialize_list_sessions_tutor_response(session_by_tutor):
        return None

    @staticmethod
    def serialize_tutor_id(response):
        tutor = response.tutor
        return {
            "idTutor": id_to_str(tutor.idx),
            "idUsuario": id_to_str(tutor.user.idx),
            "correo": tutor.user.email,
            "codigo": tutor.user.code,
            "nombres": tutor.user.names,
            "apellidoPat": tutor.user.father_lastname,
            "apellidoMat": tutor.user.mother_lastname,
            "telefono": tutor.user.phone_number,
            "tienePermisoAccesoInfoAdicional": tutor.has_access_to_additional_information
        }

