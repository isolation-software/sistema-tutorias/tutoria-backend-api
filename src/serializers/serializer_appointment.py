from src.helpers.serializer import id_to_str


class AppointmentSerializer:

    @staticmethod
    def serialize_list_appointment_response(response):
        sessions = response.sessions
        return [{"nombre": s.sessions_serie.tutorship.name,
                 "fecha": s.start_time.isoformat(),
                 "tutor": f"{s.tutor.user.names} {s.tutor.user.father_lastname} "
                          f"{s.tutor.user.mother_lastname}",
                 "estado": s.state.value}
                for s in sessions]
