
# BUSINESS LOGIC: ERRORS & MESSAGES
GENERIC_LOGIC_EXCEPTION_CODE = 700
GENERIC_LOGIC_EXCEPTION_MESSAGE = "Error en el sistema, vuelva a intentarlo más tarde"

STUDENT_CANNOT_REGISTER_ALREADY_EXISTS_CODE = 701
STUDENT_CANNOT_REGISTER_ALREADY_EXISTS_MESSAGE = "El alumno {names} {lastnames} con correo {email} no puede ser registrado nuevamente en el sistema"

COORDINATOR_CANNOT_REGISTER_ALREADY_EXISTS_CODE = 702
COORDINATOR_CANNOT_REGISTER_ALREADY_EXISTS_MESSAGE = "El coordinador {names} {lastnames} con correo {email} no puede ser registrado nuevamente en el sistema"

USER_NOT_FOUND_IN_SYSTEM_EXCEPTION_CODE = 703
USER_NOT_FOUND_IN_SYSTEM_EXCEPTION_MESSAGE = "No se ha encontrado al usuario con el correo {email}"

TUTOR_CANNOT_REGISTER_ALREADY_EXISTS_CODE = 704
TUTOR_CANNOT_REGISTER_ALREADY_EXISTS_MESSAGE = "El tutor {names} {lastnames} con correo {email} no puede ser registrado nuevamente en el sistema"

TIME_ALREADY_TAKEN_ERROR = 705
TIME_ALREADY_TAKEN_MESSAGE = "El horario seleccinado ya ha sido reservado anteriormente. Elija otra fecha."

REGISTER_TOKEN_INVALID_CODE = 706
REGISTER_TOKEN_INVALID_MESSAGE = "EL token no es valido"

REGISTER_TOKEN_EXPIRED_CODE = 707
REGISTER_TOKEN_EXPIRED_MESSAGE = "El token ha expirado"

UNAUTHENTICATED_USER_ACCOUNT_CODE = 708
UNAUTHENTICATED_USER_ACCOUNT_MESSAGE = "Esta cuenta {email} aún no ha sido confirmada"

INCORRECT_PASSWORD_CODE = 709
INCORRECT_PASSWORD_MESSAGE = "La contraseña ingresada es incorrecta. Vuelva a intentarlo"


DELETE_STUDENT_INDIVIDUAL_TUTORSHIP_OK_CODE = 720
DELETE_STUDENT_INDIVIDUAL_TUTORSHIP_OK_MESSAGE = "EL alumno no puede ser eliminado de esta tutoría porque ya posee citas"

DELETE_TUTOR_INDIVIDUAL_TUTORSHIP_OK_CODE = 721
DELETE_TUTOR_INDIVIDUAL_TUTORSHIP_OK_MESSAGE = "EL tutor no puede ser eliminado de esta tutoria porque ya posee citas"

REGISTER_INDICATOR_OK_CODE = 722
REGISTER_INDICATOR_OK_MESSAGE = "Esta sesion ya ha sido calificada"

REGISTER_INDIVIDUAL_TUTORSHIP_WHEN_GROUP_ERROR = 740
REGISTER_INDIVIDUAL_TUTORSHIP_WHEN_GROUP_MESSAGE = "No se puede registrar una tutoría individual si su característica indica que es grupal"
