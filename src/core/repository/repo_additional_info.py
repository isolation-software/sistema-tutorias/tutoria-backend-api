from datetime import datetime
from pytz import timezone

from src.core.repository.common import Repository
from src.core.repository.database.models.model_additional_info import AdditionalInfoModel
from src.core.repository.errors import catch_repo_errors
from src.helpers import constants


class AdditionalInfoRepo(Repository):

    @catch_repo_errors
    def get_additional_info(self,student):
        additional_info = self.session.query(AdditionalInfoModel).\
            filter(AdditionalInfoModel.id_student == student.idx).all()

        return [info.to_entity() for info in additional_info]

    @catch_repo_errors
    def delete_additional_info(self, id_additional_info):
        self.session.query(AdditionalInfoModel)\
            .filter(AdditionalInfoModel.id_additional_info == id_additional_info).delete()
        self.session.commit()

    @catch_repo_errors
    def register_additional_info(self, additional_info):
        row_info = AdditionalInfoModel(
            id_student=additional_info.student.idx,
            url_file=additional_info.archive,
            name=additional_info.name,
            date_created=datetime.now(timezone(constants.UTC))
        )

        self.session.add(row_info)
        self.session.commit()
        self.session.refresh(row_info)

        return row_info.to_entity()