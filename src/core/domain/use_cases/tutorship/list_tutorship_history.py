from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response
from src.helpers import constants


class ListTutorshipHistoryUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        # request data
        self.student = req.student
        self.search = req.search or constants.EMPTY_STRING

    @catch_errors
    def execute(self):
        tutorships = self.repo_tutorship.read_tutorship_history(self.student, self.search)
        self.res = Response(tutorships=tutorships)
