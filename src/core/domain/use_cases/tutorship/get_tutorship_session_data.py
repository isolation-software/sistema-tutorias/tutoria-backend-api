from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class GetTutorshipSessionData(UseCase):
    def __init__(self, errors=None, req=None, repo_session=None):
        super().__init__(errors, req)
        # repos
        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        # request data
        self.start_date = req.start_date
        self.end_date = req.end_date
        self.unit = req.unit

    @catch_errors
    def execute(self):
        stats = self.repo_session.get_sessions_statistics_for_tutorships_by_unit(start_date=self.start_date,
                                                                                 end_date=self.end_date,
                                                                                 unit=self.unit)
        self.res = Response(stats=stats)
