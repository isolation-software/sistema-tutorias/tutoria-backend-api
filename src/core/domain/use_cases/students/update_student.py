from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class UpdateStudentUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        # request data
        self.student = req.student

    @catch_errors
    def execute(self):
        student_updated = self.repo_student.update_student(self.student)
        self.res = Response(student_updated=student_updated)
