from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListStudentsIndividualTutorshipUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None, repo_sessions_serie=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_sessions_serie = repo_sessions_serie or self.RepoFactory.SessionsSerieRepo(session=self.repo_tutorship.session)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo(session=self.repo_tutorship.session)
        self.id_tutorship = req.id_tutorship
        self.search = req.search
        self.limit = req.limit
        self.filter = req.filter

    @catch_errors
    def execute(self):
        unit = self.repo_tutorship.get_unit_by_tutorship(self.id_tutorship)
        children_units = self.repo_unit.read_children_units_by_father_id(unit)
        students = self.repo_sessions_serie.list_students_individual_tutorship(self.id_tutorship, unit, self.search,
                                                                               self.limit, self.filter, children_units)
        self.res = Response(students=students)
        self.success = ListStudentsNotinIndividualTutorshipSuccess


class ListStudentsNotinIndividualTutorshipSuccess(Success):
    code = info_success_logic.LIST_STUDENTS_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.LIST_STUDENTS_INDIVIDUAL_TUTORSHIP_OK_MESSAGE