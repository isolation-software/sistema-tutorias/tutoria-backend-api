
class Session:

    def __init__(self, idx=None, start_time=None, end_time=None, comments=None, state=None, support_unit=None,
                 tutor=None, order_number=None, derivation=None, sessions_serie=None, register_date=None):
        self.idx = idx
        self.start_time = start_time
        self.end_time = end_time
        self.comments = comments
        self.state = state
        self.tutor = tutor
        self.derivation = derivation
        self.sessions_serie = sessions_serie
        self.order_number = order_number
        self.support_unit = support_unit
        self.register_date = register_date
