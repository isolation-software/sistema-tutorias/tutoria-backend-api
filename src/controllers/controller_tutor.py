import dateutil.parser

from src.core.domain.entities import Unit, Tutor
from src.controllers.common import Controller
from src.core.domain.use_cases.common import Request, Response
from src.core.domain.use_cases.tutors import ListSessionsForTutorUseCase, RegisterTutorUseCase, \
    RegisterAvailabilityTimeUseCase, ReplicateAvailabilityTimeUseCase, ListAvailabilityTimeForTutorUseCase
from src.core.domain.use_cases.tutors.list_tutors_by_unit import ListTutorsByUnitUseCase
from src.core.domain.use_cases.tutors.request_data_tutor import RequestDataTutorUseCase
from src.deserializers.deserializer_tutor import TutorDeserializer
from src.deserializers.deserializer_availability_time import AvailabilityTimeDeserializer
from src.serializers.serializer_availability_time import AvailabilityTimeSerializer
from src.helpers import constants
from src.helpers.serializer import str_to_id
from src.serializers.serializer_tutor import TutorSerializer


class TutorController(Controller):
    list_sessions_tutor_uc = ListSessionsForTutorUseCase
    tutor_serializer = TutorSerializer
    register_tutor_uc = RegisterTutorUseCase
    tutor_deserializer = TutorDeserializer

    @staticmethod
    def listTutorByUnit(id_unit, search):
        unit = Unit(idx=str_to_id(id_unit))
        uc = ListTutorsByUnitUseCase(req=Request(unit=unit, search=search))
        uc.execute()

        if not uc.has_errors():
            tutors_by_unit = uc.res.tutors_by_unit
            serialized = TutorSerializer.tutor_list_by_unit(tutors_by_unit)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            data={})

    @classmethod
    def listSessionsForTutor(cls, idTutor, search):
        tutor = Tutor(idx=str_to_id(idTutor))
        return cls.build_response(
            use_case=cls.list_sessions_tutor_uc,
            req=Request(tutor=tutor, search=search),
            serialize_response=cls.tutor_serializer.serialize_list_sessions_tutor_response
        )

    @classmethod
    def register_tutor(cls, json):
        tutor, unit = cls.tutor_deserializer.tutor(json)

        return cls.build_response(
            use_case=cls.register_tutor_uc,
            req=Request(tutor=tutor, unit=unit),
            serialize_response=None
        )

    register_availability_time_uc = RegisterAvailabilityTimeUseCase
    availability_time_deserializer = AvailabilityTimeDeserializer
    availability_time_serializer = AvailabilityTimeSerializer

    @classmethod
    def register_availability_time(cls, json, id_tutor):
        unit, availability_dates = cls.availability_time_deserializer.availability_dates(json, id_tutor)
        req = Request(availability_dates=availability_dates, tutor=Tutor(idx=str_to_id(id_tutor)), unit=unit)

        return cls.build_response(
            use_case=cls.register_availability_time_uc,
            req=req
        )

    replicate_availability_time_uc = ReplicateAvailabilityTimeUseCase

    @classmethod
    def replicate_availability_time(cls, json, id_tutor):
        unit, replicated_dates = cls.availability_time_deserializer.replicate_dates(json, id_tutor)
        req = Request(availability_dates=replicated_dates, tutor=Tutor(idx=str_to_id(id_tutor)), unit=unit)

        return cls.build_response(
            use_case=cls.replicate_availability_time_uc,
            req=req
        )

    list_availability_time_for_tutor_uc = ListAvailabilityTimeForTutorUseCase

    @classmethod
    def list_availability_time_for_tutor(cls, id_tutor, list_non_approved, start_date, end_date):
        start_date = dateutil.parser.parse(start_date)
        end_date = dateutil.parser.parse(end_date)
        req = Request(tutor=Tutor(idx=str_to_id(id_tutor)),
                      list_non_approved=list_non_approved,
                      start_date=start_date,
                      end_date=end_date)

        return cls.build_response(
            use_case=cls.list_availability_time_for_tutor_uc,
            req=req,
            serialize_response=cls.availability_time_serializer.schedule_by_unit
        )

    request_data_tutor_uc = RequestDataTutorUseCase

    @classmethod
    def get_tutor(cls, id_tutor):
        id_tutor = str_to_id(id_tutor)
        req = Request(id_tutor=id_tutor)

        return cls.build_response(
            use_case=cls.request_data_tutor_uc,
            req=req,
            serialize_response=cls.tutor_serializer.serialize_tutor_id
        )
