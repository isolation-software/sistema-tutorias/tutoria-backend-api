import uuid

import pytest

from src.core.domain.entities.student import Student


@pytest.fixture
def student_dictionary():
    return dict(idx=uuid.uuid4(), user=None)


def test_construct_student_from_dict(student_dictionary):
    student = Student.from_dict(student_dictionary)
    assert student.idx  == student_dictionary['idx'], "Constructor con diccionario no implementado correctamente"
    assert student.user == student_dictionary['user'], "Constructor con diccionario no implementado correctamente"


def test_two_students_same_id():
    idx = uuid.uuid4()
    origin = Student(idx=idx)
    another = Student(idx=idx)
    assert origin == another, "Método comparación no implementado correctamente"


def test_two_students_different_ids():
    origin = Student(idx=uuid.uuid4())
    another = Student(idx=uuid.uuid4())
    assert origin != another, "Método comparación no implementado correctamente"
