from src.core.repository.database.connection import DBSession


class SessionFactory:
    @staticmethod
    def get_session():
        return DBSession()


class Repository:
    """ Class that brings facilities to persist data into
        a database or third-party services. It implements
        methods like Read, Insert, Update, Delete and variants
    """
    def __init__(self, session=None):
        self._is_own_session = session is None
        self.session = session or SessionFactory().get_session()

    def close(self):
        if self.session is not None and self._is_own_session:
            self.session.close()
