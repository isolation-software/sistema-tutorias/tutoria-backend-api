from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RequestDataAdministratorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_administrator=None):
        super().__init__(errors, req)
        # repos
        self.repo_administrator = repo_administrator or self.RepoFactory.AdministratorRepo()
        # request data
        self.id_administrator = req.id_administrator

    @catch_errors
    def execute(self):
        coordinator = self.repo_administrator.read_data_administrator_id(self.id_administrator)

        self.res = Response(coordinator=coordinator)
        self.success = RequestDataAdministratorSuccess


class RequestDataAdministratorSuccess(Success):
    code = info_success_logic.REQUEST_DATA_ADMINISTRATOR_OK_CODE
    message = info_success_logic.REQUEST_DATA_ADMINISTRATOR_OK_MESSAGE
