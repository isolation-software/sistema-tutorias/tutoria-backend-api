from src.core.domain.entities import Unit, User, Tutor
from src.helpers.serializer import str_to_id


class TutorDeserializer:

    @staticmethod
    def tutor(json):
        data = json["data"]
        user = User(names=data["tutor"]["nombres"],
                    code=data["tutor"]["codigo"],
                    father_lastname=data["tutor"]["apellidoPat"],
                    mother_lastname=data["tutor"]["apellidoMat"],
                    email=data["tutor"]["correo"],
                    phone_number=data["tutor"]["telefono"],
                    )
        unit = Unit(idx= data["idUnidad"])
        tutor = Tutor(user=user,
                      has_access_to_additional_information=data["tutor"].get("tienePermisoAccesoInfoAdicional", False))

        return tutor, unit
