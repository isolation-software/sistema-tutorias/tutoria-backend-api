import logging
import logging.config

from src.config.config_general import EnvironFiles, EnvironLogs


class LoggerFactory:

    level = None

    @classmethod
    def config(cls):
        """ Logger Configuration with the file .ini needed and gotten from
            the environment variable. Log level depends on the environment type. """
        cls.level = EnvironLogs.LEVEL
        logging.general_file_path = EnvironLogs.LOG_GENERAL_FILE_PATH
        logging.config.fileConfig(EnvironFiles.PATH_FILENAME_LOG)

    @classmethod
    def get_logger(cls, name):
        """Gets an instance of a Logger with the given name
        :param name: name of the current file or class
        :return: a logger instance
        """
        logger = logging.getLogger(name)
        logger.setLevel(cls.level)
        return logger


LoggerFactory.config()
