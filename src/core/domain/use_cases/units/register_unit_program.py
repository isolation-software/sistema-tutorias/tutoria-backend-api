from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterProgramUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_unit=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo(session=self.repo_unit.session)
        self.program   = req.program

    @catch_errors
    def execute(self):
        saved_program = self.repo_unit.save_unit_program(self.program)
        self.repo_tutorship.register_moment_tutorship_in_unit(id_unit=saved_program.idx)
        self.res = Response(program=saved_program)
        self.success = RegisterProgramSuccess


class RegisterProgramSuccess(Success):
    code    = info_success_logic.REGISTER_PROGRAM_OK_CODE
    message = info_success_logic.REGISTER_PROGRAM_OK_MESSAGE
