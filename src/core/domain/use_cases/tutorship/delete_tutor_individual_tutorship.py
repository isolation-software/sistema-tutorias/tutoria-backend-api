from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import TutorNotDeleteInSystemError
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class DeleteTutorIndividualTutorshipUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.id_tutorship = req.id_tutorship
        self.id_tutor = req.id_tutor

    @catch_errors
    def execute(self):
        unit = self.repo_tutorship.get_unit_by_tutorship(self.id_tutorship)
        is_possible = self.repo_tutorship.delete_tutor_individual_tutorship(self.id_tutorship, self.id_tutor, unit.idx)
        if not is_possible:
            raise TutorNotDeleteInSystemError()
        self.res = Response()
        self.success = DeleteTutorIndividualTutorshipSuccess


class DeleteTutorIndividualTutorshipSuccess(Success):
    code = info_success_logic.DELETE_TUTOR_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.DELETE_TUTOR_INDIVIDUAL_TUTORSHIP_OK_MESSAGE
