from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ListMomentTutorshipSessionForStudentUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None, repo_session=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_session = repo_session or self.RepoFactory.SessionRepo(self.repo_tutorship.session)
        # request data
        self.student = req.student

    @catch_errors
    def execute(self):
        tutorship = self.repo_tutorship.read_moment_tutorship()
        sessions = self.repo_session.get_sessions_by_student_and_tutorship(student=self.student, tutorship=tutorship)
        self.res = Response(sessions=sessions)
