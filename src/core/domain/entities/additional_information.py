
class AdditionalInformation:

    def __init__(self, idx=None, name=None, archive=None, student=None, date_created=None):
        self.idx = idx
        self.name = name
        self.archive = archive
        self.student = student
        self.date_created = date_created
