from sqlalchemy import and_, or_

from src.core.domain.entities import Tutor, User
from src.core.repository import UserRepo
from src.core.repository.common import Repository
from src.core.repository.database.models import TutorModel, UserModel, TutorPerUnitModel, TutorPerTutorshipModel, \
    TutorshipModel, SessionModel, SessionSetModel, SetPerStudentModel, StudentModel
from src.core.repository.errors import catch_repo_errors
from src.helpers import constants


class TutorRepo(Repository):

    @catch_repo_errors
    def list_tutors_by_unit(self, id_unit, search):
        search = "%{}%".format(search)
        row_tutors = self.session.query(TutorModel).join(TutorPerUnitModel,
                                                         TutorPerUnitModel.id_tutor == TutorModel.id_tutor) \
            .join(UserModel, TutorModel.id_user == UserModel.id_user).filter(
            and_(
                TutorPerUnitModel.id_unit == id_unit,
                or_(
                    UserModel.code.ilike(search),
                    UserModel.names.ilike(search),
                    UserModel.father_lastname.ilike(search),
                    UserModel.mother_lastname.ilike(search)
                )
            )
        ).order_by(UserModel.register_date.desc()).all()
        return [tr.to_entity() for tr in row_tutors]

    @catch_repo_errors
    def get_tutors_by_tutorship(self, id_tutorship):
        tutors_rows = self.session.query(TutorModel) \
            .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
            .join(TutorPerTutorshipModel,
                  TutorPerUnitModel.id_tutor_per_unit == TutorPerTutorshipModel.id_tutor_per_unit) \
            .filter(TutorPerTutorshipModel.id_tutorship == id_tutorship) \
            .all()
        tutors = [tr.to_entity() for tr in tutors_rows]
        return len(tutors), tutors

    @catch_repo_errors
    def get_tutor_per_unit(self, id_tutor, id_unit):
        tutor_row = self.session.query(TutorPerUnitModel).filter(
            and_(
                TutorPerUnitModel.id_tutor == id_tutor,
                TutorPerUnitModel.id_unit == id_unit
            )
        ).one_or_none()
        return Tutor(idx=tutor_row.id_tutor_per_unit)

    @catch_repo_errors
    def list_sessions_by_tutor(self, tutor, search=""):
        search = "%{}%".format(search)
        row_sessions = self.session \
            .query(SessionModel, UserModel, TutorshipModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor_per_unit == SessionModel.id_tutor_per_unit) \
            .join(TutorModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(UserModel, UserModel.id_user == StudentModel.id_user) \
            .filter(and_(
            TutorshipModel.name.ilike(search),
            TutorModel.id_tutor == tutor.idx)) \
            .order_by(SessionModel.state.asc()) \
            .all()
        return [row_session.to_entity() for row_session in row_sessions]

    @catch_repo_errors
    def save_tutor_with_user(self, tutor, unit):
        repo_user = UserRepo()
        user = repo_user.save_user(tutor.user)

        row_tutor = TutorModel(id_user=user.idx,
                               has_access_to_additional_information=tutor.has_access_to_additional_information)

        self.session.add(row_tutor)
        self.session.commit()
        self.session.refresh(row_tutor)
        tutor.idx = row_tutor.id_tutor
        row_tutor_unit = TutorPerUnitModel(id_tutor=tutor.idx, id_unit=unit.idx)
        self.session.add(row_tutor_unit)
        self.session.commit()
        return tutor

    @catch_repo_errors
    def save_tutor_without_user(self, tutor, unit):
        row_user = self.session.query(UserModel).filter(UserModel.email == tutor.user.email).one()
        row_tutor = TutorModel(id_user=row_user.id_user,
                               has_access_to_additional_information=tutor.has_access_to_additional_information)
        self.session.add(row_tutor)
        self.session.commit()
        self.session.refresh(row_tutor)
        tutor.idx = row_tutor.id_tutor
        row_tutor_unit = TutorPerUnitModel(id_tutor=tutor.idx, id_unit=unit.idx)
        self.session.add(row_tutor_unit)
        self.session.commit()
        self.session.close()

    @catch_repo_errors
    def save_tutor_in_tutor_per_unit(self, tutor, unit):
        row_user = self.session.query(TutorModel) \
            .join(UserModel, UserModel.id_user == TutorModel.id_user) \
            .filter(UserModel.email == tutor.user.email).one()
        row_tutor_unit = TutorPerUnitModel(id_tutor=row_user.id_tutor, id_unit=unit.idx)

        self.session.add(row_tutor_unit)
        self.session.commit()
        self.session.close()

    @catch_repo_errors
    def is_tutor_in_tutor_per_unit(self, tutor, unit):
        row_count = self.session.query(TutorModel) \
            .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor == tutor.idx) \
            .filter(TutorPerUnitModel.id_unit == unit.idx) \
            .count()
        return row_count == constants.ONE

    @catch_repo_errors
    def read_data_tutor_id(self, id_tutor):
        row_tutor = self.session.query(TutorModel).join(UserModel).filter(
            TutorModel.id_tutor == id_tutor
        ).first()
        row_tutor.id_tutor = id_tutor
        return row_tutor.to_entity()

    @catch_repo_errors
    def update_tutor_permission(self, id_tutor, has_access_to_additional_information):
        row_tutor = self.session.query(TutorModel).filter(TutorModel.id_tutor == id_tutor).one()
        row_tutor.has_access_to_additional_information = has_access_to_additional_information
        self.session.commit()
        return row_tutor.to_entity()
