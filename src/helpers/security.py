import hashlib
import uuid

from src.helpers import constants


def hash_password(password):
    """
    Hashing a password randomly and returning the salt used and
    the hashed password using sha512 (128 characters)
    :param password:
    :return:
    """
    salt    = uuid.uuid4().hex
    encoded = (password+salt).encode(constants.UTF8)
    hashed  = hashlib.sha512(encoded).hexdigest()
    return hashed, salt


def check_password(password, hashed, salt):
    """
    Check if password given is valid using the hashed password and salt generated and used
    before, if it's the same hash, it's true.
    :param password: given by the user who is trying to authenticate itself
    :param hashed: password hashed obtained from database
    :param salt: word used when hashing the password
    :return:
    """
    encoded = (password + salt).encode(constants.UTF8)
    recovered = hashlib.sha512(encoded).hexdigest()
    return hashed == recovered

