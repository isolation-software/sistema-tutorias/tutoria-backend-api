from src.core.repository.common import Repository
from src.core.repository.database.models.model_support_unit import SupportUnitModel
from src.core.repository.errors import catch_repo_errors


class SupportUnitRepo(Repository):

    @catch_repo_errors
    def read_support_unit(self):
        rows_support_units = self.session.query(SupportUnitModel).all()
        return [row_support_unit.to_entity() for row_support_unit in rows_support_units]

