from src.helpers.serializer import id_to_str


class CoordinatorSerializer:

    @classmethod
    def serialize_coordinators(cls, coordinators):
        return [cls.serialize_coordinator(c) for c in coordinators]

    @classmethod
    def serialize_coordinator(cls, coordinator):
        return None if not coordinator else {
            "idCoordinador": id_to_str(coordinator.idx),
            "nombres": coordinator.user.fullname(),
            "codigo": coordinator.user.code
        }

    @staticmethod
    def serialize_coordinator_id(response):
        coordinator = response.coordinator
        return {
            "idCoordinador": id_to_str(coordinator.idx),
            "idUsuario": id_to_str(coordinator.user.idx),
            "correo": coordinator.user.email,
            "codigo": coordinator.user.code,
            "nombres": coordinator.user.names,
            "apellidoPat": coordinator.user.father_lastname,
            "apellidoMat": coordinator.user.mother_lastname,
            "telefono": coordinator.user.phone_number

        }
