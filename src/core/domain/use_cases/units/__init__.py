from src.core.domain.use_cases.units.get_unit import GetUnitUseCase
from src.core.domain.use_cases.units.list_faculty import ListFacultiesUseCase
from src.core.domain.use_cases.units.list_units import ListUnitsUseCase
from src.core.domain.use_cases.units.register_faculty import RegisterFacultyUseCase
from src.core.domain.use_cases.units.register_unit_program import RegisterProgramUseCase
from src.core.domain.use_cases.units.update_unit import UpdateUnitUseCase
