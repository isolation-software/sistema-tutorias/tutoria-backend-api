from enum import Enum


class Roles(Enum):
    STUDENT = "Alumno"
    TUTOR   = "Tutor"
    COORDINATOR   = "Coordinador"
    ADMINISTRATOR = "Administrador"
