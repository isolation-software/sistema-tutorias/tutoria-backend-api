from src.core.domain.entities import User
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import UserNotFoundInSystemError
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import constants, info_success_logic


class ListRolesForUserUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_user=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo(session=self.repo_user.session)
        self.email = req.email

    @catch_errors
    def execute(self):
        user = self.repo_user.read_user_by_email_if_exists(User(email=self.email))
        if not user:
            raise UserNotFoundInSystemError(email=self.email)
        roles = self.repo_user.read_roles(user.idx)
        coord_units = None
        if roles[constants.POSITION_COORD]:
            coord_units = self.repo_unit.list_units_by_coordinator_id(roles[constants.POSITION_COORD].idx)
        self.res = Response(user=user, roles=roles, coord_units=coord_units)
        self.success = ListRolesForUserSuccess


class ListRolesForUserSuccess(Success):
    code = info_success_logic.LIST_ROLES_SEARCH_OK_CODE
    message = info_success_logic.LIST_ROLES_SEARCH_OK_MESSAGE
