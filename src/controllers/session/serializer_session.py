from src.config import LoggerFactory
from src.core.domain.entities import Student
from src.helpers.serializer import id_to_str


class SessionSerializer:

    logger = LoggerFactory.get_logger(__name__)

    @staticmethod
    def session_list(sessions):
        return {
            "sesiones": [
                {
                    "idSession": id_to_str(session.idx),
                    "numeroOrden": session.order_number,
                    "idTutoria": id_to_str(tutorship.idx),
                    "nombreTutoria": tutorship.name,
                    "alumno": student.user.fullname(),
                    "idAlumno": id_to_str(student.idx),
                    "fecha": session.start_time.isoformat() if session.start_time else None,
                    "estado": session.state.value,
                    "segimiento": has_action_plan,
                    "idUnidad": id_to_str(tutorship.unit.idx) if tutorship.unit else None,
                    "nombreUnidad": tutorship.unit.name if tutorship.unit else None
                }
                for session, student, tutorship, has_action_plan in sessions]
        }

    @staticmethod
    def session_list_per_tutorship_per_student(response):
        sessions = response.sessions
        return {
            "sesiones": [
                {
                    "idSession": id_to_str(s.idx),
                    "numeroOrden": s.order_number,
                    "fecha": s.start_time.isoformat(),
                    "nombreTutor": s.tutor.user.fullname(),
                    "estado": s.state.value
                }
                for s in sessions]
        }

    @staticmethod
    def session_list_per_student(response):
        sessions = response.sessions
        return {
            "sesiones": [
                {
                    "idSession": id_to_str(s.idx),
                    "numeroOrden": s.order_number,
                    "nombreTutoria": s.sessions_serie.tutorship.name,
                    "fecha": s.start_time.isoformat(),
                    "nombreTutor": s.tutor.user.fullname() if s.tutor else None,
                    "estado": s.state.value
                }
                for s in sessions]
        }

    @staticmethod
    def session_data(response):

        action_plan = response.action_plan
        students = response.to_dict().get("students")
        student = response.to_dict().get("student")
        session = response.session
        unit = response.unit
        tutorship = response.tutorship

        if action_plan:
            action_plan_serialized = [{
                "nombre": a.description,
                "completado": a.state
            } for a in action_plan]
        else:
            action_plan_serialized = []

        return {
            "sesion": {
                "alumno": [{
                    "idAlumno": id_to_str(s.idx) if type(s) == Student else id_to_str(s),
                    "nombre": s.user.fullname() if type(s) == Student and s.user else None,
                    "codigo": s.user.code if type(s) == Student and s.user else None,
                    "correo": s.user.email if type(s) == Student and s.user else None,
                    "unidad": {
                        "idUnidad": id_to_str(unit.idx),
                        "nombre": unit.name
                    }
                } for s in students] if students else {
                    "idAlumno": id_to_str(student.idx) if type(student) == Student else id_to_str(student),
                    "nombre": student.user.fullname() if type(student) == Student and student.user else None,
                    "codigo": student.user.code if type(student) == Student and student.user else None,
                    "correo": student.user.email if type(student) == Student and student.user else None,
                    "unidad": {
                        "idUnidad": id_to_str(unit.idx),
                        "nombre": unit.name
                    }
                },
                "datos": {
                    "idSession": id_to_str(session.idx),
                    "idTutoria": id_to_str(tutorship.idx),
                    "nombreTutoria": tutorship.name,
                    "numeroOrden": session.order_number,
                    "fechaInicio": session.start_time.isoformat(),
                    "comentario": session.comments,
                    "derivacion": session.derivation,
                    "idUnidadDerivada": id_to_str(session.support_unit.idx) if session.support_unit else None,
                    "idEstado": session.state.value,
                },
                "planDeAccion": action_plan_serialized
            }
        }

    @staticmethod
    def session_states(states):
        return {
            "estados": [
                {
                    "nombre": s.value
                }
                for s in states]
        }

    @classmethod
    def request_sessions_by_tutorship_for_students(cls, res):
        tuples = res.students_and_sessions
        return {
            "alumnos": [
                {
                    "idAlumno": id_to_str(student.idx),
                    "codigo": student.user.code,
                    "nombre": student.user.fullname(),
                    "ultimaSesion": {
                        "idSesion": id_to_str(session.idx),
                        "fechaInicio": session.start_time.isoformat() if session.start_time else None,
                        "estado": session.state.value,
                        "tutorAsignado": {
                            "idTutor": id_to_str(session.tutor.idx),
                            "nombre": session.tutor.user.fullname()
                        } if session.tutor else None
                    } if session else None
                }
                for student, session in tuples
            ]
        }

    @classmethod
    def request_sessions_by_tutorship_by_student(cls, res):
        student, session = res.student, res.session
        return {
            "alumno":
                {
                    "idAlumno": id_to_str(student.idx),
                    "codigo": student.user.code,
                    "nombre": student.user.fullname(),
                    "ultimaSesion": {
                        "idSesion": id_to_str(session.idx),
                        "fechaInicio": session.start_time.isoformat() if session.start_time else None,
                        "estado": session.state.value,
                        "fechaRegistro": session.register_date.isoformat() if session.register_date else None,
                        "tutorAsignado": {
                            "idTutor": id_to_str(session.tutor.idx),
                            "nombre": session.tutor.user.fullname()
                        } if session.tutor else None
                    } if session else None
                }

        }
