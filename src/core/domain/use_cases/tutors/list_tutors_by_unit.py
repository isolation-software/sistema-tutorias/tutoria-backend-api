from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ListTutorsByUnitUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutor=None):
        super().__init__(errors, req)
        self.repo_tutor = repo_tutor or self.RepoFactory.TutorRepo()
        self.id_unit = req.unit.idx
        self.search = req.search

    @catch_errors
    def execute(self):
        tutors_by_unit = self.repo_tutor.list_tutors_by_unit(self.id_unit, self.search)
        self.res = Response(tutors_by_unit=tutors_by_unit)
