from src.config import LoggerFactory
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import StudentAlreadyRegisteredError
from src.core.domain.use_cases.common import UseCase, Response


class RegisterStudentUseCase(UseCase):

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_student=None, repo_user=None):
        super().__init__(errors, req)
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        self.repo_user = repo_user or self.RepoFactory.UserRepo(self.repo_student.session)
        self.student = req.student

    @catch_errors
    def execute(self):
        self.logger.debug('Iniciando registro de alumno')
        user = self.repo_user.read_user_by_email_if_exists(self.student.user)
        if user is not None:
            self.logger.debug('Registrando a un alumno con usuario existente')
            student_saved = self._register_already_user(user)
        else:
            self.logger.debug('Registrando a un alumno con usuario nuevo')
            student_saved = self._register_not_yet_user(self.student)
        self.res = Response(student=student_saved)

    def _register_already_user(self, user):
        self.student.user.idx = user.idx
        is_student = self.repo_user.is_student_with_user_id(self.student.user)
        if is_student:
            raise StudentAlreadyRegisteredError(self.student)
        else:
            return self.repo_student.save_student_without_user(self.student)

    def _register_not_yet_user(self, student):
        return self.repo_student.save_student_with_user(student)
