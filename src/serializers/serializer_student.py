from src.config import LoggerFactory
from src.helpers.serializer import id_to_str


class StudentSerializer:
    logger = LoggerFactory.get_logger(__name__)

    @staticmethod
    def student_list(students):
        return [{"idAlumno": id_to_str(s.idx),
                 "codigo": s.user.code,
                 "nombres": f"{s.user.names} {s.user.father_lastname} {s.user.mother_lastname}"}
                for s in students]

    @staticmethod
    def student_list_by_unit(response):
        students = response.students
        return [{"idUnidad": id_to_str(u.idx),
                 "alumno": {
                     "idAlumno": id_to_str(s.idx),
                     "nombre": f"{s.user.names} {s.user.father_lastname} {s.user.mother_lastname}",
                     "codigo": s.user.code,
                     "correo": s.user.email}} for u, s in students]

    @classmethod
    def list_students_for_moment_tutorship(cls, response):
        return [
            {
                "idUnidad": id_to_str(student.unit.idx),
                "nombreUnidad": student.unit.name,
                "alumno": {
                    "idAlumno": id_to_str(student.idx),
                    "nombre": student.user.fullname(),
                    "codigo": student.user.code,
                    "correo": student.user.email
                }
            } for student in response.students]

    @staticmethod
    def student_data(student):
        return {
            "alumno": {
                "codigo": student.user.code,
                "nombres": student.user.names,
                "apellidoPat": student.user.father_lastname,
                "apellidoMat": student.user.mother_lastname,
                "telefono": student.user.phone_number,
                "idUnidad": id_to_str(student.id_unit),
                "infoAdicional": [{
                    # TODO (CSP-5) Define name on additional info
                    # TODO (CSP-6) Define response structure
                    "nombre": None,
                    "url": i.archive
                } for i in student.additional_information]
            }
        }

    @staticmethod
    def serialize_student_id(response):
        student = response.student
        return {
            "idStudent": id_to_str(student.idx),
            "idUsuario": id_to_str(student.user.idx),
            "correo": student.user.email,
            "codigo": student.user.code,
            "nombres": student.user.names,
            "apellidoPat": student.user.father_lastname,
            "apellidoMat": student.user.mother_lastname,
            "telefono": student.user.phone_number,
            "idUnidad": id_to_str(student.unit.idx),
            "nombreUnidad": student.unit.name
        }

    @staticmethod
    def student_list_unit(response):
        students = response.students
        return [{
            "idAlumno": id_to_str(s.idx),
            "codigo": s.user.code,
            "nombres": s.user.fullname(),
            "nombreUnidad": s.unit.name
        }
            for s in students]

    @staticmethod
    def additional_info(response):
        additional_info = response.additional_info

        return [
            {
                "idInfoAdicional": id_to_str(info.idx),
                "nombre": info.name,
                "contenido": info.archive,
                "fechaCreacion": info.date_created.isoformat()
            } for info in additional_info]

    @staticmethod
    def info_file(response):
        info_file = response.additional_info

        return {
            "idInfoAdicional": id_to_str(info_file.idx),
            "nombre": info_file.name,
            "contenido": info_file.archive,
            "fechaCreacion": info_file.date_created.isoformat()
        }

    @staticmethod
    def serialize_register_bulk_student_response(response):
        student_not_register_filter_unit_code = response.students_not_in_unit
        students_not_register_filter_user = response.students_already_exists
        students_not_register_filter_code = response.students_conflict_code
        json_student_not_register_filter_unit_code = [{
            "correo": s.user.email,
            "codigo": s.user.code,
            "nombres": s.user.names,
            "apellidoPat": s.user.father_lastname,
            "apellidoMat": s.user.mother_lastname,
            "telefono": s.user.phone_number,
            "codUnidad": s.unit.code,
            "mensaje": "Error. No se pudo registrar al alumno porque no pertenece a sus unidades designadas"
        } for s in student_not_register_filter_unit_code]
        json_students_not_register_filter_user = [{
            "correo": s.user.email,
            "codigo": s.user.code,
            "nombres": s.user.names,
            "apellidoPat": s.user.father_lastname,
            "apellidoMat": s.user.mother_lastname,
            "telefono": s.user.phone_number,
            "codUnidad": s.unit.code,
            "mensaje": "Error. No se pudo registrar al alumno porque ya se encuentra registrado"
        } for s in students_not_register_filter_user]
        json_students_not_register_filter_code = [{
            "correo": s.user.email,
            "codigo": s.user.code,
            "nombres": s.user.names,
            "apellidoPat": s.user.father_lastname,
            "apellidoMat": s.user.mother_lastname,
            "telefono": s.user.phone_number,
            "codUnidad": s.unit.code,
            "mensaje": "Error. No se pudo registrar al alumno porque su codigo ya se encuentra registrado"
        } for s in students_not_register_filter_code]
        return json_student_not_register_filter_unit_code + json_students_not_register_filter_user + json_students_not_register_filter_code
