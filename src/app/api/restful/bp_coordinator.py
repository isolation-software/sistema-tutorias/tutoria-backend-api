from flask_restful import Resource
from flask import request

from src.app.auth.handler import jwt_required
from src.config import LoggerFactory
from src.controllers import CoordinatorController
from src.helpers import constants


class CoordinatorsResource(Resource):
    logger = LoggerFactory.get_logger(__name__)

    @jwt_required
    def get(self):
        search = request.args.get('search', default=constants.EMPTY_STRING, type=str)
        page = request.args.get('page', default=constants.DEFAULT_PAGE, type=int)
        pagesize = request.args.get('pagesize', default=constants.DEFAULT_PAGESIZE, type=int)
        response = CoordinatorController.list_coordinators(search=search, page=page, pagesize=pagesize)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        data = request.get_json()
        response = CoordinatorController.register_coordinator(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class CoordinatorAuthResource(Resource):
    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class CoordinatorResource(Resource):
    @jwt_required
    def get(self, id_coordinator):
        response = CoordinatorController.get_coordinator(id_coordinator=id_coordinator)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST
