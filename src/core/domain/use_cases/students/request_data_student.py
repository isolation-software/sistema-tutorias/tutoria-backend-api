from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RequestDataStudentUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        # request data
        self.id_student = req.id_student

    @catch_errors
    def execute(self):
        student = self.repo_student.read_data_student_id(self.id_student)
        self.res = Response(student=student)
        self.success = RequestDataStudentSuccess

class RequestDataStudentSuccess(Success):
    code = info_success_logic.REQUEST_DATA_STUDENT_OK_CODE
    message = info_success_logic.REQUEST_DATA_STUDENT_OK_MESSAGE


