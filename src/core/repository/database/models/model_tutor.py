from uuid import uuid4

from sqlalchemy import Column, ForeignKey, Boolean
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import Tutor
from src.core.repository.database.connection import Base
from src.core.repository.database.models import UserModel
from src.helpers import constants as cons


class TutorModel(Base):
    __tablename__ = cons.TABLE_NAME_TUTOR

    id_tutor = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_user = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_USER}.{cons.TABLE_ID_USER}"))
    has_access_to_additional_information = Column(Boolean, default=False)

    user = relationship(UserModel)

    def to_entity(self):
        return Tutor(
            idx=self.id_tutor,
            user=self.user.to_entity(),
            has_access_to_additional_information=self.has_access_to_additional_information
        )
