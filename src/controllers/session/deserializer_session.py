import dateutil.parser

from src.core.domain.entities import Session, SupportUnity, Tutor
from src.core.domain.entities.action_plan import ActionPlan
from src.core.domain.entities.enum.session_states import SessionStates
from src.core.domain.entities.indicators import Indicators
from src.core.domain.entities.unit_action_plan import UnitActionPlan
from src.helpers.serializer import str_to_id


class SessionDeserializer:

    @staticmethod
    def session(json, id_session=None):
        session_data = json["sesion"]
        ap_data = json.get("planDeAccion")

        session = Session(
            idx=str_to_id(id_session) if id_session else None,
            comments=session_data.get("comentario"),
            derivation=session_data.get("derivacion"),
            support_unit=SupportUnity(idx=str_to_id(session_data["idUnidadDerivada"]))
            if session_data.get("idUnidadDerivada") else None,
            state=SessionStates(session_data["idEstado"]) if session_data.get("idEstado") else SessionStates.PENDING,
            tutor=Tutor(idx=session_data["idTutorAsignado"]) if session_data.get("idTutorAsignado") else None,
            start_time=dateutil.parser.parse(session_data["fechaInicio"]) if session_data.get("fechaInicio") else None,
            end_time=dateutil.parser.parse(session_data["fechaFin"]) if session_data.get("fechaFin") else None,
        )

        action_plan = ActionPlan(
            accion_units=[
                UnitActionPlan(
                    description=au["nombre"],
                    state=au["completado"]
                ) for au in ap_data]
        ) if ap_data else None
        return session, action_plan

    @staticmethod
    def session_individual(json):
        start_date = json["fechaInicio"]
        id_assigned_tutor = json["idTutorAsignado"]
        return start_date, id_assigned_tutor

    @staticmethod
    def register_indicator_of_session(json):
        token = json.get("token")
        indicators_data = json.get("data")
        indicators = Indicators(
            indicator1=indicators_data.get("indicador1"),
            indicator2=indicators_data.get("indicador2"),
            indicator3=indicators_data.get("indicador3"),
            indicator4=indicators_data.get("indicador4"),
            indicator5=indicators_data.get("indicador5"),
            indicator6=indicators_data.get("indicador6")
        )
        return token, indicators