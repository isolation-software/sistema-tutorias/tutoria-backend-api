from sqlalchemy import and_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import MultipleResultsFound

from src.config import LoggerFactory
from src.core.domain.entities import User
from src.core.domain.errors.logic_errors import UserNotFoundInSystemError
from src.core.repository.errors.sqla_errors import UserIntegrityError
from src.core.repository.common import Repository
from src.core.repository.database.models import UserModel, StudentModel, CoordinatorModel, TutorModel, \
    AdministratorModel, TutorPerUnitModel, UnitModel
from src.core.repository.errors import catch_repo_errors, MultipleResultsError
from src.helpers import constants
from src.helpers.role_model import RoleModels


class UserRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def save_picture(self, email, picture_url):
        self.logger.debug(f'salvando imagen de {picture_url}')
        user_row = self.session.query(UserModel).filter(UserModel.email == email).one()
        user_row.picture_url = picture_url
        self.session.commit()

    @catch_repo_errors
    def save_user(self, user):
        row_user = UserModel(
            email=user.email,
            code=user.code,
            names=user.names,
            father_lastname=user.father_lastname,
            mother_lastname=user.mother_lastname,
            phone_number=user.phone_number
        )

        try:
            self.session.add(row_user)
            self.session.flush()
        except IntegrityError as e:
            self.session.rollback()
            param = ''
            value = ''
            existing_email = self.session.query(UserModel).filter(UserModel.email == row_user.email).one_or_none()
            if not existing_email:
                existing_code = self.session.query(UserModel).filter(UserModel.code == row_user.code).one_or_none()
                if existing_code:
                    param = "código"
                    value = row_user.code
            else:
                param = "correo"
                value = row_user.email

            raise UserIntegrityError(param, value, e)
        else:
            self.session.commit()
            self.session.refresh(row_user)
            user.idx = row_user.id_user

            return user

    @catch_repo_errors
    def update_user(self, user, role, idx):
        self.logger.debug(f"Repo - iniciando la actualización del {role.value.lower()} con id {idx}")
        model, model_idx = RoleModels.role_model(role)

        row_user = self.session.query(UserModel) \
            .join(model, model.id_user == UserModel.id_user) \
            .filter(model_idx == idx).one_or_none()

        try:
            row_user.email = user.email if user.email else row_user.email
            row_user.code = user.code if user.code else row_user.code
            row_user.names = user.names if user.names else row_user.names
            row_user.father_lastname = user.father_lastname if user.father_lastname else row_user.father_lastname
            row_user.mother_lastname = user.mother_lastname if user.mother_lastname else row_user.mother_lastname
            row_user.phone_number = user.phone_number if user.phone_number else row_user.phone_number

            self.session.flush()
        except IntegrityError as e:
            self.session.rollback()
            param = ''
            value = ''
            existing_email = self.session.query(UserModel).filter(UserModel.email == row_user.email).one_or_none()
            if not existing_email:
                existing_code = self.session.query(UserModel).filter(UserModel.code == row_user.code).one_or_none()
                if existing_code:
                    param = "código"
                    value = row_user.code
            else:
                param = "correo"
                value = row_user.email

            raise UserIntegrityError(param, value, e)
        else:
            self.session.commit()

        self.logger.debug(f"Repo - terminando la actualización del {role.value.lower()} con id {idx}")

        return row_user.to_entity()

    def read_user_by_email_if_exists(self, user, with_password=False):
        """ Method that searchs for a user with the email passed inside the User object.
            If found, return a new User object with its information. If not, returns
            None.
            :param user: User
            :param with_password: Boolean
            :return User object with all information
        """
        try:
            row_user = self.session.query(UserModel).filter(UserModel.email == user.email).one_or_none()
            if not with_password:
                return None if row_user is None else row_user.to_entity()
            else:
                return (None, None, None) if row_user is None else (
                    row_user.to_entity(), row_user.hash_password, row_user.hash_salt)
        except MultipleResultsFound as e:
            self.logger.error(exc_info=True)
            raise MultipleResultsError(model=UserModel, cause=e)

    @staticmethod
    def _build_row_to_object(row):
        return User(
            idx=row.id_user,
            names=row.names,
            father_lastname=row.father_lastname,
            mother_lastname=row.mother_lastname,
            email=row.email,
            code=row.code,
            phone_number=row.phone_number
        )

    @catch_repo_errors
    def is_student_with_user_id(self, user):
        row_count = self.session.query(StudentModel) \
            .filter(StudentModel.id_user == user.idx) \
            .count()
        return row_count == constants.ONE

    @catch_repo_errors
    def is_coordinator_with_user_id(self, user):
        row_count = self.session.query(CoordinatorModel) \
            .filter(CoordinatorModel.id_user == user.idx) \
            .count()
        return row_count == constants.ONE

    @catch_repo_errors
    def is_tutor_with_user_id(self, user):
        row_count = self.session.query(TutorModel) \
            .filter(TutorModel.id_user == user.idx) \
            .count()
        return row_count == constants.ONE

    @catch_repo_errors
    def is_administrator_with_user_id(self, user):
        row_count = self.session.query(AdministratorModel) \
            .filter(AdministratorModel.id_user == user.idx) \
            .count()
        return row_count == constants.ONE

    @catch_repo_errors
    def get_student_with_user_id_if_exists(self, id_user):
        row = self.session.query(StudentModel) \
            .filter(StudentModel.id_user == id_user) \
            .one_or_none()
        return row.to_entity() if row else None

    @catch_repo_errors
    def get_coordinator_with_user_id_if_exists(self, id_user):
        row = self.session.query(CoordinatorModel) \
            .filter(CoordinatorModel.id_user == id_user) \
            .one_or_none()
        return row.to_entity() if row else None

    @catch_repo_errors
    def get_tutor_with_user_id_if_exists(self, id_user):
        row = self.session.query(TutorModel) \
            .filter(TutorModel.id_user == id_user) \
            .one_or_none()
        return row.to_entity() if row else None

    @catch_repo_errors
    def get_administrator_with_user_id_if_exists(self, id_user):
        row = self.session.query(AdministratorModel) \
            .filter(AdministratorModel.id_user == id_user) \
            .one_or_none()
        return row.to_entity() if row else None

    @catch_repo_errors
    def read_roles(self, id_user):
        administrator = self.get_administrator_with_user_id_if_exists(id_user)
        coordinator = self.get_coordinator_with_user_id_if_exists(id_user)
        tutor = self.get_tutor_with_user_id_if_exists(id_user)
        student = self.get_student_with_user_id_if_exists(id_user)
        return administrator, coordinator, tutor, student

    def read_given_roles_by_email(self, email):
        row_user = self.session.query(UserModel,
                                      AdministratorModel.id_administrator,
                                      CoordinatorModel.id_coordinator,
                                      TutorModel.id_tutor,
                                      StudentModel.id_student) \
            .outerjoin(AdministratorModel, AdministratorModel.id_user == UserModel.id_user) \
            .outerjoin(CoordinatorModel, CoordinatorModel.id_user == UserModel.id_user) \
            .outerjoin(TutorModel, TutorModel.id_user == UserModel.id_user) \
            .outerjoin(StudentModel, StudentModel.id_user == UserModel.id_user) \
            .filter(UserModel.email == email).first()
        if row_user:
            user = row_user[0].to_entity() if row_user[0] else None
            ids_roles = row_user[1:]
            return user, ids_roles
        else:
            return None, None

    @catch_repo_errors
    def list_users(self, students):
        rows = self.session.query(StudentModel) \
            .join(UserModel) \
            .filter(StudentModel.id_student.in_(students)) \
            .all()
        return [row.to_entity() for row in rows]

    @catch_repo_errors
    def get_users(self):
        row_user = self.session.query(UserModel).all()
        return [r.to_entity() for r in row_user]

    @catch_repo_errors
    def is_activated_user(self, email):
        row_user = self.session.query(UserModel).filter(UserModel.email == email).one_or_none()
        return row_user.is_activate if row_user else None

    @catch_repo_errors
    def is_already_tutor_in_unit(self, unit, user):
        row_user = self.session.query(UserModel) \
            .join(TutorModel, TutorModel.id_user == UserModel.id_user) \
            .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor == TutorModel.id_tutor) \
            .join(UnitModel, UnitModel.id_unit == TutorPerUnitModel.id_unit) \
            .filter(and_(
            UnitModel.id_unit == unit.idx,
            UserModel.id_user == user.idx
        )).one_or_none()
        return bool(row_user)

    @catch_repo_errors
    def update_password_user(self, email, hashed, salt):
        row_user = self.session.query(UserModel).filter(UserModel.email == email).one_or_none()
        if row_user:
            row_user.hash_password = hashed
            row_user.hash_salt = salt
            row_user.is_activate = True
            self.session.commit()
        else:
            raise UserNotFoundInSystemError(email=email)

    @catch_repo_errors
    def get_auth_password(self, email):
        row_user = self.session.query(UserModel).filter(UserModel.email == email).one_or_none()
        return row_user.hash_password, row_user.hash_salt
