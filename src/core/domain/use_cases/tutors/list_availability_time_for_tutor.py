from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Success, Response
from src.helpers import info_success_logic


class ListAvailabilityTimeForTutorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_availability_date=None):
        super().__init__(errors, req)
        self.repo_availability_date = repo_availability_date or self.RepoFactory.AvailabilityDateRepo()
        self.tutor = req.tutor
        self.list_non_approved = req.list_non_approved
        self.start_date = req.start_date
        self.end_date = req.end_date

    @catch_errors
    def execute(self):
        schedule = self.repo_availability_date.\
            list_availability_time_for_tutor(self.tutor, self.start_date, self.end_date) if \
            self.list_non_approved else \
            self.repo_availability_date\
                .list_availability_time_for_tutor_only_approved(self.tutor, self.start_date, self.end_date)
        self.res = Response(schedule=schedule)
        self.success = ListAvailabilityTimeForTutorSuccess


class ListAvailabilityTimeForTutorSuccess(Success):
    code = info_success_logic.AVAILABILITY_TIME_SEARCH_OK_CODE
    message = info_success_logic.AVAILABILITY_TIME_SEACRH_OK_MESSAGE
