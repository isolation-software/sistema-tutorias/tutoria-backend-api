from src.core.domain.entities import User, Unit
from src.core.domain.entities.enums import Roles
from src.helpers import constants
from src.helpers.serializer import str_to_id


class UserDeserializer:

    @staticmethod
    def email_validation_params_to_objects(param_email):
        return User(email=param_email)

    @classmethod
    def get_role_data(cls, data):
        role_data = data.get("alumno") or data.get("tutor") or data.get("coordinador") or data.get("administrador")
        role = Roles(list(data.keys())[constants.FIRST].capitalize())

        return role_data, role

    @classmethod
    def user_role_access_and_unit(cls, data, idx):
        data, role = cls.get_role_data(data)
        user = User(names=data.get("nombres"),
                    father_lastname=data.get("apellidoPaterno"),
                    mother_lastname=data.get("apellidoMaterno"),
                    email=data.get("correo"),
                    phone_number=data.get("telefono"),
                    code=data.get("codigo"))

        id_unit = data.get("idUnidad")

        unit = Unit(idx=str_to_id(id_unit)) if role is Roles.STUDENT else None
        has_access_to_additional_information = data.get("tienePermisoAccesoInfoAdicional", False) \
            if role is Roles.TUTOR else None

        return user, role, str_to_id(idx), has_access_to_additional_information, unit

    @classmethod
    def register_user(cls, data):
        email = data.get("correo")
        return email

    @classmethod
    def confirmation_user(cls, data):
        token = data.get("token")
        password = data.get("contrasenia")
        return token, password

    @classmethod
    def validate_login(cls, data):
        email = data.get("email")
        password = data.get("password")
        return email, password

    @classmethod
    def validate_google_login(cls, data):
        code = data.get("code")
        return code
