from src.helpers.serializer import id_to_str


class SupportUnitSerializer:

    @classmethod
    def serialize_list_support_units_response(cls,res):
        support_units = res.support_units
        return [
            {
                "idUnidadSoporte": id_to_str(support_unit.idx),
                "nombre": support_unit.name,
                "email":support_unit.email,
                "telefono":support_unit.phone
            }
            for support_unit in support_units
        ]