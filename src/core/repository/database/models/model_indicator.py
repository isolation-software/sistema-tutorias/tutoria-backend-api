from uuid import uuid4

from sqlalchemy import Column, ForeignKey, String, Boolean, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities.indicators import Indicators
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class IndicatorModel(Base):
    __tablename__ = cons.TABLE_NAME_INDICATOR

    id_indicator = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    indicator_1 = Column(Integer, comment="Satisfaccion con el acompañamiento del programa de tutoria del 1 al 5")
    indicator_2 = Column(Integer, comment="Utilidad del programa de tutoría del 1 al 5")
    indicator_3 = Column(Boolean, comment="Utilizo las recomendaciones para toma de decisiones")
    indicator_4 = Column(Boolean, comment="EL programa solucionó su situación")
    indicator_5 = Column(Boolean, comment="Recomendaría el programa de tutoría a sus pares")
    indicator_6 = Column(String(cons.STRING_LENGTH), comment="Motivos de consulta")
    id_session = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_SESSION}.{cons.TABLE_ID_SESSION}"))

    session = relationship("SessionModel")

    def to_entity(self):
        return Indicators(
            idx=self.id_indicator,
            indicator1=self.indicator_1,
            indicator2=self.indicator_2,
            indicator3=self.indicator_3,
            indicator4=self.indicator_4,
            indicator5=self.indicator_5,
            indicator6=self.indicator_6,
            session=self.session.to_entity() if self.session else None
        )