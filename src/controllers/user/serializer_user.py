import jwt

from src.config import LoggerFactory
from src.helpers.serializer import id_to_str


class UserSerializer:

    logger = LoggerFactory.get_logger(__name__)

    @staticmethod
    def serialize_data_user(user):
        """ Serializer method that serialize an User objet to a dictionary.
            If object passed as argument is None, then, the method returns None.
            :param user: User or None
            :return User dictionary or None
            """
        if not user:
            return None
        return {
            # "idUsuario": id_to_str(user.idx),
            "correo": user.email,
            "codigo": user.code,
            "nombres": user.names,
            "apellidoPat": user.father_lastname,
            "apellidoMat": user.mother_lastname,
            "telefono": user.phone_number
        }

    @staticmethod
    def serialize_data_validate_email_for_student(res_user, res_has_role):
        return {"usuarioExiste": res_user is not None,
                "esAlumno": res_has_role,
                "usuario": UserSerializer.serialize_data_user(res_user)}

    @staticmethod
    def serialize_data_validate_email_for_coordinator(res_user, res_has_role):
        return {"usuarioExiste": res_user is not None,
                "esCoordinador": res_has_role,
                "usuario": UserSerializer.serialize_data_user(res_user)}

    @staticmethod
    def serialize_data_validate_email_for_tutor(res_user, res_has_role, res_same_unit):
        return {"usuarioExiste": res_user is not None,
                "esTutor": res_has_role,
                "esTutorEnLaUnidad": res_same_unit,
                "usuario": UserSerializer.serialize_data_user(res_user)}

    @classmethod
    def serialize_data_list_roles_for_user(cls, res):
        user = res.user
        admin, coord, tutor, student = res.roles
        coord_units = res.coord_units
        cls.logger.info(f"unidades: {coord_units}")
        return {
            "usuario": {
                "idUsuario": id_to_str(user.idx),
                "codigo": user.code,
                "correo": user.email,
                "nombres": user.names,
                "apellidoPat": user.father_lastname,
                "apellidoMat": user.mother_lastname,
                "telefono": user.phone_number,
                "avatar": user.picture_url
            },
            "idAdministrador": id_to_str(admin.idx) if admin else None,
            "idCoordinador": id_to_str(coord.idx) if coord else None,
            "idTutor": id_to_str(tutor.idx) if tutor else None,
            "idAlumno": id_to_str(student.idx) if student else None,
            "tutor": {
                "idTutor": id_to_str(tutor.idx),
                "tienePermiso": tutor.has_access_to_additional_information
            } if tutor else None,
            "unidadAlumno": {
                "idUnidad": id_to_str(student.unit.idx),
                "nombre": student.unit.name
            } if student else None,
            "unidadesCoordinador": [
                {
                    "idUnidad": id_to_str(unit.idx),
                    "codigo": unit.code,
                    "nombre": unit.name,
                    "tipo": unit.typex.value,
                    "idUnidadPadre": id_to_str(unit.father_unit.idx) if unit.father_unit else None
                }
                for unit in coord_units] if coord_units is not None else None
        }

    @classmethod
    def serialize_data_validate_login(cls, res):
        token = res.token
        # token = jwt.decode(token, FlaskConfig.SECRET_KEY)
        return {
            "token": token
        }

    @classmethod
    def serialize_validate_google_login(cls, res):
        token = res.token
        email = res.email
        return {
            "token": token,
            "email": email
        }
