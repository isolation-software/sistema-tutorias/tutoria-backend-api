from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ListStateTutorshipUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        # request data

    @catch_errors
    def execute(self):
        states_tutorship = self.repo_tutorship.read_state_tutorship()
        self.res = Response(states_tutorship=states_tutorship)
