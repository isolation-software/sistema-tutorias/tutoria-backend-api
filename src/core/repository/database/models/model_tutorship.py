from uuid import uuid4

from sqlalchemy import Column, ForeignKey, String, Integer, Boolean
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import Tutorship
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class TutorshipModel(Base):
    __tablename__ = cons.TABLE_NAME_TUTORSHIP

    id_tutorship       = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    name               = Column(String(cons.STRING_LONG_LENGHT), nullable=False)
    description        = Column(String(cons.STRING_LONG_LENGHT), nullable=False)
    sessions_number    = Column(Integer, nullable=True)
    is_individual      = Column(Boolean, nullable=False)
    is_tutor_permanent = Column(Boolean, nullable=False)
    is_obligatory      = Column(Boolean, nullable=False)
    is_tutor_assigned  = Column(Boolean, nullable=False)
    id_unit            = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_UNIT}.{cons.TABLE_ID_UNIT}"))

    unit = relationship("UnitModel")

    def to_entity(self):
        """
        Método que convierte el objeto TutorshipModel a un objeto de la entidad Tutorship.
        Solo se están tomando en cuenta los siguientes atributos de la entidad:
            - idx
            - name
            - description
            - sessions_number
        :return: Tutorship object
        """
        return Tutorship(
            idx=self.id_tutorship,
            name=self.name,
            description=self.description,
            sessions_number=self.sessions_number,
            unit=self.unit.to_entity() if self.unit else None,
            is_individual=self.is_individual,
            is_tutor_permanent=self.is_tutor_permanent,
            is_obligatory=self.is_obligatory,
            is_tutor_assigned=self.is_tutor_assigned
        )
