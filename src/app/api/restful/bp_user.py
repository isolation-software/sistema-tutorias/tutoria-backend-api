from flask import request
from flask_restful import Resource

from src.app.auth.handler import jwt_required
from src.config import LoggerFactory
from src.controllers import UserController
from src.helpers import constants


logger = LoggerFactory.get_logger(__name__)


class UserValidationStudentResource(Resource):
    @jwt_required
    def get(self):
        email = request.args.get("correo")
        response = UserController.validate_email_for_student(email)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class UserValidationCoordinatorResource(Resource):
    @jwt_required
    def get(self):
        email = request.args.get("correo")
        response = UserController.validate_email_for_coordinator(email)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class UserValidationTutorResource(Resource):
    @jwt_required
    def get(self):
        email = request.args.get("correo")
        id_unit = request.args.get("idUnidad")
        response = UserController.validate_email_for_tutor(email, id_unit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class UserRolesResource(Resource):
    @jwt_required
    def get(self):
        email = request.args.get("correo")
        logger.info(f'Petición iniciada de roles para el usuario con correo {email}')
        response = UserController.list_roles_for_user(email)
        if response.status is constants.OK:
            logger.debug('Roles encontrados')
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UserResource(Resource):
    @jwt_required
    def put(self, idx):
        data = request.get_json()
        response = UserController.update_user(data, idx)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UserRegister(Resource):
    def post(self):
        data = request.get_json()
        response = UserController.register_user(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UserConfirmation(Resource):
    # NO SE VALIDA TOKEN PORQUE VIENE EN JSON, NO REQUIERE LOGUEARSE
    def post(self):
        data = request.get_json()
        response = UserController.confirmation_user(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UserValidateLogin(Resource):
    def post(self):
        data = request.get_json()
        response = UserController.validate_login(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UserValidateGoogleLogin(Resource):
    def post(self):
        data = request.get_json()
        response = UserController.validate_google_login(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

