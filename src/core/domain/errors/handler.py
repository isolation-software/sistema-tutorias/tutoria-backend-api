import time

from src.config import LoggerFactory
from src.core.domain.errors.logic_errors import LogicError, GenericLogicError
from src.core.repository.errors import RepositoryError
from src.helpers import constants


logger = LoggerFactory.get_logger(__name__)


def catch_errors(execute):
    """
    decorator that wraps a use-case and execute it.
    If an error appears, it's catched and keep it
    in errors list of use-case.
        - LogicError is shown in error log (not shown before, generated in USECASE) & kept in errors usecase list
        - RepositoryErrors are not shown in error log (shown before, generated in REPOSITORY),
          but kept in errors usecase list. This kind of errors should be shown in the repository error handler.
        - Exceptions are shown in error log (not shown before, generated in USECASE) & kept in errors usecase list
    :param execute:
    :return:
    """
    def execute_and_catch(*args, **kwargs):
        usecase = args[constants.ZERO]
        start = time.time()
        logger.debug(f"Nivel use-case. Iniciando atención {type(usecase).__name__}")
        try:
            execute(*args, **kwargs)

        except LogicError as logic_e:
            usecase.errors.append(logic_e)
            logger.error(logic_e.message, exc_info=True)

        except RepositoryError as repo_e:
            usecase.errors.append(repo_e)
            logger.error(repo_e.message, exc_info=False)

        except Exception as e:
            logger.error(e, exc_info=True)
            logic_error = GenericLogicError(cause=e)
            usecase.errors.append(logic_error)
            logger.error(logic_error, exc_info=False)

        finally:
            end = time.time()
            usecase.close_all()
            logger.debug(f"Nivel use-case. Tiempo transcurrido: {(end - start):.2f} segundos(s)")

    return execute_and_catch
