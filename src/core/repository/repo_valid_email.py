from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import UniversityModel, ValidEmailModel
from src.core.repository.errors.handler import catch_repo_errors


class ValidEmailRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def register_valid_email_pattern(self, emails):
        self.logger.debug("Repo - Iniciando actualización de correos válidos")
        university = self.session.query(UniversityModel).one_or_none()
        self.session.query(ValidEmailModel).delete()
        valid_email_rows = [ValidEmailModel(
            id_university=university.id_university if university else None,
            prefix=email.prefix,
            domain=email.domain or university.domain,
            sponsored_tld=email.sponsored_tld,
            top_level_domain=email.top_level_domain
        ) for email in emails]
        self.session.bulk_save_objects(valid_email_rows)
        self.session.commit()
        self.logger.debug("Repo - Terminando actualización de correos válidosn")

    @catch_repo_errors
    def get_valid_email_pattern(self):
        row_emails = self.session.query(ValidEmailModel).all()
        return [sr.to_entity() for sr in row_emails]
