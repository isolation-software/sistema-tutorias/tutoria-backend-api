from sqlalchemy import or_, and_
from sqlalchemy.exc import IntegrityError

from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import UnitModel, TutorPerUnitModel, TutorModel
from src.core.repository.errors import UnitNotFoundError
from src.core.repository.errors.handler import catch_repo_errors
from src.core.repository.errors.sqla_errors import UnitIntegrityError
from src.helpers.formater import no_double_space


class UnitRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def list_units_per_tutor_all(self, tutor):
        unit_rows = self.session.query(UnitModel) \
            .filter(or_(
                UnitModel.id_unit == TutorPerUnitModel.id_unit,
                UnitModel.id_father_unit == TutorPerUnitModel.id_unit
            )) \
            .filter(TutorPerUnitModel.id_tutor == TutorModel.id_tutor) \
            .filter(TutorModel.id_tutor == tutor.idx) \
            .order_by(UnitModel.code) \
            .all()
        return [row.to_entity() for row in unit_rows]

    @catch_repo_errors
    def read_units(self):
        unit_rows = self.session.query(UnitModel).order_by(UnitModel.code).all()
        units = [r.to_entity() for r in unit_rows]
        return units

    @catch_repo_errors
    def list_units_per_tutor(self, tutor):
        self.logger.debug("Iniciando con la búsqueda en BD de unidades por tutor en repositorio")
        tutor_x_unit_rows = self.session.query(TutorPerUnitModel) \
            .join(TutorModel) \
            .join(UnitModel) \
            .filter(TutorPerUnitModel.id_tutor == tutor.idx).all()
        self.logger.debug("Terminando con la búsqueda en BD de unidades por tutor en repositorio")
        return [row.unit.to_entity() for row in tutor_x_unit_rows]

    @catch_repo_errors
    def get_unit_by_id(self, id_unit):
        self.logger.debug("Iniciando con la búsqueda en BD de unidad por su id en repositorio")
        unit_row = self.session.query(UnitModel) \
            .filter(UnitModel.id_unit == id_unit) \
            .one_or_none()
        if unit_row is None:
            raise UnitNotFoundError()
        unit = unit_row.to_entity()
        self.logger.debug("Terminando con la búsqueda en BD de unidad por su id en repositorio")
        return unit

    @catch_repo_errors
    def read_children_units_by_father_id(self, unit):
        self.logger.debug("Iniciando con la búsqueda en BD de programas por unidad padre en repositorio")
        unit_rows = self.session.query(UnitModel) \
            .filter(UnitModel.id_father_unit == unit.idx) \
            .order_by(UnitModel.code) \
            .all()
        self.logger.debug("Terminando con la búsqueda en BD de programas por unidad padre en repositorio")
        return [unit_row.to_entity() for unit_row in unit_rows]

    @catch_repo_errors
    def save_unit_program(self, program):
        try:
            unit_row = UnitModel(
                name=program.name,
                code=program.code,
                typex=program.typex.value,
                id_father_unit=program.father_unit.idx,
                id_coordinator=program.coordinator.idx
            )
            self.session.add(unit_row)
            self.session.flush()
        except IntegrityError as e:
            raise UnitIntegrityError(e)
        else:
            self.session.commit()
            self.session.refresh(unit_row)
            program.idx = unit_row.id_unit
            return program

    @catch_repo_errors
    def save_faculty(self, faculty):
        try:
            row_unit = UnitModel(name=faculty.name,
                                 id_coordinator=faculty.coordinator.idx,
                                 code=faculty.code,
                                 typex=faculty.typex.value
                                 )
            self.session.add(row_unit)
            self.session.flush()
        except IntegrityError as e:
            raise UnitIntegrityError(e)
        else:
            self.session.commit()
            self.session.refresh(row_unit)
            return row_unit

    @catch_repo_errors
    def read_faculties(self, search="", limit=10):
        search = "%{}%".format(no_double_space(search))
        faculty_rows = self.session.query(UnitModel) \
            .filter(and_(or_(
                UnitModel.name.ilike(search),
                UnitModel.code.ilike(search)
            ),
                UnitModel.typex == "Facultad")) \
            .order_by(UnitModel.code) \
            .all()
        faculties = [r.to_entity() for r in faculty_rows]
        return faculties

    @catch_repo_errors
    def list_units_by_coordinator_id(self, id_coordinator):
        rows_unit = self.session.query(UnitModel)\
            .filter(UnitModel.id_coordinator == id_coordinator)\
            .order_by(UnitModel.code) \
            .all()
        return [row.to_entity() for row in rows_unit]

    @catch_repo_errors
    def list_units_ids_by_coordinator_id(self, id_coordinator):
        unit_ids = self.session.query(UnitModel.id_unit) \
            .filter(UnitModel.id_coordinator == id_coordinator) \
            .all()
        return unit_ids

    @catch_repo_errors
    def update_unit(self, unit):
        try:
            row_unit = self.session.query(UnitModel).filter(UnitModel.id_unit == unit.idx).one_or_none()
            row_unit.name = unit.name if unit.name else row_unit.name
            row_unit.code = unit.code if unit.code else row_unit.code
            row_unit.typex = unit.typex.value if unit.typex else row_unit.typex
            row_unit.id_father_unit = unit.father_unit.idx if unit.father_unit else row_unit.id_father_unit
            row_unit.id_coordinator = unit.coordinator.idx if unit.coordinator else row_unit.id_coordinator
            self.session.flush()
        except IntegrityError as e:
            raise UnitIntegrityError(e)
        else:
            self.session.commit()
            return unit
