from src.app.auth.handler import UserToken
from src.config import LoggerFactory
from src.config.config_oauth import OAuthConfig
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import GoogleAuthenticationAccountError, UserNotFoundInSystemError
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic, constants
from oauth2client import client


class ValidateGoogleLoginUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_user=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo(session=self.repo_user.session)
        self.code      = req.code

    @catch_errors
    def execute(self):
        # LOGIC
        self.logger.info('Inicio de validación de usuario con cuenta de Google')
        email, picture_url = self.authenticate_user_code(code=self.code)
        self.logger.debug(f'Usuario autenticado correctamente: {email}')

        data  = self.get_user_data_by_email(email=email)
        self.logger.debug(f'Datos obtenidos del usuario')

        self.repo_user.save_picture(email=email, picture_url=picture_url)

        token = self.generate_token_from_user_data(data=data)
        self.logger.info(f'Token de acceso generado exitosamente')

        # RESPONSE
        self.res     = Response(token=token, email=email)
        self.success = ValidateGoogleLoginSuccess

    def authenticate_user_code(self, code):
        try:
            credentials = client.credentials_from_code(
                client_id=OAuthConfig.ID_CLIENT,
                client_secret=OAuthConfig.SECRET_CLIENT,
                scope=OAuthConfig.GOOGLE_SCOPE,
                code=code)
            self.logger.info('Credenciales obtenidas desde Google Api')
        except client.FlowExchangeError as e:
            self.logger.error(
                "No es posible obtener las credenciales del cliente Google, las credenciales para el uso de Google Api pueden ser erróneas o código de usuario es incorrecto o nulo")
            self.logger.error(msg=e, exc_info=True)
            raise GoogleAuthenticationAccountError(cause=e)
        return credentials.id_token['email'], credentials.id_token['picture']

    def get_user_data_by_email(self, email):
        user, id_roles = self.repo_user.read_given_roles_by_email(email=email)
        if not user:
            raise UserNotFoundInSystemError(email=email)
        ids_unit = self.repo_unit.list_units_ids_by_coordinator_id(id_roles[constants.POSITION_COORD])\
            if id_roles[constants.POSITION_COORD]\
            else None
        return user, id_roles, ids_unit

    def generate_token_from_user_data(self, data):
        user, id_roles, ids_unit = data
        token = UserToken(
            names=user.fullname(),
            email=user.email,
            id_admin=id_roles[constants.POSITION_ADMIN],
            id_coordinator=id_roles[constants.POSITION_COORD],
            id_tutor=id_roles[constants.POSITION_TUTOR],
            id_student=id_roles[constants.POSITION_STUDENT],
            ids_unit=ids_unit
        )
        token.update_expiration_time()
        self.logger.debug(f'Token enviado: {token.encode_to_utf8()}')
        return token.encode_to_utf8()


class ValidateGoogleLoginSuccess(Success):
    code = info_success_logic.VALIDATE_GOOGLE_LOGIN_OK_CODE
    message = info_success_logic.VALIDATE_GOOGLE_LOGIN_OK_MESSAGE
