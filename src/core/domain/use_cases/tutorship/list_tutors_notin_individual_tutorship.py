from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListTutorsIndividualTutorshipUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.id_tutorship = req.id_tutorship
        self.search = req.search
        self.limit = req.limit
        self.filter = req.filter

    @catch_errors
    def execute(self):
        unit = self.repo_tutorship.get_unit_by_tutorship(self.id_tutorship)
        tutors = self.repo_tutorship.list_tutors_individual_tutorship(self.id_tutorship, unit.idx, self.search,
                                                                      self.limit, self.filter)
        self.res = Response(tutors=tutors)
        self.success = ListTutorsIndividualTutorshipSuccess()


class ListTutorsIndividualTutorshipSuccess(Success):
    code = info_success_logic.LIST_TUTORS_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.LIST_TUTORS_INDIVIDUAL_TUTORSHIP_OK_MESSAGE