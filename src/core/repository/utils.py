from src.helpers import constants


def paginate(query, page=constants.DEFAULT_PAGE, pagesize=constants.DEFAULT_PAGESIZE):
    """
    This util function paginates a SQLAlchemy-query adding calculated offset and limit.
    Pages start at ONE and have a 'pagesize' number or rows.
    The query offset is calculated as 'page' minus ONE.
    :param query: SQLAlchemy Query object given by the repository
    :param page: int that represents the page to return
    :param pagesize: int that represents the number of rows to return in a page
    :return: SQLAlchemy Query object param modified for pagination
    """
    return query.offset((page-constants.ONE) * pagesize).limit(pagesize)
