from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response
from src.helpers import constants


class ListSessionsForTutorUseCase(UseCase):
    """ Class that lists all sessions for a tutor and
    by tutorship name of the session.
    """

    def __init__(self, errors=None, req=None, repo_session=None, repo_tutorship=None, repo_student=None,
                 repo_action_plan=None):
        super().__init__(errors, req)
        # repos
        self.repo_session   = repo_session or self.RepoFactory.SessionRepo()
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo(session=self.repo_session.session)
        self.repo_student   = repo_student or self.RepoFactory.StudentRepo(session=self.repo_session.session)
        self.repo_action_plan = repo_action_plan or self.RepoFactory.ActionPlanRepo(session=self.repo_session.session)
        # request data
        self.tutor = req.tutor
        self.search = req.search or constants.EMPTY_STRING
        self.show_all = req.show_all

    @catch_errors
    def execute(self):
        sessions = self.repo_session.list_sessions_per_tutor(self.tutor, self.show_all, self.search)
        if sessions:
            sessions, students, tutorships = zip(*sessions)
            has_action_plan = [bool(len(self.repo_action_plan.has_valid_action_plan(session.sessions_serie.idx)))
                               for session in sessions]
            sessions = zip(sessions, students, tutorships, has_action_plan)
        self.res = Response(sessions=sessions)
