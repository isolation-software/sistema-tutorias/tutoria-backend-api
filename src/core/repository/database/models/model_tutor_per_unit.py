from uuid import uuid4

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class TutorPerUnitModel(Base):
    __tablename__ = cons.TABLE_NAME_TUTOR_PER_UNIT

    id_tutor_per_unit = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_tutor = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_TUTOR}.{cons.TABLE_ID_TUTOR}"))
    id_unit  = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_UNIT}.{cons.TABLE_ID_UNIT}"))

    unit  = relationship("UnitModel")
    tutor = relationship("TutorModel")
