from src.core.domain.entities.enums import Roles
from src.core.repository.database.models import StudentModel, CoordinatorModel, TutorModel, AdministratorModel


class RoleModels:
    model_dict = {
        Roles.STUDENT: (StudentModel, StudentModel.id_student),
        Roles.COORDINATOR: (CoordinatorModel, CoordinatorModel.id_coordinator),
        Roles.TUTOR: (TutorModel, TutorModel.id_tutor),
        Roles.ADMINISTRATOR: (AdministratorModel, AdministratorModel.id_administrator)
    }

    @classmethod
    def role_model(cls, role):
        return cls.model_dict[role]
