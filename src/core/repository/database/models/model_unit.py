from uuid import uuid4

from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import Unit
from src.core.domain.entities.enums import UnitTypes
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class UnitModel(Base):
    __tablename__ = cons.TABLE_NAME_UNIT

    id_unit = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    name = Column(String(cons.STRING_SHORT_LENGTH), nullable=False)
    code = Column(String(cons.STRING_SHORT_LENGTH), unique=True)
    typex = Column(String(cons.STRING_SHORT_LENGTH), nullable=False)

    id_father_unit = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_UNIT}.{cons.TABLE_ID_UNIT}"))
    id_coordinator = Column(UUID(as_uuid=True),
                            ForeignKey(f"{cons.TABLE_NAME_COORDINATOR}.{cons.TABLE_ID_COORDINATOR}"))

    coordinator = relationship("CoordinatorModel")

    def to_entity(self):
        """ Método que convierte este objeto Model a un objeto de la entidad Unit.
            Obs: S está tomando en cuenta el pase de la unidad padre, pero debido a la recursión
            no se está pasando toda la data completa.
            :return Unit object
        """
        return Unit(
            idx=self.id_unit,
            name=self.name,
            code=self.code,
            typex=UnitTypes(self.typex),
            coordinator=self.coordinator.to_entity() if self.coordinator else None,
            father_unit=Unit(idx=self.id_father_unit)
        )
