from flask_restful import Resource
from flask import request

from src.app.auth.handler import jwt_required
from src.controllers import UnitController
from src.helpers import constants


class UnitsResource(Resource):
    @jwt_required
    def get(self):
        id_tutor = request.args.get('idTutor')
        response = UnitController.list_units(id_tutor)
        return response.to_dict(), constants.HTTP_OK

    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class UnitResource(Resource):
    @jwt_required
    def get(self, id_unit):
        response = UnitController.request_unit(id_unit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def put(self, id_unit):
        data = request.get_json()
        response = UnitController.update_unit(id_unit, data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UnitProgramsResource(Resource):
    @jwt_required
    def post(self):
        data = request.get_json()
        response = UnitController.register_unit_program(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UnitFacultyResource(Resource):
    @jwt_required
    def get(self):
        search = request.args.get('search', '')
        limit = request.args.get('limit', 10)
        response = UnitController.list_faculties(search, limit=limit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        data = request.get_json()
        response = UnitController.register_faculty(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UnitStudentResource(Resource):
    @jwt_required
    def get(self, id_unit):
        search = request.args.get('search', '')
        limit = request.args.get('limit', constants.DEFAULT_LIMIT)
        response = UnitController.list_student_per_unit(id_unit, search, limit=limit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

