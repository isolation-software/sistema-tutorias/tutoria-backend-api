from src.config import LoggerFactory, EngineFactory, FlaskConfig

from flask import Flask
from flask_cors import CORS

from src.app.api.views import generate_tutorias_backend_blueprint
from src.core.repository.database.connection import Base


logger = LoggerFactory.get_logger(__name__)


def init_app(app):
    logger.info("Inicializando configuración de APP")
    CORS(app)
    app.config.from_object(FlaskConfig)
    bp = generate_tutorias_backend_blueprint()
    app.register_blueprint(bp, url_prefix='/api')
    logger.info("APP inicializada con éxito")
    return app


def init_database(base):
    logger.info("Iniciando creación de base de datos")
    base.metadata.create_all(EngineFactory.get_engine())
    logger.info("Base de datos creada con éxito")


def generate_app():
    app = Flask(__name__)
    init_database(Base)
    init_app(app)
    return app

# sudo gunicorn "app.create_app:generate_app()"
