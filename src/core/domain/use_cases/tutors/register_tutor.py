from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import TutorAlreadyRegisteredError

from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterTutorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutor=None, repo_user=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutor = repo_tutor or self.RepoFactory.TutorRepo()
        self.repo_user = repo_user or self.RepoFactory.UserRepo(self.repo_tutor.session)
        # request data
        self.tutor = req.tutor
        self.unit = req.unit

    @catch_errors
    def execute(self):
        user = self.repo_user.read_user_by_email_if_exists(self.tutor.user)
        if user is not None:
            self.tutor.user.idx = user.idx
            is_tutor = self.repo_user.is_tutor_with_user_id(self.tutor.user)
            if is_tutor:
                is_tutor_unit = self.repo_tutor.is_tutor_in_tutor_per_unit(self.tutor,self.unit)
                if is_tutor_unit is not None:
                    tutor_saved = self.repo_tutor.save_tutor_in_tutor_per_unit(self.tutor, self.unit)
                else:
                    raise TutorAlreadyRegisteredError(self.tutor)
            else:
                tutor_saved = self.repo_tutor.save_tutor_without_user(self.tutor, self.unit)
        else:
            tutor_saved = self.repo_tutor.save_tutor_with_user(self.tutor, self.unit)

        self.res = Response(tutor=tutor_saved)
        self.success = RegisterTutorSuccess


class RegisterTutorSuccess(Success):
      code = info_success_logic.REGISTER_TUTOR_OK_CODE
      message = info_success_logic.REGISTER_TUTOR_OK_MESSAGE
