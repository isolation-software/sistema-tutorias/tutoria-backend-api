from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import RegisterIndividualWhenGroupError
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterIndividualTutorshipUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None, repo_sessions_serie=None):
        super().__init__(errors, req)
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_sessions_serie = repo_sessions_serie or self.RepoFactory.SessionsSerieRepo(
            session=self.repo_tutorship.session)
        self.unit_id     = req.unit_id
        self.tutorship   = req.tutorship
        self.tutor_ids   = req.tutor_ids
        self.student_ids = req.student_ids

    @catch_errors
    def execute(self):
        if not self.tutorship.is_individual:
            raise RegisterIndividualWhenGroupError()
        tutorship_saved = self.repo_tutorship.save_individual_tutorship(unit_id=self.unit_id, tutorship=self.tutorship)
        if self.student_ids:
            self.repo_sessions_serie.register_series_per_student(tutorship_saved, self.student_ids)
        if self.tutor_ids:
            self.repo_tutorship.save_tutors_for_tutorship(self.tutor_ids, tutorship_saved, self.unit_id)
        self.res = Response(tutorship_saved=tutorship_saved)
        self.success = RegisterIndividualTutorshipSuccess
        #TODO aqui manda la notificacion para el form?


class RegisterIndividualTutorshipSuccess(Success):
    code = info_success_logic.REGISTER_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.REGISTER_INDIVIDUAL_TUTORSHIP_OK_MESSAGE
