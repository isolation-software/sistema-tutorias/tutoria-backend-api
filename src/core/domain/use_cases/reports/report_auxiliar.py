from openpyxl.chart import BarChart, Reference, LineChart, PieChart

from src.config import LoggerFactory


class ReportAuxiliar:

    logger = LoggerFactory.get_logger(__name__)

    @staticmethod
    def create_tutorship_h1(ws1, result):

        table = ws1.tables["TablaTutoria1"]
        a = 12
        for b in range(len(result)):
            a = a + 1

        table.ref = 'B' + '11' + ':' + 'E' + str(a - 1)
        ws1.tables["TablaTutoria1"] = table

        c = 0
        for b in range(len(result)):
            ws1['B' + str(b + 12)] = result[c][1]
            ws1['C' + str(b + 12)] = result[c][2]
            ws1['D' + str(b + 12)] = result[c][3]
            ws1['E' + str(b + 12)] = result[c][4]
            c = c + 1

        values = Reference(ws1, min_col=3, min_row=11, max_col=3, max_row=len(result) + 11)
        chart = BarChart()
        chart.type = "bar"
        chart.add_data(values, titles_from_data=True)
        cats = Reference(ws1, min_col=2, min_row=12, max_row=len(result) + 11)
        chart.set_categories(cats)
        chart.title = "SESIONES ATENDIDAS POR TUTORIAS"
        celda = 'C' + str(len(result) + 14)
        ws1.add_chart(chart, celda)
        return ws1

    @staticmethod
    def create_tutorship_moment_h1(ws1, result):

        table = ws1.tables["TablaTutoria1"]
        a = 12
        for b in range(len(result)):
            a = a + 1

        table.ref = 'B' + '11' + ':' + 'F' + str(a - 1)
        ws1.tables["TablaTutoria1"] = table

        c = 0
        for b in range(len(result)):
            ws1['B' + str(b+12)] = result[c][0]
            ws1['C' + str(b+12)] = result[c][1]
            ws1['D' + str(b+12)] = result[c][2]
            ws1['E' + str(b+12)] = result[c][3]
            ws1['F' + str(b+12)] = result[c][4]
            c = c + 1

        values = Reference(ws1, min_col=4, min_row=11, max_col=4, max_row=len(result) + 11)
        chart = BarChart()
        chart.type = "bar"
        chart.add_data(values, titles_from_data=True)
        cats = Reference(ws1, min_col=3, min_row=12, max_row=len(result) + 11)
        chart.set_categories(cats)
        chart.title = "SESIONES ATENDIDAS DE TUTORIA AL MOMENTO POR TUTOR"
        celda = 'C' + str(len(result) + 14)
        ws1.add_chart(chart, celda)
        return ws1

    @classmethod
    def create_tutorship_moment_h2(cls, ws2, result):
        table = ws2.tables["TablaTutoria2"]
        a = 12
        for b in range(len(result)):
            a = a + 1

        table.ref = 'B' + '11' + ':' + 'D' + str(a - 1)
        ws2.tables["TablaTutoria2"] = table

        mesesDic = {
            "JANUARY  ": 'Enero',
            "FEBRUARY ": 'Febrero',
            "MARCH    ": 'Marzo',
            "APRIL    ": 'Abril',
            "MAY      ": 'Mayo',
            "JUNE     ": 'Junio',
            "JULY     ": 'Julio',
            "AUGUST   ": 'Agosto',
            "SEPTEMBER": 'Septiembre',
            "OCTOBER  ": 'Octubre',
            "NOVEMBER ": 'Noviembre',
            "DECEMBER ": 'Diciembre'
        }

        c = 0
        # cls.logger.debug(f"report auxiliar: {type(result[c])} - {result[c]}")
        # cls.logger.debug(f"report auxiliar: {type(result[c][0])} - {result[c][0]}")
        for b in range(len(result)):
            ws2['B' + str(b + 12)] = mesesDic[str(result[c][0])]
            ws2['C' + str(b + 12)] = result[c][1]
            ws2['D' + str(b + 12)] = result[c][2]
            c = c + 1

        values = Reference(ws2, min_col=3, min_row=11, max_col=3, max_row=len(result) + 11)
        chart = LineChart()
        chart.style = 12
        chart.add_data(values, titles_from_data=True)
        cats = Reference(ws2, min_col=2, min_row=12, max_row=len(result) + 11)
        chart.set_categories(cats)
        chart.title = "FRECUENCIA DE SESIONES DE TUTORÍAS DE MOMENTO"
        celda = 'C' + str(len(result) + 14)
        ws2.add_chart(chart, celda)
        return ws2

    @staticmethod
    def create_tutorship_moment_h3(ws3, result):
        table = ws3.tables["TablaTutoria3"]
        a = 15
        for b in range(len(result)):
            a = a + 1

        table.ref = 'B' + '14' + ':' + 'C' + str(a - 1)
        ws3.tables["TablaTutoria3"] = table

        c = 0
        for b in range(len(result)):
            ws3['B' + str(b + 15)] = result[c][0]
            ws3['C' + str(b + 15)] = result[c][1]
            c = c + 1

        values = Reference(ws3, min_col=3, min_row=14, max_row=len(result) + 14)
        chart = PieChart()
        chart.add_data(values, titles_from_data=True)
        cats = Reference(ws3, min_col=2, min_row=15, max_row=len(result) + 14)
        chart.set_categories(cats)
        chart.title = "FRECUENCIA DE TEMAS TRATADOS EN SESIONES DE TUTORIAS AL MOMENTO"
        celda = 'E' + str(14)
        ws3.add_chart(chart, celda)
        return ws3

    @staticmethod
    def create_tutors_h1(ws1, result):

        table = ws1.tables["TablaTutores1"]
        a = 12
        for b in range(len(result)):
            a = a + 1

        table.ref = 'B' + '11' + ':' + 'F' + str(a - 1)
        ws1.tables["TablaTutores1"] = table

        c = 0
        for b in range(len(result)):
            ws1['B' + str(b + 12)] = result[c][0]
            ws1['C' + str(b + 12)] = result[c][1]
            ws1['D' + str(b + 12)] = result[c][2]
            ws1['E' + str(b + 12)] = result[c][3]
            ws1['F' + str(b + 12)] = result[c][4]
            c = c + 1

        values = Reference(ws1, min_col=4, min_row=11, max_col=4, max_row=len(result) + 11)
        chart = BarChart()
        chart.type = "bar"
        chart.add_data(values, titles_from_data=True)
        cats = Reference(ws1, min_col=3, min_row=12, max_row=len(result) + 11)
        chart.set_categories(cats)
        chart.title = "SESIONES ATENDIDAS POR TUTOR"
        celda = 'C' + str(len(result) + 14)
        ws1.add_chart(chart, celda)
        return ws1
