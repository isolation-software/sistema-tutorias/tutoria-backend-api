from src.helpers import info_exception_repository


class RepositoryError(Exception):
    def __init__(self, code, message, cause=None):
        self.code = code
        self.message = message
        self.cause = cause
        super().__init__(self.message)


class UserIntegrityError(RepositoryError):

    def __init__(self, param, value, cause=None):
        code = info_exception_repository.USER_INTEGRITY_EXCEPTION_CODE
        message = info_exception_repository.USER_INTEGRITY_EXCEPTION_MESSAGE.format(param, value)
        super().__init__(code, message, cause)


class UnitIntegrityError(RepositoryError):
    def __init__(self, cause=None):
        code = info_exception_repository.UNIT_INTEGRITY_EXCEPTION_CODE
        message = info_exception_repository.UNIT_INTEGRITY_EXCEPTION_MESSAGE
        super().__init__(code, message, cause)


class MultipleResultsError(RepositoryError):

    def __init__(self, model, cause):
        self.model = model
        code  = info_exception_repository.MULTIPLE_RESULTS_EXCEPTION_CODE
        message = info_exception_repository.MULTIPLE_RESULTS_EXCEPTION_MESSAGE.format(model.__tablename__)
        super().__init__(code, message, cause)


class GenericRepositoryError(RepositoryError):
    def __init__(self, cause=None):
        code = info_exception_repository.GENERIC_REPOSITORY_EXCEPTION_CODE
        message = info_exception_repository.GENERIC_REPOSITORY_EXCEPTION_MESSAGE
        super().__init__(code, message, cause)


class UnitNotFoundError(RepositoryError):
    def __init__(self, cause=None):
        code  = info_exception_repository.UNIT_NO_RESULTS_BY_ID_EXCEPTION_CODE
        message = info_exception_repository.UNIT_NO_RESULTS_BY_ID_EXCEPTION_MESSAGE
        super().__init__(code, message, cause)


class TutorshipNotFoundError(RepositoryError):
    def __init__(self, cause=None):
        code = info_exception_repository.TUTORSHIP_NO_RESULTS_BY_ID_EXCEPTION_CODE
        message = info_exception_repository.TUTORSHIP_NO_RESULTS_BY_ID_EXCEPTION_MESSAGE
        super().__init__(code, message, cause)


class SessionNotFoundError(RepositoryError):
    def __init__(self, cause=None):
        code = info_exception_repository.SESSION_NO_RESULTS_BY_ID_EXCEPTION_CODE
        message = info_exception_repository.SESSION_NO_RESULTS_BY_ID_EXCEPTION_MESSAGE
        super().__init__(code, message, cause)


class NoTutorshipMomentForUnitError(RepositoryError):
    def __init__(self, cause=None):
        code = info_exception_repository.MOMENT_TUTORSHIP_NOT_FOUND_FOR_UNIT_CODE
        message = info_exception_repository.MOMENT_TUTORSHIP_NOT_FOUND_FOR_UNIT_MESSAGE
        super().__init__(code, message, cause)
