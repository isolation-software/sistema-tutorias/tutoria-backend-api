from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ListStudentsUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        # request data
        self.search = req.search
        self.limit = req.limit

    @catch_errors
    def execute(self):
        last_saved_students = self.repo_student.read_last_saved_students(self.search, self.limit)
        self.res = Response(students=last_saved_students)
