from src.controllers.common import Controller
from src.controllers.university.deserializer_university import ValidEmailDeserializer, UniversityDeserializer
from src.controllers.university.serializer_university import UniversitySerializer
from src.core.domain.use_cases.university import RegisterValidEmailPatternsUseCase, RegisterUniversityUseCase,\
    UpdateUniversityUseCase, GetValidEmailPatternsUseCase
from src.core.domain.use_cases.common import Request
from src.core.domain.use_cases.university.get_university import GetUniversityUseCase


class UniversityController(Controller):
    valid_email_deserializer = ValidEmailDeserializer
    university_deserializer = UniversityDeserializer
    university_serializer = UniversitySerializer
    register_valid_email_patterns_uc = RegisterValidEmailPatternsUseCase

    @classmethod
    def register_valid_email_patterns(cls, json):
        emails = cls.valid_email_deserializer.valid_emails(json)
        req = Request(emails=emails)
        return cls.build_response(
            use_case=cls.register_valid_email_patterns_uc,
            req=req
        )

    register_university_uc = RegisterUniversityUseCase

    @classmethod
    def register_university(cls, logo, json):
        university, file_extension = cls.university_deserializer.university_data(json)
        req = Request(university=university, logo=logo, file_extension=file_extension)
        return cls.build_response(
            use_case=cls.register_university_uc,
            req=req,
            serialize_response=cls.university_serializer.university_data
        )

    get_university_uc = GetUniversityUseCase

    @classmethod
    def get_university(cls):
        return cls.build_response(use_case=cls.get_university_uc, req=None, serialize_response=cls.university_serializer.university_data)

    update_university_uc = UpdateUniversityUseCase

    @classmethod
    def update_university(cls, logo, json):
        university, file_extension = cls.university_deserializer.university_data(json)
        req = Request(university=university, logo=logo, file_extension=file_extension)
        return cls.build_response(
            use_case=cls.update_university_uc,
            req=req,
            serialize_response=cls.university_serializer.university_data
        )

    get_valid_email_patterns_uc = GetValidEmailPatternsUseCase

    @classmethod
    def get_valid_email_patterns(cls):
        return cls.build_response(
            use_case=cls.get_valid_email_patterns_uc,
            req=None,
            serialize_response=cls.university_serializer.domains
        )
