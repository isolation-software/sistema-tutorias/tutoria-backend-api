from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterStudentsIndividualTutorshipUseCase(UseCase):
    def __init__(self, errors=None, req=None,  repo_sessions_serie=None):
        super().__init__(errors, req)
        self.repo_sessions_serie = repo_sessions_serie or self.RepoFactory.SessionsSerieRepo()
        self.id_tutorship = req.id_tutorship
        self.students_id = req.students_id

    @catch_errors
    def execute(self):
        self.repo_sessions_serie.register_series_per_additional_student(self.id_tutorship, self.students_id)
        self.res = Response()
        self.success = RegisterStudentsIndividualTutorshipSuccess


class RegisterStudentsIndividualTutorshipSuccess(Success):
    code = info_success_logic.REGISTER_STUDENTS_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.REGISTER_STUDENTS_INDIVIDUAL_TUTORSHIP_OK_MESSAGE