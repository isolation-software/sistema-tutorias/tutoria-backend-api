from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, String, DateTime, Boolean
from sqlalchemy.dialects.postgresql import UUID

from src.core.domain.entities import User
from src.core.repository.database.connection import Base
from src.helpers import constants


class UserModel(Base):
    __tablename__ = constants.TABLE_NAME_USER

    id_user = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    email = Column(String(constants.EMAIL_LENGTH), nullable=False, unique=True)
    code = Column(String(constants.CODE_LENGTH), nullable=False, unique=True)

    names   = Column(String(constants.STRING_LENGTH), nullable=False)
    father_lastname = Column(String(constants.STRING_LENGTH), nullable=False)
    mother_lastname = Column(String(constants.STRING_LENGTH))
    register_date   = Column(DateTime, default=datetime.utcnow)
    phone_number    = Column(String(constants.PHONE_LENGTH))
    is_activate = Column(Boolean, default=False)
    picture_url = Column(String(constants.STRING_URL_FILE_LENGHT))

    hash_password = Column(String(constants.HASH_LENGTH))
    hash_salt     = Column(String(constants.SALT_LENGTH))

    def to_entity(self):
        return User(
            idx=self.id_user,
            email=self.email,
            code=self.code,
            names=self.names,
            father_lastname=self.father_lastname,
            mother_lastname=self.mother_lastname,
            phone_number=self.phone_number,
            is_activate=self.is_activate,
            picture_url=self.picture_url
        )
