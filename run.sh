#!/bin/bash

# WINDOWS - FLASK
#export ENVIRONMENT=DEVELOPMENT
#export CONFIG_PATH=C:\\Users\\dg\\Documents\\isolation\\sistema-tutorias\\tutoria-config\\files-backend
#export LOGS_PATH=C:\\Users\\dg\\Documents\\isolation\\sistema-tutorias\\tutoria-backend
#export FLASK_APP=src/wsgi.py:generate_app
#FLASK_ENV=development flask run

# LINUX - GUNICORN
source ../tutoria-credentials/variables.sh
export LOGS_PATH=logs
export CONFIG_PATH=${VAR_CONFIG_PATH}
export ENVIRONMENT=${VAR_ENVIRONMENT}
nohup gunicorn -b 127.0.0.1:${VAR_GUNICORN_PORT} "src.wsgi:generate_app()" -t ${VAR_GUNICORN_TIMEOUT} --workers ${VAR_GUNICORN_WORKERS_CANT} > ~/aux.log 2>&1 &
#echo $! > pid.txt
#gunicorn -b 127.0.0.1:${VAR_GUNICORN_PORT} "src.wsgi:generate_app()" -t ${VAR_GUNICORN_TIMEOUT} --workers ${VAR_GUNICORN_WORKERS_CANT}
