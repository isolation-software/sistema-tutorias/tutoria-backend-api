from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

from src.config import LoggerFactory
from src.config.config_general import EnvironVariables, EnvironConfig


class EngineFactory:

    engine = None
    url = None
    logger = LoggerFactory.get_logger(__name__)

    @classmethod
    def config(cls):
        config = EnvironConfig.DB_CONFIG
        cls.logger.debug(f"Conexión a base de datos con configuración de ambiente {EnvironVariables.ENVIRONMENT}")

        cls.url = URL(
            drivername=config['DB_DRIVER'],
            host=config['DB_HOST'],
            port=config['DB_PORT'],
            username=config['DB_USERNAME'],
            password=config['DB_PASSWORD'],
            database=config['DB_DATABASE'],
        )
        cls.logger.debug(f"URL generada {cls.url}")

        cls.engine = create_engine(cls.url)
        cls.logger.info("Motor de base de datos creada")

    @classmethod
    def get_engine(cls):
        return cls.engine


EngineFactory.config()
