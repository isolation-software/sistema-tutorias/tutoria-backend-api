from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RequestDataTutorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutor=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutor = repo_tutor or self.RepoFactory.TutorRepo()
        # request data
        self.id_tutor = req.id_tutor

    @catch_errors
    def execute(self):
        tutor = self.repo_tutor.read_data_tutor_id(self.id_tutor)

        self.res = Response(tutor=tutor)
        self.success = RequestDataCoordinatorSuccess


class RequestDataCoordinatorSuccess(Success):
     code = info_success_logic.REQUEST_DATA_TUTOR_OK_CODE
     message = info_success_logic.REQUEST_DATA_TUTOR_OK_MESSAGE
