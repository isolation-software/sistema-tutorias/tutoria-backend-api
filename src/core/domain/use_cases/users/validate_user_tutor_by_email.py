from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ValidateUserAsTutorByEmailUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_user=None):
        super().__init__(errors, req)
        self.user = req.user
        self.unit = req.unit
        self.repo_user = repo_user or self.RepoFactory.UserRepo()

    @catch_errors
    def execute(self):
        user = self.repo_user.read_user_by_email_if_exists(self.user)
        if user is not None:
            has_role = self.repo_user.is_tutor_with_user_id(user)
        else:
            has_role = None
        same_unit = self.repo_user.is_already_tutor_in_unit(self.unit, user) if self.unit and user else None
        self.res = Response(user=user, has_role=has_role, same_unit=same_unit)

