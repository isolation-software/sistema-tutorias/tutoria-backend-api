from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success


class RegisterFacultyUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_unit=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo(session=self.repo_unit.session)
        self.faculty = req.faculty

    @catch_errors
    def execute(self):
        faculty_saved = self.repo_unit.save_faculty(self.faculty)
        self.repo_tutorship.register_moment_tutorship_in_unit(id_unit=faculty_saved.id_unit)
        self.res = Response(faculty_saved=faculty_saved)
        self.success = RegisterFacultySuccess


class RegisterFacultySuccess(Success):
    code = 801
    message = "Registro de facultad exitosa"

