from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RequestDataCoordinatorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_coordinator=None):
        super().__init__(errors, req)
        # repos
        self.repo_coordinator = repo_coordinator or self.RepoFactory.CoordinatorRepo()
        # request data
        self.id_coordinator = req.id_coordinator

    @catch_errors
    def execute(self):
        coordinator = self.repo_coordinator.read_data_coordinator_id(self.id_coordinator)

        self.res = Response(coordinator=coordinator)
        self.success = RequestDataCoordinatorSuccess


class RequestDataCoordinatorSuccess(Success):
     code = info_success_logic.REQUEST_DATA_COORDINATOR_OK_CODE
     message = info_success_logic.REQUEST_DATA_COORDINATOR_OK_MESSAGE
