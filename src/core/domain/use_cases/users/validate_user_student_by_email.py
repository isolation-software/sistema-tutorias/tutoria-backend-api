from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class ValidateUserAsStudentByEmailUseCase(UseCase):
    """ UseCase Class that validates if a user exists in the system and as Student
        in an specific unit if necessary. This class uses two objects:
        - User object => with email attribute
        - Unit object => with its idx attribute
        Without these three objects inside the request param, this class is unable
        to complete its execution successfully.
    """

    def __init__(self, errors=None, req=None, repo_user=None):
        super().__init__(errors, req)
        self.user = req.user
        self.repo_user = repo_user or self.RepoFactory.UserRepo()

    @catch_errors
    def execute(self):
        user = self.repo_user.read_user_by_email_if_exists(self.user)
        if user is not None:
            has_role = self.repo_user.is_student_with_user_id(user)
        else:
            has_role = None
        self.res = Response(user=user, has_role=has_role)
