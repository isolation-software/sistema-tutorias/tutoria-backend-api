import datetime

import jwt

from src.config import LoggerFactory, FlaskConfig
from src.core.domain.entities.enum.session_states import SessionStates
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.core.repository.email import EmailSender
from src.helpers import info_success_logic
from src.helpers.serializer import id_to_str


class UpdateSessionUseCase(UseCase):
    # Class - Register the result of the session of type Moment Tutorship

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_session=None, repo_availability_date=None):
        super().__init__(errors, req)
        # repos
        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        self.repo_availability_date = \
            repo_availability_date or self.RepoFactory.AvailabilityDateRepo(self.repo_session.session)

        # request data
        self.session = req.session
        self.action_plan = req.action_plan

    @catch_errors
    def execute(self):
        self.logger.debug(f"UseCase - Iniciando con la actualización de la sesión con id {self.session.idx}")
        session_updated = self.repo_session.update_session(self.session, self.action_plan)
        self.logger.debug(f"UseCase - Terminando con la actualización de la sesión con id {self.session.idx}")
        if self.session.start_time:
            self.repo_availability_date.delete_session_on_schedule(self.session)
            if self.session.state != SessionStates.CANCELED:
                unit = self.repo_session.get_unit_by_id_tutor_session(session_updated)
                self.repo_availability_date.book_session_on_schedule(self.session, unit)
        self.res = Response(session=session_updated)
        alumno = self.repo_session.get_student_of_session(self.session)
        #TODO aqui para mandar correo del form?
        if session_updated.state == SessionStates.CANCELED:
            session_to_send = self.repo_session.get_session_by_id(session_updated.idx)
            EmailSender.send_cancel_appointment(student=alumno,
                                                tutorship=session_to_send.sessions_serie.tutorship,
                                                tutor=session_to_send.tutor,
                                                session=session_to_send)
            EmailSender.send_cancel_appointment_tutor(tutorship=session_to_send.sessions_serie.tutorship,
                                                      tutor=session_to_send.tutor,
                                                      session=session_to_send)

        if session_updated.state == SessionStates.FINISHED:
            session_to_send = self.repo_session.get_session_by_id(session_updated.idx)
            tutorship = session_to_send.sessions_serie.tutorship
            token_expired_time = datetime.datetime.utcnow() + datetime.timedelta(weeks=24)
            token = jwt.encode({'tutoria': tutorship.name,
                                'idSesion': id_to_str(session_to_send.idx),
                                'usuario': alumno.user.fullname(),
                                'correo': alumno.user.email,
                                'fechaAtencion': session_to_send.start_time.isoformat(),
                                'exp': token_expired_time,
                                'expFormat': token_expired_time.isoformat()},
                               FlaskConfig.SECRET_KEY)
            token_utf8 = token.decode('utf-8')
            tutor = session_to_send.tutor
            EmailSender.send_satisfaction(student=alumno, tutorship=tutorship, tutor=tutor, session=session_to_send, token=token_utf8)
        self.success = UpdateSessionSuccess


class UpdateSessionSuccess(Success):
    code = info_success_logic.SESSION_UPDATED_OK_CODE
    message = info_success_logic.SESSION_UPDATED_OK_MESSAGE
