from src.core.domain.entities.enum.session_states import SessionStates
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.core.domain.entities import Session
from src.helpers import info_success_logic


class RegisterTutorshipMomentUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None, repo_session=None, repo_sessions_serie=None):
        super().__init__(errors, req)
        self.repo_tutorship     = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_session       = repo_session or self.RepoFactory.SessionRepo(self.repo_tutorship.session)
        self.repo_session_serie = repo_sessions_serie or self.RepoFactory.SessionsSerieRepo(self.repo_tutorship.session)
        self.unit    = req.unit
        self.tutor   = req.tutor
        self.student = req.student
        self.start_time = req.start_time

    @catch_errors
    def execute(self):
        # se obtiene la tutoria de momento de la unidad del alumno
        tutorship = self.repo_tutorship.read_moment_tutorship(self.unit)
        session_serie = self.repo_session_serie.register_serie_without_sessions(tutorship=tutorship, student=self.student)
        session = Session(start_time=self.start_time,
                          tutor=self.tutor,
                          sessions_serie=session_serie,
                          state=SessionStates.PENDING)
        tutorship_moment_saved = self.repo_session.save_moment_session(session, self.unit)
        self.res = Response(tutorship_moment_saved=tutorship_moment_saved)
        self.success = RegisterTutorshipMomentSuccess


class RegisterTutorshipMomentSuccess(Success):
    code = info_success_logic.REGISTER_TUTORSHIP_MOMENT_OK_CODE
    message = info_success_logic.REGISTER_TUTORSHIP_MOMENT_OK_MESSAGE
