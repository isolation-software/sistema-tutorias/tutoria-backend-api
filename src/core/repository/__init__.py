from src.core.repository.repo_administrator import AdministratorRepo
from src.core.repository.repo_report import ReportRepo
from src.core.repository.repo_valid_email import ValidEmailRepo
from src.core.repository.repo_student import StudentRepo
from src.core.repository.repo_support_unit import SupportUnitRepo
from src.core.repository.repo_unit import UnitRepo
from src.core.repository.repo_user import UserRepo
from src.core.repository.repo_tutorship import TutorshipRepo
from src.core.repository.repo_sessions_serie import SessionsSerieRepo
from src.core.repository.repo_action_plan import ActionPlanRepo
from src.core.repository.repo_session import SessionRepo
from src.core.repository.repo_coordinator import CoordinatorRepo
from src.core.repository.repo_tutor import TutorRepo
from src.core.repository.repo_availability_date import AvailabilityDateRepo
from src.core.repository.repo_university import UniversityRepo
from src.core.repository.repo_additional_info import AdditionalInfoRepo
from src.core.repository.repo_indicators import IndicatorsRepo








