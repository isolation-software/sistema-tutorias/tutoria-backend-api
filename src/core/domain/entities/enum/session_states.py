from enum import Enum


class SessionStates(Enum):
    CANCELED = "Cancelado"
    PENDING = "Pendiente"
    FINISHED = "Atendido"

