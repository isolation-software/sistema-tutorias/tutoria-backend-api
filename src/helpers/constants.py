# business logic
from dateutil.relativedelta import relativedelta

EMPTY_LIST = 0
EMPTY_STRING = ""
DEFAULT_LIMIT = 10
ZERO = 0
ONE  = 1
LAST_ITEM_INDEX = -1
FULLNAMES_FORMAT = "{names} {father} {mother}"
EMAIL_WITH_STLD_PATTERN_FORMAT = "{prefix}%@{domain}.{sponsored_tld}.{tld}"
EMAIL_WITHOUT_STLD_PATTERN_FORMAT = "{prefix}%@{domain}.{tld}"

TUTORSHIP_MOMENT_NAME = "Tutoría de Momento"
TUTORSHIP_MOMENT_DESCRIPTION        = "Es una tutoría llevada a cabo repentinamente cuando el alumno necesite ayuda de un profesor y este disponga del tiempo necesario."
TUTORSHIP_MOMENT_SESSIONS_NUMBER    = 1
TUTORSHIP_MOMENT_IS_INDIVIDUAL      = True
TUTORSHIP_MOMENT_IS_TUTOR_PERMANENT = False
TUTORSHIP_MOMENT_IS_OBLIGATORY      = False
TUTORSHIP_MOMENT_IS_TUTOR_ASSIGNED  = False

DOT = "."
STATIC = 'static'
SLASH = '/'

FIRST = 0
LAST = -1

STRING_PLAN_DESCRIPTION_LENGTH = 200
STRING_SESSION_COMMENT_LENGTH = 400

STRING_URL_FILE_LENGHT = 300

DEFAULT_PAGE = 1
DEFAULT_PAGESIZE = 10

POSITION_ADMIN = 0
POSITION_COORD = 1
POSITION_TUTOR = 2
POSITION_STUDENT = 3

## DATABASE ##
# sizes
PHONE_LENGTH  = 15
STRING_LENGTH = 150
STRING_SHORT_LENGTH = 50
STRING_STATE_LENGTH = 30
STRING_LONG_LENGHT = 400
EMAIL_LENGTH = 50
CODE_LENGTH = 20
HASH_LENGTH = 200
SALT_LENGTH = 200


# table names
TABLE_NAME_USER = "MT_USER"
TABLE_ID_USER = "id_user"

TABLE_NAME_STUDENT = "MT_STUDENT"
TABLE_ID_STUDENT = "id_student"

TABLE_NAME_UNIT = "MT_UNIT"
TABLE_ID_UNIT = "id_unit"

TABLE_NAME_TUTOR = "MT_TUTOR"
TABLE_ID_TUTOR = "id_tutor"

TABLE_NAME_COORDINATOR = "MT_COORDINATOR"
TABLE_ID_COORDINATOR = "id_coordinator"

TABLE_NAME_TUTOR_PER_UNIT = "MT_TUTOR_PER_UNIT"
TABLE_ID_TUTOR_PER_UNIT = "id_tutor_per_unit"

TABLE_NAME_AVAILABILITY_DATE = "MT_AVAILABILITY_DATE"
TABLE_ID_AVAILABILITY_DATE = "id_availability_date"

TABLE_NAME_TUTORSHIP = "MT_TUTORSHIP"
TABLE_ID_TUTORSHIP = "id_tutorship"

TABLE_NAME_SESSION_SET = "MT_SESSION_SET"
TABLE_ID_SESSION_SET = "id_session_set"

TABLE_NAME_SESSION = "MT_SESSION"
TABLE_ID_SESSION = "id_session"

TABLE_NAME_SET_PER_STUDENT = "MT_SET_PER_STUDENT"
TABLE_ID_SET_PER_STUDENT = "id_set_per_student"

TABLE_NAME_TUTOR_PER_TUTORSHIP = "MT_TUTOR_PER_TUTORSHIP"
TABLE_ID_TUTOR_PER_TUTORSHIP = "id_tutor_per_tutorship"

TABLE_NAME_ACTION_PLAN = "MT_ACTION_PLAN"
TABLE_ID_ACTION_PLAN = "id_action_plan"

TABLE_NAME_ADDITIONAL_INFO = "MT_ADDITIONAL_INFO"
TABLE_ID_ADDITIONAL_INFO = "id_additional_info"

TABLE_NAME_UNIVERSITY = "MT_UNIVERSITY"
TABLE_ID_UNIVERSITY = "id_university"

TABLE_NAME_SUPPORT_UNIT = "MT_SUPPORT_UNIT"
TABLE_ID_SUPPORT_UNIT = "id_support_unit"

TABLE_NAME_ADMINISTRATOR = "MT_ADMINISTRATOR"
TABLE_ID_ADMINISTRATOR = "id_administrator"

TABLE_NAME_VALID_EMAIL = "MT_VALID_EMAIL"
TABLE_ID_VALID_EMAIL = "id_valid_email"

TABLE_NAME_INDICATOR = "MT_INDICATOR"
TABLE_ID_INDICATOR = "id_indicator"


### REST ###
# status
OK = "ok"
ERROR = "error"

# CODES AND MESSAGES
CODE_REGISTER_OK = 0
CODE_REGISTER_ERROR = 100
CODE_SEARCH_OK = 1
CODE_SEARCH_ERROR = 101
CODE_UPDATED_OK = 2
CODE_UPDATED_ERROR = 102

REGISTER_STUDENT_OK_MESSAGE = "Alumno registrado exitosamente"
REGISTER_STUDENT_ERROR_MESSAGE = "No se ha podido registrar al alumno"
SEARCH_STUDENTS_OK_MESSAGE = "Búsqueda de alumnos exitosa"
SEARCH_STUDENTS_ERROR_MESSAGE = "Búsqueda de alumnos fallida"
SEARCH_STUDENT_OK_MESSAGE = "Alumno encontrado exitosamente"
SEARCH_STUDENT_ERROR_MESSAGE = "No se ha podido encontrar al alumno"
REGISTER_TUTORSHIP_OK_MESSAGE = "Tutoría registrada exitosamente"
REGISTER_TUTORSHIP_ERROR_MESSAGE = "No se ha podido registrar la tutoría"
SEARCH_TUTORSHIP_OK_MESSAGE = "Búsqueda de tutorías exitosa"
SEARCH_TUTORSHIP_ERROR_MESSAGE = "Búsqueda de tutorías fallida"
REGISTER_SESSION_OK_MESSAGE = "Sesión registrada exitosamente"
REGISTER_SESSION_ERROR_MESSAGE = "No se ha podido registrar la sesión"
SEARCH_SESSIONS_OK_MESSAGE = "Búsqueda de sesiones exitosa"
SEARCH_SESSIONS_ERROR_MESSAGE = "Búsqueda de sesiones fallida"
SEARCH_SESSIONS_STATES_OK_MESSAGE = "Búsqueda de estados de sesión exitosa"
SEARCH_SESSIONS_STATES_ERROR_MESSAGE = "Búsqueda de estados de sesión fallida"
UPDATE_SESSION_OK_MESSAGE = "Sesión actualizada exitosamente"
UPDATE_SESSION_ERROR_MESSAGE = "No se ha podido actualizar "
SEARCH_TYPES_TUTORSHIP_BY_UNIT_OK_MESSAGE = "Búsqueda de tipos de tutoría exitosa"
SEARCH_TYPES_TUTORSHIP_BY_UNIT_ERROR_MESSAGE = "Búsqueda de tipos de tutoría fallida"

SEARCH_UNITS_ERROR_MESSAGE = "No se ha podido encontrar las unidades solicitadas"
SEARCH_UNITS_OK_MESSAGE = "Búsqueda de unidades exitosa"

# coordinator

REGISTER_COORDINATOR_OK_MESSAGE = "Registro de coordinador exitosa"
REGISTER_COORDINATOR_ERROR_MESSAGE = "Registro fallido de coordinador"

## USER
USER_SERVICE_ERROR_CODE = 40
USER_SERVICE_ERROR_MESSAGE = "Error interno al atender servicios de usuarios. Nos encontramos trabajando en ello."

### USER EMAIL VALIDATION
VALIDATE_USER_EMAIL_NO_EXIST_CODE    = 200
VALIDATE_USER_EMAIL_NO_EXIST_MESSAGE = "El usuario no se encuentra registrado en el sistema"

VALIDATE_USER_EMAIL_ALREADY_EXISTS_CODE    = 201
VALIDATE_USER_EMAIL_ALREADY_EXISTS_MESSAGE = "El usuario ya se encuentra registrado en el sistema"

VALIDATE_USER_EMAIL_EXISTS_AS_STUDENT_CODE    = 202
VALIDATE_USER_EMAIL_EXISTS_AS_STUDENT_MESSAGE = "El usuario ya se encuentra registrado como alumno"

VALIDATE_USER_EMAIL_EXISTS_AS_COORDINATOR_CODE    = 203
VALIDATE_USER_EMAIL_EXISTS_AS_COORDINATOR_MESSAGE = "El usuario ya se encuentra registrado como coordinador"

VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_CODE    = 204
VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_MESSAGE = "El usuario ya se encuentra registrado como tutor"

VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_ON_UNIT_CODE = 205
VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_ON_UNIT_MESSAGE = "El usuario ya se encuentra registrado como tutor en la unidad"
# http
HTTP_OK = 200

HTTP_BAD_REQUEST = 400
HTTP_UNAUTHORIZED = 401
HTTP_FORBIDDEN = 403
HTTP_NOT_FOUND = 404
HTTP_METHOD_NOT_ALLOWED = 405

## SECURITY ##
UTF8 = 'utf-8'

#CONTENT-TYPE
IMAGE = "image/"
EXTENSION_FILE = "png"
APPLICATION = "application/"
PDF = "application/pdf"
OCTECT_STREAM = "octet-stream"

NEXT_PERIOD = relativedelta(minutes=30)

DIFF_PERU = 5


UTC = 'UTC'
PERU_TIMEZONE = 'America/Lima'
