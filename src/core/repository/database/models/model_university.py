from uuid import uuid4

from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID

from src.core.domain.entities import University
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class UniversityModel(Base):
    __tablename__ = cons.TABLE_NAME_UNIVERSITY

    id_university = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    name    = Column(String(cons.STRING_SHORT_LENGTH), nullable=False)
    domain  = Column(String(cons.STRING_SHORT_LENGTH))
    logo   = Column(String(cons.STRING_URL_FILE_LENGHT))

    def to_entity(self):
        return University(name=self.name, domain=self.domain, logo=self.logo)
