SELECT X.PERIODO,X.ATENDIDAS,
       CASE WHEN X.MAX = 0 THEN 'NO HAY ENCUESTAS RESPONDIDAS'
            WHEN X.ACADEMICA = X.MAX THEN 'ACADÉMICA'
            WHEN X.ACAD_ADMIN = X.MAX THEN 'ACADÉMICO-ADMINISTRATIVO'
            WHEN X.VOCACIONAL = X.MAX THEN 'VOCACIONAL'
            WHEN X.INDIVIDUAL = X.MAX THEN 'INDIVIDUAL'
            WHEN X.PSICOLOGICO = X.MAX THEN 'PSICOLÓGICO'
            WHEN X.PERSONAL = X.MAX THEN 'PERSONAL'
            WHEN X.FAMILIAR = X.MAX THEN 'FAMILIAR'
            WHEN X.ECONOMICO = X.MAX THEN 'ECONÓMICO' END AS MAX_TEMA_TRATADO
FROM (
         SELECT Z.*,
                GREATEST(Z.ACADEMICA, Z.ACAD_ADMIN, Z.VOCACIONAL, Z.PERSONAL, Z.FAMILIAR,
                         Z.INDIVIDUAL, Z.ECONOMICO, Z.PSICOLOGICO) AS MAX
         FROM (
                  SELECT A.PERIODO,
                         COALESCE(B.ATENDIDAS, 0)                AS ATENDIDAS,
                         COALESCE(C.ACADEMICA, 0)                AS ACADEMICA,
                         COALESCE(D.ACADEMICO_ADMINISTRATIVO, 0) AS ACAD_ADMIN,
                         COALESCE(E.VOCACIONAL, 0)               AS VOCACIONAL,
                         COALESCE(F.PERSONAL, 0)                 AS PERSONAL,
                         COALESCE(G.FAMILIAR, 0)                 AS FAMILIAR,
                         COALESCE(H.INDIVIDUAL, 0)               AS INDIVIDUAL,
                         COALESCE(I.ECONOMICO, 0)                AS ECONOMICO,
                         COALESCE(J.PSICOLOGICO, 0)              AS PSICOLOGICO
                  FROM (
                           SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO
                           FROM "MT_SESSION" MS
                           INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                           AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                           GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')) A
                  LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS ATENDIDAS
                      FROM "MT_SESSION" MS
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                      AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')) B ON A.PERIODO = B.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS ACADEMICA
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Académica%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) C ON A.PERIODO = C.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS ACADEMICO_ADMINISTRATIVO
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Académico – administrativo%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) D ON A.PERIODO = D.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS VOCACIONAL
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Vocacional%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) E ON A.PERIODO = E.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS PERSONAL
                      FROM "MT_SESSION" MS
                        INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Personal%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) F ON A.PERIODO = F.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS FAMILIAR
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Familiar%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) G ON A.PERIODO = G.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS INDIVIDUAL
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Individual%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) H ON A.PERIODO = H.PERIODO
                 LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS ECONOMICO
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Económico%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) I ON A.PERIODO = I.PERIODO
                LEFT OUTER JOIN (
                      SELECT TO_CHAR(date(MS.start_time), 'MONTH') AS PERIODO, COUNT(*) AS PSICOLOGICO
                      FROM "MT_SESSION" MS
                               INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
                      INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
                           INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
                           WHERE UPPER(MT.name) NOT LIKE '%MOMENTO%' AND MS.state = 'Atendido'
                           AND MT.id_unit = :id_unit
                        AND MS.start_time>=:start_time AND MS.start_time<=:end_time
                        AND MI.indicator_6 LIKE ('%Psicológico%')
                      GROUP BY TO_CHAR(date(MS.start_time), 'MONTH')
                  ) J ON A.PERIODO = J.PERIODO
              ) Z
     )X;
