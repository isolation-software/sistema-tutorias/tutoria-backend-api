from src.helpers.serializer import id_to_str


class TutorshipSerializer:
    @staticmethod
    def tutorship_list_per_student(tutorships):
        return {
            "tutorias": [
                {
                    "idTutoria": id_to_str(t.idx),
                    "nombre": t.name,
                    "grupal": t.is_individual,
                    "obligatoria": t.is_obligatory,
                    "tutorAsignado": t.is_tutor_assigned,
                    "tutorVariable": t.is_tutor_assigned,
                    "unit": t.unit.name
                }
                for t in tutorships]

        }

    @staticmethod
    def get_tutorship(res):
        tutorship = res.tutorship
        num_tutors, tutors = res.data_tutors
        num_students, students = res.data_students
        return {
            "idTutoria": id_to_str(tutorship.idx),
            "nombre": tutorship.name,
            "descripcion": tutorship.description,
            "tipoTutoria": {
                "tutorFijo": tutorship.is_tutor_permanent,
                "tutorAsignado": tutorship.is_tutor_assigned,
                "individual": tutorship.is_individual,
                "obligatoria": tutorship.is_obligatory
            },
            "numSesiones": tutorship.sessions_number,
            "totalTutores": num_tutors,
            "idUnidad": id_to_str(tutorship.unit.idx),
            "nombreUnidad": tutorship.unit.name,
            "tutores": [{"idTutor": id_to_str(t.idx), "nombre": t.user.fullname(), "codigo": t.user.code} for t in tutors],
            "totalAlumnos": num_students,
            "alumnos": [{"idAlumno": id_to_str(s.idx), "nombre": s.user.fullname(), "codigo": s.user.code} for s in students]
        }

    @classmethod
    def serialize_list_tutorship_for_unit_response(cls, res):
        tutorships = res.tutorships
        return [
            {
                "idTutoria": id_to_str(tutorship.idx),
                "nombre": tutorship.name
            }
            for tutorship in tutorships
        ]

    @classmethod
    def serialize_register_tutorship_moment_response(cls, res):
        tutorship_moment = res.tutorship_moment_saved
        return {
            "idSesion": id_to_str(tutorship_moment.idx),

        }

    @classmethod
    def serialize_list_students_individual_tutorship_response(cls, res):
        students = res.students
        if students:
            return [
                {
                    "idAlumno": id_to_str(student.idx),
                    "nombre": student.user.fullname(),
                    "codigo": student.user.code
                }
                for student in students
            ]
        else:
            return []

    @classmethod
    def serialize_list_tutors_individual_tutorship_response(cls, res):
        tutors = res.tutors
        if tutors:
            return [
                {
                    "idTutor": id_to_str(tutor.idx),
                    "nombre": tutor.user.fullname(),
                    "codigo": tutor.user.code
                }
                for tutor in tutors
            ]
        else:
            return []