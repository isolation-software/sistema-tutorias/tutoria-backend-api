from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import constants, info_success_logic


class ListAppointmentsUseCase(UseCase):
    """ Class that lists all sessions of a student and
    by tutorship name of the session.
    """

    def __init__(self, errors=None, req=None, repo_session=None):
        super().__init__(errors, req)
        # repos
        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        # request data
        self.id_student = req.id_student
        self.search = req.search
        self.limit = req.limit

    @catch_errors
    def execute(self):
        sessions = self.repo_session.get_all_appointments_by_student(self.id_student, self.search, self.limit)

        self.res = Response(sessions=sessions)
        self.success = ListAppointmentsForStudentSuccess


class ListAppointmentsForStudentSuccess(Success):
    code = info_success_logic.APPOINTMENT_SEARCH_OK_CODE
    message = info_success_logic.APPOINTMENT_SEARCH_OK_MESSAGE
