from uuid import uuid4

from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID

from src.core.domain.entities import SupportUnity
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class SupportUnitModel(Base):
    __tablename__ = cons.TABLE_NAME_SUPPORT_UNIT

    id_support_unit = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    name = Column(String(cons.STRING_SHORT_LENGTH), nullable=False)
    email = Column(String(cons.STRING_SHORT_LENGTH), nullable=False)
    phone_number = Column(String(cons.STRING_SHORT_LENGTH))

    def to_entity(self):
        """
        Método que convierte el objeto SupportUnitModel a un objeto de la entidad SupportUnity.
        Solo se están tomando en cuenta los siguientes atributos de la entidad:
            - idx
            - name
            - email
            -phone
        :return: SupportUnity object
        """
        return SupportUnity(
            idx=self.id_support_unit,
            name=self.name,
            email= self.email,
            phone= self.phone_number
        )
