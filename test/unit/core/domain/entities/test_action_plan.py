import uuid

import pytest

from src.core.domain.entities.action_plan import ActionPlan


@pytest.mark.skip(reason="No corregido aún, pendiente porque implica refactorizar funcionalidades dependientes")
def test_all_atributes_are_none_when_constructor_without_parameters():
    action_plan = ActionPlan()
    for attribute in action_plan.__dict__:
        assert getattr(action_plan, attribute) is None, f"El atributo {attribute} no posee un valor nulo"


@pytest.mark.skip(reason="Plan de acción ya no necesita de un idx")
def test_does_not_require_idx_action_plan_entity():
    action_plan = ActionPlan()
    assert action_plan.idx is None, "Constructor de clase ActionPlan está creando idx a los objetos," \
                                    "constructor no debe crear idx"


@pytest.mark.skip(reason="La comparación de objetos no se usa en sistema")
def test_comparison_action_plan_same_idx():
    idx = uuid.uuid4()
    origin = ActionPlan(idx=idx)
    copy   = ActionPlan(idx=idx)
    assert origin == copy, "Método comparación no implementado correctamente"


@pytest.mark.skip(reason="La comparación de objetos no se usa en sistema")
def test_comparison_method_action_plan_different_idx():
    idx_origin = uuid.uuid4()
    idx_copy   = uuid.uuid4()
    origin = ActionPlan(idx=idx_origin)
    copy   = ActionPlan(idx=idx_copy)
    assert origin != copy, "Método comparación no implementado correctamente"
