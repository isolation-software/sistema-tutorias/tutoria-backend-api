import time

from src.config import LoggerFactory
from src.core.repository.errors.sqla_errors import GenericRepositoryError, RepositoryError
from src.helpers import constants


logger = LoggerFactory.get_logger(__name__)


def catch_repo_errors(repo_fun):
    """
    Decorator that wraps a repository method in order
    to catch third-party errors like SQLAlchemy.
        - RepositoryErrors have not to be shown using the error log. These are known by developer
        - Exceptions not mapped have to be shown using the error
          log and changed by a mapped GenericRepositoryError
    :param repo_fun: the repository method
    :return:
    """
    def execute_and_catch(*args, **kwargs):
        repo = args[constants.ZERO]
        start = time.time()
        logger.debug(f"Nivel repositorio {type(repo).__name__}. Iniciando atención")
        try:
            return repo_fun(*args, **kwargs)

        except RepositoryError as e:
            logger.error(msg=e.message, exc_info=False)
            repo.session.rollback()
            raise e

        except Exception as e:
            logger.error(e, exc_info=True)
            repo.session.rollback()
            raise GenericRepositoryError(cause=e)

        finally:
            end = time.time()
            logger.debug(f"Nivel repositorio. Tiempo transcurrido: {(end - start):.2f} segundos(s)")

    return execute_and_catch
