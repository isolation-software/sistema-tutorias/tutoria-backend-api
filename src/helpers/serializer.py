from uuid import UUID

import dateutil.parser
from dateutil.relativedelta import relativedelta
from pytz import utc


def id_to_str(idx):
    if idx is str:
        return idx
    return str(idx) if idx else None


def str_to_id(idx_str):
    if idx_str is UUID:
        return idx_str
    return UUID(idx_str) if idx_str else None


def replicate_dates_from_list(start_date, end_date, origin):
    origin = [dateutil.parser.parse(date_str) for date_str in origin]
    aux = origin[:]
    start_date = dateutil.parser.parse(start_date)
    end_date = dateutil.parser.parse(end_date)
    replicated_dates = []
    delta = relativedelta(days=+7)
    valid = True

    while valid:
        for date in origin:
            replicated = date + delta
            if replicated > end_date:
                valid = False
                break
            elif replicated >= start_date:
                replicated_dates.append(replicated)
        origin = [date + delta for date in origin]

    return aux + replicated_dates
