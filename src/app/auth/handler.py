import datetime
import jwt
from flask import request

from src.config import LoggerFactory, FlaskConfig
from src.helpers import constants
from src.helpers.serializer import id_to_str

logger = LoggerFactory.get_logger(__name__)


def jwt_required(request_send):
    def execute_and_catch(*args, **kwargs):
        logger.info('Inicio de validación del token')
        try:
            raw_token = request.headers.get('Authorization')
            logger.debug(f'Token por decodificar: {raw_token}')
            token = UserToken.decode(raw=raw_token)
        except jwt.DecodeError:
            logger.error(msg="Token inválido, no es posible decodificar el token", exc_info=False)
            return {'message': 'Token invalido'}, constants.HTTP_UNAUTHORIZED
        except jwt.ExpiredSignatureError:
            logger.error(msg="Token expirado, se evita la atención del request", exc_info=False)
            return {'message': 'Token expirado'}, constants.HTTP_UNAUTHORIZED
        except Exception as e:
            logger.error(msg="El token no se ha podido validar", exc_info=False)
            logger.error(e)
            return {'message': 'El token no se ha podido validar'}, constants.HTTP_BAD_REQUEST

        logger.info('Inicio de validación del token')
        token.update_expiration_time()
        token_utf_8 = token.encode_to_utf8()
        body, code = request_send(*args, **kwargs)
        headers = {'token': token_utf_8}
        return body, code, headers

    return execute_and_catch


class UserToken:
    def __init__(self, names=None, email=None, id_admin=None, id_coordinator=None, id_tutor=None, id_student=None, ids_unit=None, exp=None):
        self.names    = names
        self.email    = email
        self.id_admin = id_admin
        self.id_coordinator = id_coordinator
        self.id_tutor   = id_tutor
        self.id_student = id_student
        self.ids_unit   = ids_unit
        self.exp        = exp
        self.exp_format = exp.isoformat() if exp else None
        self._original_token = None
        self._origintal_token_utf8 = None

    @classmethod
    def from_decoded(cls, decoded):
        token = cls()
        token.names    = decoded['usuario']
        token.email    = decoded['correo']
        token.id_admin = decoded['idAministrador']
        token.id_coordinator = decoded['idCoordinador']
        token.id_tutor   = decoded['idTutor']
        token.id_student = decoded['idAlumno']
        token.ids_unit   = decoded['idUnidadesCoord']
        token.exp        = decoded['exp']
        token.exp_format = decoded['expFormat']
        return token

    @classmethod
    def decode(cls, raw):
        decoded = jwt.decode(raw, FlaskConfig.SECRET_KEY)
        return cls.from_decoded(decoded=decoded)

    def encode(self):
        dictionary = dict(
            usuario=self.names,
            correo=self.email,
            idAministrador=id_to_str(self.id_admin),
            idCoordinador=id_to_str(self.id_coordinator),
            idTutor=id_to_str(self.id_tutor),
            idAlumno=id_to_str(self.id_student),
            idUnidadesCoord=id_to_str(self.ids_unit),
            exp=self.exp,
            expFormat=self.exp.isoformat()
        )
        return jwt.encode(dictionary, FlaskConfig.SECRET_KEY)

    def encode_to_utf8(self):
        return self.encode().decode(constants.UTF8)

    def update_expiration_time(self, minutes=FlaskConfig.EXP_TIME):
        self.exp = datetime.datetime.utcnow() + datetime.timedelta(minutes=minutes)
