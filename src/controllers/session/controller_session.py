from src.config import LoggerFactory
from src.controllers.common import Controller
from src.core.domain.entities import Student, Tutorship, Session, Unit, Tutor
from src.core.domain.use_cases.appointments import GetLastSessionsByStudentsByIndividualTutorshipUseCase
from src.core.domain.use_cases.common import Response, Request
from src.core.domain.use_cases.sessions import ReadSessionUseCase, UpdateSessionUseCase, GetSessionsStateUseCase, \
    ListSessionsForStudentUseCase, SaveIndividualSessionUseCase, GetLastSessionsForStudentsByIndividualTutorshipUseCase, \
    RegisterSessionUseCase, ListSessionsByStudentAndTutorUseCase
from src.controllers.session.deserializer_session import SessionDeserializer
from src.core.domain.use_cases.sessions.register_indicator_of_session import RegisterIndicatorOfSessionUseCase
from src.helpers import constants
from src.helpers.serializer import str_to_id
from src.controllers.session.serializer_session import SessionSerializer


class SessionController(Controller):

    logger = LoggerFactory.get_logger(__name__)
    save_individual_session_uc = SaveIndividualSessionUseCase
    request_session_uc = ReadSessionUseCase
    session_serializer = SessionSerializer
    session_deserializer = SessionDeserializer

    @classmethod
    def request_student_data_from_session(cls, id_session):
        cls.logger.debug("Controller - iniciando el obtener datos de una sesión")
        req = Request(id_session=str_to_id(id_session))
        return cls.build_response(
            use_case=cls.request_session_uc,
            req=req,
            serialize_response=SessionSerializer.session_data
        )

    update_session_uc = UpdateSessionUseCase

    @classmethod
    def update_session(cls, id_session, json):
        session, action_plan = cls.session_deserializer.session(json, id_session)
        req = Request(session=session, action_plan=action_plan)

        return cls.build_response(
            use_case=cls.update_session_uc,
            req=req
        )

    @staticmethod
    def request_session_states():
        use_case_get_session_states = GetSessionsStateUseCase()
        use_case_get_session_states.execute()

        if not use_case_get_session_states.has_errors():
            states = use_case_get_session_states.res.states
            serialized = SessionSerializer.session_states(states)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_SESSIONS_STATES_OK_MESSAGE,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_SESSIONS_STATES_ERROR_MESSAGE,
                            data={})

    list_sessions_for_student_uc = ListSessionsForStudentUseCase

    @classmethod
    def list_sessions_for_student(cls, id_student, id_tutorship):
        student = Student(idx=str_to_id(id_student))
        tutorship = Tutorship(idx=str_to_id(id_tutorship)) if id_tutorship else None
        req = Request(student=student, tutorship=tutorship)
        serializer = cls.session_serializer.session_list_per_student

        return cls.build_response(
            use_case=cls.list_sessions_for_student_uc,
            req=req,
            serialize_response=serializer
        )

    request_sessions_uc = GetLastSessionsForStudentsByIndividualTutorshipUseCase

    @classmethod
    def request_sessions_by_tutorship_for_students(cls, data_id_tutorship):
        id_tutorship = str_to_id(data_id_tutorship)
        req = Request(id_tutorship=id_tutorship)
        return cls.build_response(
            use_case=cls.request_sessions_uc,
            req=req,
            serialize_response=cls.session_serializer.request_sessions_by_tutorship_for_students)

    request_sessions_student_uc = GetLastSessionsByStudentsByIndividualTutorshipUseCase

    @classmethod
    def request_sessions_by_tutorship_by_student(cls, data_id_tutorship, data_id_student):
        id_tutorship = str_to_id(data_id_tutorship)
        id_student = str_to_id(data_id_student)
        req = Request(id_tutorship=id_tutorship, id_student=id_student)
        return cls.build_response(
            use_case=cls.request_sessions_student_uc,
            req=req,
            serialize_response=cls.session_serializer.request_sessions_by_tutorship_by_student)

    @classmethod
    def save_individual_session(cls, id_tutorship, id_student, id_session, json):
        cls.logger.debug("Controller - iniciando el salvar sesión individual")
        start_date, id_assigned_tutor = SessionDeserializer.session_individual(json)
        session = Session(idx=id_session, start_time=start_date)
        req = Request(id_tutorship=id_tutorship,
                      id_student=id_student,
                      session=session,
                      id_assigned_tutor=id_assigned_tutor)
        return cls.build_response(
            use_case=cls.save_individual_session_uc,
            req=req
        )

    register_session_uc = RegisterSessionUseCase

    @classmethod
    def register_session(cls, json):
        session, _ = cls.session_deserializer.session(json)
        students = [str_to_id(idx) for idx in json["alumnos"]]
        unit = Unit(idx=str_to_id(json["idUnidad"]))
        tutorship = Tutorship(idx=str_to_id(json["idTutoria"]))

        req = Request(session=session, students=students, unit=unit, tutorship=tutorship)
        return cls.build_response(
            use_case=cls.register_session_uc,
            req=req,
            serialize_response=cls.session_serializer.session_data
        )

    list_sessions_by_student_and_tutor_uc = ListSessionsByStudentAndTutorUseCase

    @classmethod
    def list_sessions_by_student_and_tutor(cls, id_student, id_tutor):
        student = Student(idx=str_to_id(id_student))
        tutor = Tutor(idx=str_to_id(id_tutor))

        return cls.build_response(
            use_case=cls.list_sessions_by_student_and_tutor_uc,
            req=Request(student=student, tutor=tutor),
            serialize_response=cls.session_serializer.session_list_per_student
        )

    register_indicator_of_session_uc = RegisterIndicatorOfSessionUseCase

    @classmethod
    def register_indicator_of_session(cls, json):
        token, indicators = cls.session_deserializer.register_indicator_of_session(json)
        req = Request(token=token, indicators=indicators)
        return cls.build_response(
            use_case=cls.register_indicator_of_session_uc,
            req=req
        )
