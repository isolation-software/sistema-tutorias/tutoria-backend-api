from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import CoordinatorAlreadyRegisteredError
from src.core.domain.use_cases.common import UseCase, Response


class RegisterCoordinatorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_coordinator=None, repo_user=None):
        super().__init__(errors, req)
        # repos
        self.repo_coordinator = repo_coordinator or self.RepoFactory.CoordinatorRepo()
        self.repo_user = repo_user or self.RepoFactory.UserRepo(self.repo_coordinator.session)
        # request data
        self.coordinator = req.coordinator

    @catch_errors
    def execute(self):
        user = self.repo_user.read_user_by_email_if_exists(self.coordinator.user)
        if user is not None:
            self.coordinator.user.idx = user.idx
            is_coordinator = self.repo_user.is_coordinator_with_user_id(self.coordinator.user)
            if is_coordinator:
                raise CoordinatorAlreadyRegisteredError(self.coordinator)
            else:
                coordinator_saved = self.repo_coordinator.save_coordinator_without_user(self.coordinator)
        else:
            coordinator_saved = self.repo_coordinator.save_coordinator_with_user(self.coordinator)

        self.res = Response(coordinator=coordinator_saved)
