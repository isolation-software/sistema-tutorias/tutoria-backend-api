from src.config.config_report import ReportFactory
from src.controllers.common import Controller
import dateutil.parser
from src.core.domain.use_cases.reports import GetSessionsUseCase, ReportAuxiliar
from src.core.repository import ReportRepo, UnitRepo
from src.deserializers import ReportDeserializer
from src.helpers.formater import convert_to_peru_time
from src.serializers import ReportSerializer
from datetime import date


class ReportController(Controller):

    report_deserializer = ReportDeserializer
    get_sessions_uc = GetSessionsUseCase
    report_serializer = ReportSerializer

    @classmethod
    def get_tutorships_report(cls, unit, start_date, end_date):
        unidad = UnitRepo().get_unit_by_id(unit)
        start_date = dateutil.parser.parse(start_date)
        end_date = dateutil.parser.parse(end_date)
        result = ReportRepo().list_tutorship_sessions(unit, start_date, end_date)

        today = date.today()
        ini_date = convert_to_peru_time(start_date, str_format=True)
        fin_date = convert_to_peru_time(end_date, str_format=True)

        wb = ReportFactory.get_workbook("reporte_tutorias.xlsx")

        ws1 = wb.worksheets[0]
        ws1['E2'] = today.strftime("%d/%m/%Y")
        ws1['E7'] = unidad.name
        ws1['C7'] = str(ini_date)
        ws1['C8'] = str(fin_date)

        ws1 = ReportAuxiliar.create_tutorship_h1(ws1, result)

        ws2 = wb.worksheets[1]
        ws2['E2'] = today.strftime("%d/%m/%Y")
        ws2['E7'] = unidad.name
        ws2['C7'] = str(ini_date)
        ws2['C8'] = str(fin_date)

        result2 = ReportRepo().list_sessions_frequency_tutorship_without_moment(unit, start_date, end_date)
        ws2 = ReportAuxiliar.create_tutorship_moment_h2(ws2, result2)

        ws3 = wb.worksheets[2]
        ws3['G2'] = today.strftime("%d/%m/%Y")
        ws3['G7'] = unidad.name
        ws3['C7'] = str(ini_date)
        ws3['C8'] = str(fin_date)

        result3 = ReportRepo().list_issues_tutorships_without_moment(unit, start_date, end_date)
        ws3 = ReportAuxiliar.create_tutorship_moment_h3(ws3, result3)

        res = ReportFactory.make_response(wb, filename="reporte_tutorias.xlsx")
        return res


    @classmethod
    def get_tutors_report(cls, unit, start_date, end_date):

        unidad = UnitRepo().get_unit_by_id(unit)
        start_date = dateutil.parser.parse(start_date)
        end_date = dateutil.parser.parse(end_date)

        result = ReportRepo().list_tutors_satisfactions(unit, start_date, end_date)

        today = date.today()
        ini_date = convert_to_peru_time(start_date, str_format=True)
        fin_date = convert_to_peru_time(end_date, str_format=True)

        wb = ReportFactory.get_workbook("reporte_tutores.xlsx")

        ws1 = wb.worksheets[0]
        ws1['F2'] = today.strftime("%d/%m/%Y")
        ws1['F7'] = unidad.name
        ws1['C7'] = str(ini_date)
        ws1['C8'] = str(fin_date)

        ws1 = ReportAuxiliar.create_tutors_h1(ws1, result)

        res = ReportFactory.make_response(wb, filename="reporte_tutores.xlsx")
        return res

    @classmethod
    def get_tutorships_moment_report(cls, unit, start_date, end_date):
        unidad = UnitRepo().get_unit_by_id(unit)
        start_date = dateutil.parser.parse(start_date)
        end_date = dateutil.parser.parse(end_date)
        result = ReportRepo().list_tutor_satisfaction_moment_tutorship(unit, start_date, end_date)

        today = date.today()
        ini_date = convert_to_peru_time(start_date, str_format=True)
        fin_date = convert_to_peru_time(end_date, str_format=True)

        wb = ReportFactory.get_workbook("reporte_tutoria_momento.xlsx")

        ws1 = wb.worksheets[0]
        ws1['F2'] = today.strftime("%d/%m/%Y")
        ws1['F7'] = unidad.name
        ws1['C7'] = str(ini_date)
        ws1['C8'] = str(fin_date)

        ws1 = ReportAuxiliar.create_tutorship_moment_h1(ws1, result)

        ws2 = wb.worksheets[1]
        ws2['E2'] = today.strftime("%d/%m/%Y")
        ws2['E7'] = unidad.name
        ws2['C7'] = str(ini_date)
        ws2['C8'] = str(fin_date)

        result2 = ReportRepo().list_sessions_frequency_moment_tutorship(unit, start_date, end_date)
        ws2 = ReportAuxiliar.create_tutorship_moment_h2(ws2, result2)

        ws3 = wb.worksheets[2]
        ws3['G2'] = today.strftime("%d/%m/%Y")
        ws3['G7'] = unidad.name
        ws3['C7'] = str(ini_date)
        ws3['C8'] = str(fin_date)

        result3 = ReportRepo().list_issues_moment_tutorship(unit, start_date, end_date)
        ws3 = ReportAuxiliar.create_tutorship_moment_h3(ws3, result3)

        res = ReportFactory.make_response(wb, filename="reporte_tutorias_momento.xlsx")
        return res
