from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class UpdateUnitUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_unit=None):
        super().__init__(errors, req)
        # repos
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()
        # request data
        self.unit = req.unit

    @catch_errors
    def execute(self):
        unit_updated = self.repo_unit.update_unit(self.unit)
        self.res = Response(unit_updated=unit_updated)
        self.success = UpdateUnitSuccess


class UpdateUnitSuccess(Success):
    code = info_success_logic.UNIT_UPDATED_OK_CODE
    message = info_success_logic.UNIT_UPDATED_OK_MESSAGE
