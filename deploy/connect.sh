#!/usr/bin/env bash
IP_EC2=$1
USERNAME=$2
TOKEN_CRED=$3
TOKEN_BACK=$4

scp -o StrictHostKeyChecking=no deploy/config.yml ubuntu@${IP_EC2}:/home/ubuntu
scp -o StrictHostKeyChecking=no deploy/manager.sh ubuntu@${IP_EC2}:/home/ubuntu
ssh -o StrictHostKeyChecking=no ubuntu@${IP_EC2} "bash manager.sh ${USERNAME} ${TOKEN_CRED} ${TOKEN_BACK}"
