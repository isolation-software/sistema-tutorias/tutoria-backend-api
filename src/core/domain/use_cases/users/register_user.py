from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class RegisterUserUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_user=None):
        super().__init__(errors, req)
        # repos
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        # request data
        self.user = req.user

    @catch_errors
    def execute(self):
        user_saved = self.repo_user.save_user(self.user)
        self.res = Response(user=user_saved)
