import pytest
from faker import Faker
from mock import Mock

# from src.controllers import UserController
# from src.core.domain.use_cases.users import ValidateUserAsStudentByEmailUseCase


@pytest.fixture(name="mock_uc")
def mock_uc_validate_user_by_email():
    # mock_uc = Mock(spec=ValidateUserAsStudentByEmailUseCase)
    # return mock_uc
    pass


@pytest.fixture(name="param_email")
def get_user_email_parameter():
    return "jhair.guzmant@pucp.edu.pe"


@pytest.fixture(name="param_unit_idx")
def get_unit_idx_parameter():
    fake_gen = Faker()
    Faker.seed(0)
    return fake_gen.uuid4(cast_to=None)


def test_instantiate_user_uc(mock_uc):
    pass


@pytest.mark.skip("Se evita por falta de conocimiento para manejar los mocks en los use-cases")
def test_call_user_uc_execute(param_email, param_unit_idx, mock_uc):
    # UserController.usecase = mock_uc
    # UserController.validate_email_for_student(param_email=param_email, param_unit_idx=param_unit_idx)
    # mock_uc.execute.assert_called_once_with()
    pass


def test_confirm_correct_request_object_sent_to_user_uc_email_validation():
    pass


def test_manage_uc_response_when_has_errors():
    pass


def test_confirm_correct_response_when_uc_has_errors():
    pass


def test_manage_uc_response_when_no_errors():
    pass


def test_confirm_correct_response_when_user_no_exists():
    pass


def test_confirm_correct_response_when_user_exists_and_no_student():
    pass


def test_confirm_correct_response_when_user_exists_and_is_student():
    pass
