import base64
from src.config import LoggerFactory
from src.helpers.serializer import id_to_str


class ReportSerializer:

    @staticmethod
    def report_session(response):
        stream = response.stream
        return base64.b64encode(stream)












