from enum import Enum


class UnitTypes(Enum):
    FACULTY = "Facultad"
    PROGRAM = "Programa"
