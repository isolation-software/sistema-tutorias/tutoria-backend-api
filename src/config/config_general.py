import configparser
import os
from datetime import datetime
from os import environ, path
from src.res.utils import RES_DIRECTORY


class EnvironVariables:
    """ Environment Variables defined before running the application.
        In README, there are descriptions about variables needed to run.
        If any variable not found, it raises an error.
        """
    ## ENVIRONMENT ##
    DEV = "DEVELOPMENT"
    PROD = "PRODUCTION"
    RLS = "RELEASE"

    CONFIG_PATH = environ['CONFIG_PATH']
    LOGS_PATH   = environ['LOGS_PATH']
    ENVIRONMENT = environ['ENVIRONMENT']


class EnvironFiles:
    ## FILENAMES ##
    LOG_FILENAME   = "logging.ini"
    API_FILENAME   = "api.ini"
    DB_FILENAME    = "database.ini"
    S3_FILENAME    = "s3.ini"
    EMAIL_FILENAME = "email.ini"
    OAUTH_FILENAME = "oauth.ini"

    ## PATHS ##
    PATH_FILENAME_LOG   = path.join(EnvironVariables.CONFIG_PATH, LOG_FILENAME)
    PATH_FILENAME_API   = path.join(EnvironVariables.CONFIG_PATH, API_FILENAME)
    PATH_FILENAME_DB    = path.join(EnvironVariables.CONFIG_PATH, DB_FILENAME)
    PATH_FILENAME_S3    = path.join(EnvironVariables.CONFIG_PATH, S3_FILENAME)
    PATH_FILENAME_EMAIL = path.join(EnvironVariables.CONFIG_PATH, EMAIL_FILENAME)
    PATH_FILENAME_OAUTH = path.join(EnvironVariables.CONFIG_PATH, OAUTH_FILENAME)


class ReportFiles:
    DIRECTORY_NAME = "reports"
    PATH_REPORT = path.join(RES_DIRECTORY, DIRECTORY_NAME)
    PATH_REPORT_TUTORS     = path.join(RES_DIRECTORY, DIRECTORY_NAME, "report_tutors.xlsx")
    PATH_REPORT_TUTORSHIPS = path.join(RES_DIRECTORY, DIRECTORY_NAME, "report_tutorships.xlsx")
    PATH_REPORT_SQL = path.join(RES_DIRECTORY, DIRECTORY_NAME, "sql")


class EmailContentFiles:
    DIRECTORY_NAME = "emails"
    PATH_WELCOME_CONFIRM     = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME, "welcome_confirm.html")
    PATH_SATISFACTION        = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME, "satisfaction.html")
    PATH_NOT_FOUND           = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME, "email_not_found.html")
    PATH_PENDING_APPOINTMENT = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME, "pending_appointment.html")
    PATH_CANCEL_APPOINTMENT  = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME, "cancel_appointment.html")
    PATH_MODIFY_APPOINTMENT  = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME, "modify_appointment.html")
    PATH_CANCEL_APPOINTMENT_TUTOR = path.join(EnvironVariables.CONFIG_PATH, DIRECTORY_NAME,
                                              "cancel_appointment_tutor.html")


class EnvironConfig:
    """ Configuration gotten from config ".ini" files.
        Each variable is a dictionary with config data. """
    _api_config = configparser.ConfigParser()
    _api_config.read(EnvironFiles.PATH_FILENAME_API)
    API_CONFIG = _api_config[EnvironVariables.ENVIRONMENT]

    _db_config = configparser.ConfigParser()
    _db_config.read(EnvironFiles.PATH_FILENAME_DB)
    DB_CONFIG = _db_config[EnvironVariables.ENVIRONMENT]

    _s3_config = configparser.ConfigParser()
    _s3_config.read(EnvironFiles.PATH_FILENAME_S3)
    S3_CONFIG = _s3_config[EnvironVariables.ENVIRONMENT]

    _email_config = configparser.ConfigParser()
    _email_config.read(EnvironFiles.PATH_FILENAME_EMAIL)
    EMAIL_CONFIG = _email_config[EnvironVariables.ENVIRONMENT]

    _oauth_config = configparser.ConfigParser()
    _oauth_config.read(EnvironFiles.PATH_FILENAME_OAUTH)
    OAUTH_CONFIG = _oauth_config[EnvironVariables.ENVIRONMENT]


class EnvironLogs:
    """ Filenames for each type of log.
        OBS: By now, System is using only GENERAL LOG to keep all
        logging messages. It'll be improved in next iterations."""

    ## LEVELS ##
    _NOTSET   = "NOTSET"
    _DEBUG    = "DEBUG"
    _INFO     = "INFO"
    _WARNING  = "WARNING"
    _ERROR    = "ERROR"
    _CRITICAL = "CRITICAL"

    LEVEL = _DEBUG if EnvironVariables.ENVIRONMENT == EnvironVariables.DEV else _INFO

    ## LOG FILENAMES ##
    LOG_EVENTS_FILENAME = "tutorias-events.log"
    LOG_ERRORS_FILENAME = "tutorias-errors.log"
    LOG_GENERAL_FILENAME = "tutorias.{}.{}.log".format(datetime.now().strftime('%Y-%m-%d_%H-%M-%S'), os.getpid())

    ## LOG FILE PATHS ##
    LOG_GENERAL_FILE_PATH = path.join(EnvironVariables.LOGS_PATH, LOG_GENERAL_FILENAME)
