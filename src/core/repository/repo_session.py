from sqlalchemy import and_, or_, func

from src.config import LoggerFactory
from src.core.domain.entities import SessionsSerie
from src.core.domain.entities.enum.session_states import SessionStates
from src.core.domain.errors.logic_errors import TimeAlreadyTakenError
from src.core.repository import ActionPlanRepo
from src.core.repository.common import Repository
from src.core.repository.database.models import SessionModel, TutorPerUnitModel, SessionSetModel, TutorshipModel, \
    SetPerStudentModel, StudentModel, TutorModel, UserModel, UnitModel
from src.core.repository.errors.handler import catch_repo_errors
from src.core.repository.errors.sqla_errors import SessionNotFoundError
from src.helpers import constants


class SessionRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def get_last_sessions_of_students_in_tutorship(self, id_tutorship):
        rows = self.session.query(StudentModel, SessionModel) \
            .join(SetPerStudentModel, SetPerStudentModel.id_student == StudentModel.id_student) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SetPerStudentModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .outerjoin(SessionModel, SessionModel.id_session_set == SessionSetModel.id_session_set) \
            .filter(TutorshipModel.id_tutorship == id_tutorship) \
            .order_by(StudentModel.id_student.asc(), SessionModel.register_date.desc()) \
            .all()
        tuples = {}
        for student_row, session_row in rows:
            if student_row.id_student not in tuples.keys():
                tuples[student_row.id_student] = (student_row, session_row)

        return [(student.to_entity() if student else None,
                 session.to_entity() if session else None) for student, session in tuples.values()]

    @catch_repo_errors
    def get_student_from_session(self, session):
        row_student = self.session.query(StudentModel) \
            .join(SessionModel, SessionModel.id_session == session.idx) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionModel.id_session_set) \
            .first()
        return row_student.to_entity()

    @catch_repo_errors
    def get_student_of_session(self, session):
        row_student = self.session.query(StudentModel) \
            .join(SetPerStudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(SessionModel, SetPerStudentModel.id_session_set == SessionModel.id_session_set) \
            .filter(SessionModel.id_session == session.idx).one_or_none()
        return row_student.to_entity() if row_student else None

    @catch_repo_errors
    def get_unit_by_id_tutor_session(self, session):
        row = self.session.query(UnitModel) \
            .filter(SessionModel.id_session == session.idx) \
            .filter(TutorPerUnitModel.id_tutor_per_unit == SessionModel.id_tutor_per_unit) \
            .filter(TutorPerUnitModel.id_unit == UnitModel.id_unit) \
            .first()
        return row.to_entity()

    @catch_repo_errors
    def get_session_by_id(self, id_session):
        row = self.session.query(SessionModel).filter(SessionModel.id_session == id_session).first()
        return row.to_entity() if row else None

    @catch_repo_errors
    def save_moment_session(self, session, unit):
        id_tutor = session.tutor.idx
        id_unit = unit.idx
        session.end_time = session.start_time if not session.end_time else session.end_time
        row_tutor_per_unit = self.session.query(TutorPerUnitModel) \
            .filter(and_(TutorPerUnitModel.id_tutor == id_tutor,
                         TutorPerUnitModel.id_unit == id_unit)
                    ).one()
        row_session = SessionModel(
            start_time=session.start_time,
            end_time=session.end_time,
            comment=session.comments,
            state=session.state.value,
            id_session_set=session.sessions_serie.idx,
            id_tutor_per_unit=row_tutor_per_unit.id_tutor_per_unit,
            order_number=1
        )
        self.session.add(row_session)
        self.session.commit()
        self.session.refresh(row_session)
        session.idx = row_session.id_session
        return row_session.to_entity()

    @catch_repo_errors
    def save_session(self, session, unit):
        id_tutor = session.tutor.idx
        id_unit = unit.idx

        session.end_time = session.start_time if not session.end_time else session.end_time

        session_already_exists = self.session.query(SessionModel) \
            .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor_per_unit == SessionModel.id_tutor_per_unit) \
            .filter(or_(
                and_(
                    SessionModel.start_time > session.start_time,
                    SessionModel.start_time < session.end_time,
                    TutorPerUnitModel.id_tutor == id_tutor,
                    SessionModel.state != SessionStates.CANCELED.value
                ),
                and_(
                    session.start_time > SessionModel.start_time,
                    session.start_time < SessionModel.end_time,
                    TutorPerUnitModel.id_tutor == id_tutor,
                    SessionModel.state != SessionStates.CANCELED.value
                ))).all()

        if session_already_exists:
            raise TimeAlreadyTakenError()

        row_tutor_per_unit = self.session.query(TutorPerUnitModel) \
            .filter(and_(TutorPerUnitModel.id_tutor == id_tutor,
                         TutorPerUnitModel.id_unit == id_unit)
                    ).one()
        row_session = SessionModel(
            start_time=session.start_time,
            end_time=session.end_time,
            comment=session.comments,
            state=session.state.value,
            id_session_set=session.sessions_serie.idx,
            id_tutor_per_unit=row_tutor_per_unit.id_tutor_per_unit,
            order_number=1
        )
        self.session.add(row_session)
        self.session.commit()
        self.session.refresh(row_session)
        session.idx = row_session.id_session
        # cambio JGT, si causa problemas, cambiar row_session.to_entity() por session, en teoria,
        # row_session refleja la bd (por el refresh), asi que es el objecto completo de la sesion
        return row_session.to_entity()

    @catch_repo_errors
    def update_session(self, session, action_plan):
        self.logger.debug(f"Repo - Actualizando sesión con id {session.idx}")
        row_session = self.session.query(SessionModel).filter(SessionModel.id_session == session.idx).first()
        row_session.comment = session.comments if session.comments else row_session.comment
        row_session.derivation = session.derivation if session.derivation else row_session.derivation
        row_session.id_support_unit = session.support_unit.idx if session.support_unit else \
            row_session.id_support_unit
        row_session.state = session.state.value
        row_session.start_time = session.start_time if session.start_time else row_session.start_time
        row_session.end_time = session.end_time if session.end_time else row_session.end_time

        if session.tutor:
            row_tutor_per_unit = self.session.query(TutorPerUnitModel) \
                .filter(TutorPerUnitModel.id_tutor == session.tutor.idx).first()
            row_session.id_tutor_per_unit = row_tutor_per_unit.id_tutor_per_unit

        self.session.commit()
        self.logger.debug(f"Repo - Sessión actualizada con id {session.idx}")
        repo_action_plan = ActionPlanRepo()
        if action_plan:
            repo_action_plan.save_action_plan(row_session.id_session_set, action_plan)
        else:
            self.logger.debug(f"Repo - Plan de acción sin cambios")

        return session

    @catch_repo_errors
    def read_session(self, id_session):
        row_session = self.session \
            .query(SessionModel) \
            .filter(SessionModel.id_session == id_session) \
            .one_or_none()
        if row_session is None:
            raise SessionNotFoundError()
        session = self._build_session(row_session)
        return session

    @catch_repo_errors
    def list_sessions_per_tutor(self, tutor, show_all, name=""):
        name = "%{}%".format(name)
        states = [SessionStates.FINISHED.value] if not show_all else [s.value for s in SessionStates]

        row_sessions = self.session \
            .query(SessionModel, StudentModel, TutorshipModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor_per_unit == SessionModel.id_tutor_per_unit) \
            .join(TutorModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(UserModel, UserModel.id_user == StudentModel.id_user) \
            .filter(and_(
                or_(
                    TutorshipModel.name.like(name),
                    UserModel.names.like(name),
                    UserModel.mother_lastname.like(name),
                    UserModel.father_lastname.like(name)
                ),
                TutorModel.id_tutor == tutor.idx),
                SessionModel.state.in_(states)) \
            .order_by(SessionSetModel.id_session_set.asc()) \
            .all()
        self.logger.debug(f"Repo - Búsqueda de sesiones: {len(row_sessions)} {row_sessions}")
        return [self._build_session_with_student_and_tutorship(sr) for sr in row_sessions]

    @catch_repo_errors
    def get_sessions_by_student_and_tutorship(self, student, tutorship):
        row_sessions = self.session \
            .query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .filter(and_(
            TutorshipModel.id_tutorship == tutorship.idx,
            StudentModel.id_student == student.idx,
            SessionModel.state == SessionStates.FINISHED.value
        )).all()

        return [sr.to_entity() for sr in row_sessions]

    @catch_repo_errors
    def get_all_sessions_by_student(self, student):
        row_sessions = self.session \
            .query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .filter(and_(
            StudentModel.id_student == student.idx,
            SessionModel.state == SessionStates.FINISHED.value
        )).all()

        return [sr.to_entity() for sr in row_sessions]

    @catch_repo_errors
    def update_individual_session(self, session):
        row_session = self.session.query(SessionModel).filter(SessionModel.id_session == session.idx).one_or_none()
        row_session.id_tutor_per_unit = session.tutor.idx
        row_session.start_time = session.start_time
        self.session.commit()
        return session

    @catch_repo_errors
    def get_last_sessions_by_tutorship_for_students(self, tutorship_id):
        self.logger.debug("Repo - buscando serie de sesiones")
        session_set_rows = self.session.query(SessionSetModel) \
            .filter(SessionSetModel.id_tutorship == tutorship_id) \
            .order_by(SessionSetModel.id_session_set.asc()) \
            .all()
        self.logger.debug(f"Repo - número de serie de sesiones encontrados: {len(session_set_rows)}")
        last_sessions = []
        for session_set_row in session_set_rows:
            session_rows = self.session.query(SessionModel) \
                .filter(SessionModel.id_session_set == session_set_row.id_session_set) \
                .all()
            session_row = self._get_last_session(session_rows)
            last_sessions.append(session_row.to_entity() if session_row else None)
        self.logger.debug(f"Repo - número de sesiones encontrados: {len(last_sessions)}")
        return last_sessions

    @catch_repo_errors
    def get_last_session_by_tutorship_by_student(self, tutorship_id, student_id):
        session_row = self.session.query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .filter(and_(
            SessionSetModel.id_tutorship == tutorship_id,
            SetPerStudentModel.id_student == student_id)) \
            .order_by(SessionModel.register_date.desc()) \
            .first()
        return session_row.to_entity() if session_row else None

    @staticmethod
    def _build_session(sr):
        session = sr.to_entity()
        session.sessions_serie = SessionsSerie(idx=sr.id_session_set)
        return session

    @staticmethod
    def _build_session_with_student_and_tutorship(sr):
        session = sr[0].to_entity()
        session.sessions_serie = SessionsSerie(idx=sr[0].id_session_set)
        student = sr[1].to_entity()
        tutorship = sr[2].to_entity()
        return session, student, tutorship

    @classmethod
    def _get_last_session(cls, session_rows):
        if session_rows:
            session_rows.sort(key=lambda row: row.order_number, reverse=True)
            return session_rows[constants.ZERO]
        return None

    @catch_repo_errors
    def get_all_appointments_by_student(self, id_student, search, limit):
        row_sessions = self.session \
            .query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .filter(and_(
            StudentModel.id_student == id_student,
            or_(
                TutorshipModel.name.like(search),
                SessionModel.state == "Atendido",
                SessionModel.state == "Pendiente",
                SessionModel.state == "Cancelado"
            )
        )
        ).limit(limit).all()

        return [sr.to_entity() for sr in row_sessions]

    @catch_repo_errors
    def get_sessions_by_student_and_tutor(self, student, tutor):
        row_sessions = self.session \
            .query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor_per_unit == SessionModel.id_tutor_per_unit) \
            .join(TutorModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
            .filter(and_(
            StudentModel.id_student == student.idx,
            SessionModel.state == SessionStates.FINISHED.value,
            TutorModel.id_tutor == tutor.idx
        )).all()

        return [sr.to_entity() for sr in row_sessions]

    @catch_repo_errors
    def get_sessions_by_range(self, start_date, end_date, tutorship):
        cant_antendida = self.session \
            .query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .filter(and_(
            TutorshipModel.id_tutorship == tutorship.idx,
            SessionModel.start_time >= start_date,
            SessionModel.end_time <= end_date,
            SessionModel.state == SessionStates.FINISHED.value
        )).count()

        cant_cancelada = self.session \
            .query(SessionModel) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SessionModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .filter(and_(
            TutorshipModel.id_tutorship == tutorship.idx,
            SessionModel.start_time >= start_date,
            SessionModel.end_time <= end_date,
            SessionModel.state == SessionStates.CANCELED.value
        )).count()

        return cant_antendida, cant_cancelada

    @catch_repo_errors
    def get_user_from_session(self, session):
        row_user = self.session.query(UserModel) \
            .join(StudentModel, UserModel.id_user == StudentModel.id_user) \
            .join(SetPerStudentModel, SetPerStudentModel.id_student == StudentModel.id_student) \
            .join(SessionModel, SessionModel.id_session_set == SetPerStudentModel.id_session_set) \
            .filter(
            SessionModel.id_session == session.idx
        ).one_or_none()
        return row_user.to_entity() if row_user else None

    @catch_repo_errors
    def get_sessions_statistics_for_tutorships_by_unit(self, start_date, end_date, unit):
        rows = self.session.query(TutorshipModel, func.count())
