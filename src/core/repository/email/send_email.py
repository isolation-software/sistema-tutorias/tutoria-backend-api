from email.mime.multipart import MIMEMultipart
import smtplib
import email.message


class SendEmail:
    def __init__(self):
        self.msg = None
        self.server = None

    def send_email(self, envia, contra, recibe , message):
        self.msg = MIMEMultipart()
        self.server = smtplib.SMTP('smtp.gmail.com: 587')

        email_content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

        <head>
            <meta charset="UTF-8">
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <meta name="x-apple-disable-message-reformatting">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="telephone=no" name="format-detection">
            <title></title>
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">

        </head>

        <body>
            <div class="es-wrapper-color">
                   <tbody>
                        <tr>
                            <td class="esd-email-paddings" valign="top">
                                <table class="es-content esd-header-popover" cellspacing="0" cellpadding="0" align="center">
                                    <tbody>
                                        <tr></tr>
                                        <tr>
                                            <td class="esd-stripe" esd-custom-block-id="7799" align="center">
                                                <table class="es-header-body" style="background-color: #044767;" width="600" cellspacing="0" cellpadding="0" bgcolor="#044767" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td class="esd-structure es-p35t es-p40b es-p35r es-p35l" align="left">
                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="esd-container-frame" width="530" valign="top" align="center">
                                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="esd-block-text es-m-txt-c" align="center">
                                                                                                <h1 style="color: #ffffff; line-height: 100%;">Sistema de Tutorias</h1>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="es-content esd-footer-popover" cellspacing="0" cellpadding="0" align="center">
                                    <tbody>
                                        <tr>
                                            <td class="esd-stripe" align="center">
                                                <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td class="esd-structure es-p35t es-p25b es-p35r es-p35l" esd-custom-block-id="7811" align="left">
                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="esd-container-frame" width="530" valign="top" align="center">
                                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="esd-block-text es-p20t es-p5b" align="left">
                                                                                                <h3 style="color: #333333;"> {mensaje} </h3>
                                                                                                <p><br></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </body>

        </html> 
        """

        self.msg = email.message.Message()
        self.msg['Subject'] = "Notificacion Sistema de Tutorias"
        self.msg.add_header('Content-Type', 'text/html')
        self.msg.set_payload(email_content.format(mensaje=message))
        self.server = smtplib.SMTP('smtp.gmail.com: 587')

        self.server.starttls()
        self.server.login(envia, contra)
        self.server.sendmail(envia, recibe, self.msg.as_string())

        self.server.quit()

