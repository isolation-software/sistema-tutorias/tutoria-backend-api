import base64

from flask_restful import Resource
from flask import request

from src.app.auth.handler import jwt_required
from src.controllers import StudentController
from src.helpers import constants


class StudentResource(Resource):
    @jwt_required
    def get(self, id_student):
        response = StudentController.get_student(id_student=id_student)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def put(self, id_student=None):
        response = None
        return response, constants.HTTP_OK


class StudentsResource(Resource):
    @jwt_required
    def get(self):
        search = request.args.get('search', '')
        limit = request.args.get('limit', 10)
        response = StudentController.list_students(search, limit=limit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        data = request.get_json()
        bulk_param = request.args.get('bulk',type=bool, default=False)
        response = StudentController.register_student(data, bulk_param)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class StudentAuthResource(Resource):
    @jwt_required
    def post(self):
        response = None
        return response, constants.HTTP_OK


class StudentAdditionalInfoResource(Resource):
    @jwt_required
    def get(self, id_student=None):
        response = StudentController.get_additional_info(id_student)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def put(self, id_student=None):
        json = request.get_json()
        file = base64.b64decode(json.get("file"))
        response = StudentController.register_additional_info(id_student, file, json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class StudentAdditionalInfosResource(Resource):
    @jwt_required
    def delete(self, id_student=None, id_additional_info=None):
        response = StudentController.delete_additional_info(id_additional_info)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST
