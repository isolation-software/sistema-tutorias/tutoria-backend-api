from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Success
from src.helpers import info_success_logic


class ReplicateAvailabilityTimeUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_availability_date=None):
        super().__init__(errors, req)
        self.repo_availability_date = repo_availability_date or self.RepoFactory.AvailabilityDateRepo()
        self.availability_dates = req.availability_dates
        self.tutor = req.tutor
        self.unit = req.unit

    @catch_errors
    def execute(self):
        self.repo_availability_date.save_availability_time(self.availability_dates, self.tutor, self.unit)
        self.res = None
        self.success = RegisterAvailabilityTimeSuccess


class RegisterAvailabilityTimeSuccess(Success):
    code = info_success_logic.AVAILABILITY_TIME_REGISTER_OK_CODE
    message = info_success_logic.AVAILABILITY_TIME_REGISTER_OK_MESSAGE
