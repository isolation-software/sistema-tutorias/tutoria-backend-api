from src.helpers.serializer import id_to_str
from src.serializers import CoordinatorSerializer


class UnitSerializer:

    @classmethod
    def serialize_units_few_data(cls, units):
        """ Método que serializa los objetos Unit a diccionarios,
            pero considerando poca información a transmitir como:
            - id unidad
            - codigo
            - nombre
            :param units, list of Unit objects
            :return list of Unit dictionaries
        """
        return [cls.serialize_unit_few_data(u) for u in units]

    @staticmethod
    def serialize_unit_few_data(unit):
        """ Método que serializa un objeto Unit a un diccionario,
            pero considerando poca información a transmitir como:
            - id unidad
            - codigo
            - nombre
            :param unit, Unit object
            :return dict of Unit
        """
        return {
            "idUnidad": id_to_str(unit.idx),
            "codigo": unit.code,
            "nombre": unit.name
        }

    @staticmethod
    def serialize_unit_all_data(unit):
        """ Método que serializa un objeto Unit a un diccionario,
            pero considerando toda su información a transmitir como:
            - id unidad
            - codigo
            - nombre
            - tipo
            - coordinador
                - id coordinador
                - nombres (completo)
            :param unit, Unit object
            :return dict of Unit
        """
        return {
            "idUnidad": id_to_str(unit.idx),
            "codigo": unit.code,
            "nombre": unit.name,
            "tipo": unit.typex.value,
            "coordinador": CoordinatorSerializer.serialize_coordinator(unit.coordinator)
        }

    @classmethod
    def serialize_units_all_data(cls, units):
        """ Método que serializa los objetos Unit a diccionarios,
            pero considerando poca información a transmitir como:
            - id unidad
            - codigo
            - nombre
            - tipo
            - coordinador
            :param units, list of Unit objects
            :return list of Unit dictionaries
        """
        return None if not units else [cls.serialize_unit_all_data(u) for u in units]

    @classmethod
    def serialize_units_and_programs_all_data(cls, unit, programs):
        return {
            "unidad": cls.serialize_unit_all_data(unit),
            "programas": cls.serialize_units_all_data(programs)
        }
