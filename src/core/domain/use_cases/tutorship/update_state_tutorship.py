from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class UpdateStateTutorshipUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        # request data
        self.id_session = req.id_session
        self.state = req.state

    @catch_errors
    def execute(self):
        state_tutorship_updated = self.repo_tutorship.update_state_tutorship(self.id_session, self.state)
        self.res = Response(state_tutorship_updated=state_tutorship_updated)
