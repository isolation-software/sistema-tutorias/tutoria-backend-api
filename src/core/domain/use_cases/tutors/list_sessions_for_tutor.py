from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response
from src.helpers import constants


class ListSessionsForTutorUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutor=None):
        super().__init__(errors, req)
        # repos
        self.repo_tutor = repo_tutor or self.RepoFactory.SessionRepo()
        # request data
        self.tutor = req.tutor
        self.search = req.search or constants.EMPTY_STRING

    @catch_errors
    def execute(self):
        sessions = self.repo_tutor.list_sessions_by_tutor(self.tutor, self.search)
        self.res = Response(sessions=sessions)
