from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import constants, info_success_logic


class ListSessionsForStudentUseCase(UseCase):
    """ Class that lists all sessions of a student and
    by tutorship name of the session.
    """

    def __init__(self, errors=None, req=None, repo_session=None):
        super().__init__(errors, req)
        # repos
        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        # request data
        self.student = req.student
        self.tutorship = req.tutorship

    @catch_errors
    def execute(self):
        if self.tutorship:
            sessions = self.repo_session.get_sessions_by_student_and_tutorship(self.student, self.tutorship)
        else:
            sessions = self.repo_session.get_all_sessions_by_student(self.student)

        self.res = Response(sessions=sessions)
        self.success = ListSessionsForStudentSuccess


class ListSessionsForStudentSuccess(Success):
    code = info_success_logic.SESSION_SEARCH_OK_CODE
    message = info_success_logic.SESSION_SEARCH_OK_MESSAGE
