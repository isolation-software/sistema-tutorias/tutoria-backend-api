from src.core.domain.entities import University
from src.core.domain.entities.valid_email import ValidEmail


class ValidEmailDeserializer:
    @staticmethod
    def valid_emails(json):
        return [ValidEmail(prefix=e.get("prefijo"),
                           domain=e.get("dominio"),
                           sponsored_tld=e.get("sTLD"),
                           top_level_domain=e.get("TLD")) for e in json["correos"]]


class UniversityDeserializer:
    @staticmethod
    def university_data(json):
        print(json.get("nombre"), json.get("dominio"))
        university = University(name=json.get("nombre"),
                                domain=json.get("dominio"))
        file_extension = json.get("extension")
        return university, file_extension
