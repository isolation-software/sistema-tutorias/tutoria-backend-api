from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListTutorshipByUnitUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_unit      = repo_unit or self.RepoFactory.UnitRepo()
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo(self.repo_unit.session)
        self.id_unit = req.id_unit
        self.search  = req.search

    @catch_errors
    def execute(self):
        unit = self.repo_unit.get_unit_by_id(self.id_unit)
        tutorships = self.repo_tutorship.get_tutorships_by_unit(unit, self.search)
        self.res = Response(tutorships=tutorships)
        self.success = ListTutorshipByUnitSuccess


class ListTutorshipByUnitSuccess(Success):
    code = info_success_logic.LIST_TUTORSHIPS_OK_CODE
    message = info_success_logic.LIST_TUTORSHIPS_OK_MESSAGE
