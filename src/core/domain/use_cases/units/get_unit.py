from src.config import LoggerFactory
from src.core.domain.entities.enums import UnitTypes
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class GetUnitUseCase(UseCase):

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo()
        self.unit = req.unit

    @catch_errors
    def execute(self):
        self.logger.debug("Iniciando la ejecución del usecase de Obtener una unidad")
        unit = self.repo_unit.get_unit_by_id(self.unit.idx)
        if unit.typex is UnitTypes.FACULTY:
            programs = self.repo_unit.read_children_units_by_father_id(self.unit)
        else:
            programs = None
        self.res = Response(unit=unit, programs=programs)
        self.success = GetUnitSuccess
        self.logger.debug("Terminando la ejecución del usecase de Obtener una unidad")


class GetUnitSuccess(Success):
    code = info_success_logic.SEARCH_UNIT_OK_CODE
    message = info_success_logic.SEARCH_UNIT_OK_MESSAGE
