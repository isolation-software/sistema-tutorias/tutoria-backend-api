from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class GetTutorshipUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None, repo_student=None, repo_tutors=None):
        super().__init__(errors, req)
        self.id_tutorship = req.id_tutorship
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_students  = repo_student or self.RepoFactory.StudentRepo(session=self.repo_tutorship.session)
        self.repo_tutors    = repo_tutors or self.RepoFactory.TutorRepo(session=self.repo_tutorship.session)

    @catch_errors
    def execute(self):
        tutorship = self.repo_tutorship.get_tutorship(self.id_tutorship)
        num_tutors, tutors = self.repo_tutors.get_tutors_by_tutorship(tutorship.idx)
        num_students, students = self.repo_students.get_students_by_tutorship(tutorship.idx)
        self.res = Response(
            tutorship=tutorship,
            data_tutors=(num_tutors, tutors),
            data_students=(num_students, students))
        self.success = GetTutorshipSuccess


class GetTutorshipSuccess(Success):
    code = info_success_logic.SEARCH_TUTORSHIP_OK_CODE
    message = info_success_logic.SEARCH_TUTORSHIP_OK_MESSAGE
