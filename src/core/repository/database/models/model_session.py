from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, ForeignKey, String, Boolean, DateTime, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import Session, SupportUnity
from src.core.domain.entities.enum.session_states import SessionStates
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class SessionModel(Base):
    __tablename__ = cons.TABLE_NAME_SESSION

    id_session = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    start_time = Column(DateTime(timezone=True))
    end_time   = Column(DateTime(timezone=True))
    comment    = Column(String(cons.STRING_SESSION_COMMENT_LENGTH))
    state      = Column(String(cons.STRING_STATE_LENGTH), default=SessionStates.PENDING.value)
    derivation = Column(Boolean, default=False)
    order_number      = Column(Integer, nullable=True)
    id_session_set    = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_SESSION_SET}.{cons.TABLE_ID_SESSION_SET}"))
    id_tutor_per_unit = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_TUTOR_PER_UNIT}.{cons.TABLE_ID_TUTOR_PER_UNIT}"))
    id_support_unit   = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_SUPPORT_UNIT}.{cons.TABLE_ID_SUPPORT_UNIT}"))
    register_date = Column(DateTime, default=datetime.utcnow)

    tutor_per_unit = relationship("TutorPerUnitModel")
    session_set    = relationship("SessionSetModel")

    def to_entity(self):
        return Session(
            idx=self.id_session,
            start_time=self.start_time,
            end_time=self.end_time,
            comments=self.comment,
            state=SessionStates(self.state),
            derivation=self.derivation,
            register_date=self.register_date,
            tutor=self.tutor_per_unit.tutor.to_entity() if self.tutor_per_unit and self.tutor_per_unit.tutor else None,
            order_number=self.order_number,
            support_unit=SupportUnity(idx=self.id_support_unit),
            sessions_serie=self.session_set.to_entity() if self.session_set else None
        )
