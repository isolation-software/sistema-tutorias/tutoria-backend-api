from src.core.domain.entities import Unit, Coordinator
from src.core.domain.entities.enums import UnitTypes
from src.helpers.serializer import str_to_id


class UnitDeserializer:

    @classmethod
    def deserialize_unit_for_program_register(cls, data):
        faculty = Unit(idx=str_to_id(data.get("idFacultad", None)))
        coordinator = Coordinator(idx=str_to_id(data["programa"]["idCoordinador"]))
        program = Unit(
            name=data["programa"]["nombre"],
            code=data["programa"]["codigo"],
            coordinator=coordinator,
            typex=UnitTypes.PROGRAM,
            father_unit=faculty
        )
        return program

    @staticmethod
    def faculty_data(json):
        faculty_data = json["data"]["facultad"]
        code = faculty_data["codigo"]
        name = faculty_data["nombre"]
        coordinator = Coordinator(idx=str_to_id(faculty_data["idCoordinador"]))
        faculty = Unit(code=code, name=name, coordinator=coordinator, typex=UnitTypes.FACULTY, father_unit=None)
        return faculty

    @staticmethod
    def update_unit_data(id_unit, json):
        json = json.get("facultad") or json.get("programa") or json.get("unidad")
        idx = id_unit
        code = json.get("codigo")
        name = json.get("nombre")
        typex = UnitTypes(json.get("tipo")) if json.get("tipo") else None
        coordinator = Coordinator(idx=str_to_id(json["idCoordinador"])) if json.get("idCoordinador") else None
        father_unit = Unit(idx=str_to_id(json.get("idFacultad"))) if json.get("idFacultad") else None
        unit = Unit(idx=idx, code=code, name=name, coordinator=coordinator, typex=typex, father_unit=father_unit)
        return unit

