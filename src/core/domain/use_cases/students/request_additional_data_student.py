from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class RequestAdditionalDataStudentUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        # request data
        self.id_student = req.id_student

    @catch_errors
    def execute(self):
        additional_data_student = self.repo_student.read_additional_data_student_id(self.id_student)
        self.res = Response(additional_data_student=additional_data_student)
