from src.core.domain.entities import Coordinator, User


class CoordinatorDeserializer:

    @staticmethod
    def coordinator(json):
        data = json["data"]
        user = User(names=data["coordinador"]["nombres"],
                    code=data["coordinador"]["codigo"],
                    father_lastname=data["coordinador"]["apellidoPat"],
                    mother_lastname=data["coordinador"]["apellidoMat"],
                    email=data["coordinador"]["correo"],
                    phone_number=data["coordinador"]["telefono"])
        coordinator = Coordinator(user=user)
        return coordinator

    @staticmethod
    def coordinator_with_id(json):
        data = json["coordinator"]
        user = User(names=data["nombres"],
                    code=data["codigo"],
                    father_lastname=data["apellidoPat"],
                    mother_lastname=data["apellidoMat"],
                    email=data["correo"],
                    phone_number=data["telefono"])
        idx = data["idCoordinador"]
        coordinator = Coordinator(idx=idx, user=user)
        return coordinator
