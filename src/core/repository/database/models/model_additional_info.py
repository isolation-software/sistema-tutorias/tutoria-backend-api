from uuid import uuid4

from sqlalchemy import Column, ForeignKey, String, DateTime
from sqlalchemy.dialects.postgresql import UUID

from src.core.domain.entities import AdditionalInformation, Student
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class AdditionalInfoModel(Base):
    __tablename__ = cons.TABLE_NAME_ADDITIONAL_INFO

    id_additional_info = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    name = Column(String(cons.STRING_LENGTH), nullable=False)
    id_student = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_STUDENT}.{cons.TABLE_ID_STUDENT}"),
                        nullable=False)
    url_file = Column(String(cons.STRING_URL_FILE_LENGHT))
    date_created = Column(DateTime(timezone=True), nullable=False)

    def to_entity(self):
        return AdditionalInformation(
            idx=self.id_additional_info,
            name=self.name,
            archive=self.url_file,
            student=Student(idx=self.id_student),
            date_created=self.date_created
        )
