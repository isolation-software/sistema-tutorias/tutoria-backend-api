from flask import request
from flask_restful import Resource

from src.app.auth.handler import jwt_required
from src.config import LoggerFactory
from src.controllers.controller_tutor import TutorController
from src.helpers import constants


class TutorsResource(Resource):
    logger = LoggerFactory.get_logger(__name__)

    @jwt_required
    def get(self):
        id_unit = request.args.get('idUnidad', type=str)
        search = request.args.get('search', type=str, default=constants.EMPTY_STRING)
        self.logger.debug(f"Resource - tutores a buscar: search={search} | id_unit={id_unit}")
        response = TutorController.listTutorByUnit(id_unit=id_unit, search=search)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        data = request.get_json()
        response = TutorController.register_tutor(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class TutorScheduleResource(Resource):
    logger = LoggerFactory.get_logger(__name__)

    @jwt_required
    def put(self, id_tutor):
        json = request.get_json()
        response = TutorController.register_availability_time(json, id_tutor)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def get(self, id_tutor):
        list_non_approved = request.args.get("listarNoAprobados", False)
        start_date = request.args.get("fechaInicio")
        end_date = request.args.get("fechaFinal")
        response = TutorController.list_availability_time_for_tutor(id_tutor, list_non_approved, start_date, end_date)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class TutorScheduleReplicateResource(Resource):
    logger = LoggerFactory.get_logger(__name__)

    @jwt_required
    def put(self, id_tutor):
        json = request.get_json()
        response = TutorController.replicate_availability_time(json, id_tutor)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class TutorResource(Resource):
    @jwt_required
    def get(self, id_tutor):
        response = TutorController.get_tutor(id_tutor=id_tutor)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

