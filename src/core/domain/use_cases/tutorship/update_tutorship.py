from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class UpdateTutorshipUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.id_tutorship = req.id_tutorship
        self.tutorship = req.tutorship
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()

    @catch_errors
    def execute(self):
        tutorship = self.repo_tutorship.update_tutorship(self.tutorship)
        self.res = Response(tutorship=tutorship)
        self.success = UpdateTutorshipSuccess


class UpdateTutorshipSuccess(Success):
    code = info_success_logic.UPDATE_TUTORSHIP_OK_CODE
    message = info_success_logic.UPDATE_TUTORSHIP_OK_MESSAGE
