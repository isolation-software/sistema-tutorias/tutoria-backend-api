from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic, constants


class GetAdditionalInfoUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_additional_info=None):
        super().__init__(errors, req)
        # repos
        self.repo_additional_info = repo_additional_info or self.RepoFactory.AdditionalInfoRepo()
        # request data
        self.student = req.student

    @catch_errors
    def execute(self):
        additional_info = self.repo_additional_info.get_additional_info(self.student)
        self.res = Response(additional_info=additional_info)
        self.success = GetAdditionalInfoSuccess


class GetAdditionalInfoSuccess(Success):
    code = info_success_logic.ADDITIONAL_INFO_SEARCH_OK_CODE
    message = info_success_logic.ADDITIONAL_INFO_SEARCH_OK_MESSAGE

