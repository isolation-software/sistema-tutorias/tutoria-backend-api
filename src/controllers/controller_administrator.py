from src.controllers.common import Controller
from src.core.domain.use_cases.administrator import RequestDataAdministratorUseCase
from src.helpers.serializer import str_to_id
from src.core.domain.use_cases.common import Request, Response
from src.serializers import AdministratorSerializer


class AdministratorController(Controller):

    request_data_administrator_uc = RequestDataAdministratorUseCase
    administrator_serializer = AdministratorSerializer

    @classmethod
    def get_administrator(cls, id_administrator):
        id_administrator = str_to_id(id_administrator)
        req = Request(id_administrator=id_administrator)

        return cls.build_response(
            use_case=cls.request_data_administrator_uc,
            req=req,
            serialize_response=cls.administrator_serializer.serialize_administrator_id
        )