from src.config import LoggerFactory
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ReadSessionUseCase(UseCase):

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_session=None, repo_tutorship=None,
                 repo_action_plan=None, repo_student=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.repo_action_plan = repo_action_plan or self.RepoFactory.ActionPlanRepo(self.repo_session.session)
        self.repo_student = repo_student or self.RepoFactory.StudentRepo(self.repo_session.session)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo(self.repo_session.session)
        self.id_session = req.id_session

    @catch_errors
    def execute(self):
        self.logger.debug("UseCase - Iniciando con la búsqueda de sesión por id")
        session = self.repo_session.read_session(self.id_session)
        self.logger.debug("UseCase - Sesión encontrada")
        self.logger.debug("UseCase - Iniciando con la búsqueda del plan de acción para la serie de sesiones respectiva")
        action_plan = self.repo_action_plan.read_action_plan(session.sessions_serie.idx)
        self.logger.debug(f"UseCase - Número de tareas en el plan de acción: {len(action_plan)}")
        self.logger.debug("UseCase - Iniciando con la búsqueda de la tutoría para la serie de sesiones respectiva")
        tutorship = self.repo_tutorship.read_tutorship_by_session_set_id(session.sessions_serie.idx)
        self.logger.debug("UseCase - Iniciando con la búsqueda del alumno para la serie de sesiones respectiva")
        student = self.repo_student.read_student_by_session_serie(session.sessions_serie.idx)
        self.logger.debug(f"UseCase - Alumno encontrado: {student.user.fullname()}")
        unit = student.unit
        self.res = Response(session=session, action_plan=action_plan, student=student, unit=unit, tutorship=tutorship)
        self.success = ReadSessionSuccess


class ReadSessionSuccess(Success):
    code = info_success_logic.SESSION_SEARCH_OK_CODE
    message = info_success_logic.SESSION_SEARCH_OK_MESSAGE
