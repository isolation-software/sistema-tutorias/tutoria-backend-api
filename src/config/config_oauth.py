from src.config.config_general import EnvironConfig


class OAuthConfig:
    _config = EnvironConfig.OAUTH_CONFIG

    ID_CLIENT = _config['OAUTH_ID_CLIENT']
    SECRET_CLIENT = _config['OAUTH_SECRET_CLIENT']
    GOOGLE_SCOPE  = _config['GOOGLE_SCOPE']
