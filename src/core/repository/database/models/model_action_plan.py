from uuid import uuid4

from sqlalchemy import Column, ForeignKey, String, Boolean
from sqlalchemy.dialects.postgresql import UUID

from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class ActionPlanModel(Base):
    __tablename__ = cons.TABLE_NAME_ACTION_PLAN

    id_action_plan = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    description = Column(String(cons.STRING_PLAN_DESCRIPTION_LENGTH))
    state = Column(Boolean, default=False)
    id_session_set = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_SESSION_SET}.{cons.TABLE_ID_SESSION_SET}"))
