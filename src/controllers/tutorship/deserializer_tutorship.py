import dateutil.parser

from src.config import LoggerFactory
from src.core.domain.entities import Tutor, Student, Unit, Tutorship
from src.helpers.serializer import str_to_id


class TutorshipDeserializer:
    logger = LoggerFactory.get_logger(__name__)

    @staticmethod
    def moment_tutorship_data(json):
        session = json["sesion"]
        tutor = Tutor(idx=str_to_id(session["idTutor"]))
        student = Student(idx=str_to_id(session["idAlumno"]))
        start_time = dateutil.parser.parse(session["fechaInicial"])
        unit = Unit(idx=session['idUnidad'])

        return tutor, student, start_time, unit

    @classmethod
    def deserialize_json_register_individual_tutorship(cls, json):
        id_unit = str_to_id(json["idUnidad"])
        json_tutorship = json["tutoria"]
        cls.logger.debug(
            f"Deserializer - caracteristicas {json_tutorship['caracteristicas']} {type(json_tutorship['caracteristicas'])}")
        tutorship = Tutorship(name=json_tutorship["nombre"],
                              description=json_tutorship["descripcion"],
                              sessions_number=int(json_tutorship["numeroSesiones"]),
                              is_obligatory=json_tutorship["caracteristicas"]["esObligatoria"],
                              is_individual=json_tutorship["caracteristicas"]["esIndividual"],
                              is_tutor_assigned=json_tutorship["caracteristicas"]["esTutorAsignado"],
                              is_tutor_permanent=json_tutorship["caracteristicas"]["esTutorFijo"])
        tutor_ids = [str_to_id(t) for t in json_tutorship["tutores"]]
        student_ids = [str_to_id(s) for s in json_tutorship["alumnos"]]
        return id_unit, tutorship, tutor_ids, student_ids

    @classmethod
    def deserialize_json_register_students_individual_tutorship(cls, json):
        student_ids = [str_to_id(s) for s in json["idAlumnos"]]
        return student_ids

    @classmethod
    def deserialize_json_register_tutors_individual_tutorship(cls, json):
        id_unit = str_to_id(json["idUnidad"])
        tutors_ids = [str_to_id(s) for s in json["idTutores"]]
        return id_unit, tutors_ids

    @classmethod
    def desearialize_put_tutorship(cls, json):
        return Tutorship(
            idx=str_to_id(json["idTutoria"]),
            name=json["nombre"],
            description=json["descripcion"],
            sessions_number=int(json["numeroSesiones"]),
            is_obligatory=json["caracteristicas"]["esObligatoria"],
            is_individual=json["caracteristicas"]["esIndividual"],
            is_tutor_assigned=json["caracteristicas"]["esTutorAsignado"],
            is_tutor_permanent=json["caracteristicas"]["esTutorFijo"])
