from src.controllers.common import Controller
from src.controllers.user.deserializer_user import UserDeserializer
from src.controllers.user.serializer_user import UserSerializer
from src.core.domain.entities import Unit
from src.core.domain.use_cases.common import Request, Response
from src.core.domain.use_cases.users import ValidateUserAsStudentByEmailUseCase, \
    ValidateUserAsCoordinatorByEmailUseCase, ListRolesForUserUseCase, ValidateUserAsTutorByEmailUseCase,\
    UpdateUserUseCase
from src.core.domain.use_cases.users.confirmation_register_user import ConfirmationRegisterUserUseCase
from src.core.domain.use_cases.users.send_email_register_user import SendEmailRegisterUserUseCase
from src.core.domain.use_cases.users.validate_google_login import ValidateGoogleLoginUseCase
from src.core.domain.use_cases.users.validate_login import ValidateLoginUseCase
from src.helpers import constants
from src.helpers.serializer import str_to_id


class UserController(Controller):

    deserializer = UserDeserializer
    serializer = UserSerializer
    validate_email_for_student_uc = ValidateUserAsStudentByEmailUseCase

    @classmethod
    def validate_email_for_student(cls, param_email):
        user = cls.deserializer.email_validation_params_to_objects(param_email)
        uc_user_validate = cls.validate_email_for_student_uc(req=Request(user=user))
        uc_user_validate.execute()

        if not uc_user_validate.has_errors():
            res_user = uc_user_validate.res.user
            res_has_role = uc_user_validate.res.has_role
            data = cls.serializer.serialize_data_validate_email_for_student(res_user, res_has_role)

            if res_user is None:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_NO_EXIST_CODE,
                                message=constants.VALIDATE_USER_EMAIL_NO_EXIST_MESSAGE,
                                data=data)
            elif not res_has_role:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_ALREADY_EXISTS_CODE,
                                message=constants.VALIDATE_USER_EMAIL_ALREADY_EXISTS_MESSAGE,
                                data=data)
            else:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_EXISTS_AS_STUDENT_CODE,
                                message=constants.VALIDATE_USER_EMAIL_EXISTS_AS_STUDENT_MESSAGE,
                                data=data)
        else:
            error = uc_user_validate.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message,
                            data={})

    @staticmethod
    def validate_email_for_coordinator(param_email):
        user = UserDeserializer.email_validation_params_to_objects(param_email)
        uc_user_validate = ValidateUserAsCoordinatorByEmailUseCase(req=Request(user=user))
        uc_user_validate.execute()

        if not uc_user_validate.has_errors():
            res_user = uc_user_validate.res.user
            res_has_role = uc_user_validate.res.has_role
            data = UserSerializer.serialize_data_validate_email_for_coordinator(res_user, res_has_role)

            if res_user is None:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_NO_EXIST_CODE,
                                message=constants.VALIDATE_USER_EMAIL_NO_EXIST_MESSAGE,
                                data=data)
            elif not res_has_role:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_ALREADY_EXISTS_CODE,
                                message=constants.VALIDATE_USER_EMAIL_ALREADY_EXISTS_MESSAGE,
                                data=data)
            else:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_EXISTS_AS_COORDINATOR_CODE,
                                message=constants.VALIDATE_USER_EMAIL_EXISTS_AS_COORDINATOR_MESSAGE,
                                data=data)
        else:
            error = uc_user_validate.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message,
                            data={})

    @staticmethod
    def validate_email_for_tutor(param_email, id_unit):
        user = UserDeserializer.email_validation_params_to_objects(param_email)
        unit = Unit(idx=str_to_id(id_unit))
        uc_user_validate = ValidateUserAsTutorByEmailUseCase(req=Request(user=user, unit=unit))
        uc_user_validate.execute()

        if not uc_user_validate.has_errors():
            res_user = uc_user_validate.res.user
            res_has_role = uc_user_validate.res.has_role
            res_same_unit = uc_user_validate.res.same_unit
            data = UserSerializer.serialize_data_validate_email_for_tutor(res_user, res_has_role, res_same_unit)

            if res_user is None:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_NO_EXIST_CODE,
                                message=constants.VALIDATE_USER_EMAIL_NO_EXIST_MESSAGE,
                                data=data)
            elif not res_has_role:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_ALREADY_EXISTS_CODE,
                                message=constants.VALIDATE_USER_EMAIL_ALREADY_EXISTS_MESSAGE,
                                data=data)
            elif not res_same_unit:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_CODE,
                                message=constants.VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_MESSAGE,
                                data=data)
            else:
                return Response(status=constants.OK,
                                code=constants.VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_ON_UNIT_CODE,
                                message=constants.VALIDATE_USER_EMAIL_EXISTS_AS_TUTOR_ON_UNIT_MESSAGE,
                                data=data)
        else:
            error = uc_user_validate.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message,
                            data={})

    list_roles_for_user_uc = ListRolesForUserUseCase

    @classmethod
    def list_roles_for_user(cls, param_email):
        req = Request(email=param_email)
        return cls.build_response(
            use_case=cls.list_roles_for_user_uc,
            req=req,
            serialize_response=cls.serializer.serialize_data_list_roles_for_user
        )

    update_user_uc = UpdateUserUseCase

    @classmethod
    def update_user(cls, data, idx):
        cls.logger.debug(f"Controller - iniciando la actualización del usuario con id {idx}")
        user, role, idx, has_access_to_additional_information, unit = cls.deserializer \
            .user_role_access_and_unit(data, idx)
        cls.logger.debug(f"Controller - el usuario es un {role.value.lower()} con id{role.value} {idx}")
        req = Request(user=user, role=role, idx=idx,
                      unit=unit, has_access_to_additional_information=has_access_to_additional_information)
        return cls.build_response(
            use_case=cls.update_user_uc,
            req=req
        )

    send_email_register_user_uc = SendEmailRegisterUserUseCase

    @classmethod
    def register_user(cls, data):
        email = cls.deserializer.register_user(data)
        req = Request(email=email)
        return cls.build_response(
            use_case=cls.send_email_register_user_uc,
            req=req
        )

    confirmation_register_user_uc = ConfirmationRegisterUserUseCase

    @classmethod
    def confirmation_user(cls, data):
        token, password = cls.deserializer.confirmation_user(data)
        req = Request(token=token, password=password)
        return cls.build_response(
            use_case=cls.confirmation_register_user_uc,
            req=req
        )

    validate_login_uc = ValidateLoginUseCase

    @classmethod
    def validate_login(cls, data):
        email, password = cls.deserializer.validate_login(data)
        req = Request(email=email, password=password)
        return cls.build_response(
            use_case=cls.validate_login_uc,
            req=req,
            serialize_response=cls.serializer.serialize_data_validate_login
        )

    validate_google_login_uc = ValidateGoogleLoginUseCase

    @classmethod
    def validate_google_login(cls, data):
        cls.logger.info('Iniciando validación con google')
        code = cls.deserializer.validate_google_login(data)
        cls.logger.debug(code)
        req = Request(code=code)
        return cls.build_response(
            use_case=cls.validate_google_login_uc,
            req=req,
            serialize_response=cls.serializer.serialize_validate_google_login
        )
