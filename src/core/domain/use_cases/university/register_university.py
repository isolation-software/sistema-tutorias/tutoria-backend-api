from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.core.repository.s3 import S3FileUploader
from src.helpers import info_success_logic, constants


class RegisterUniversityUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_university=None):
        super().__init__(errors, req)
        # repos
        self.repo_university = repo_university or self.RepoFactory.UniversityRepo()
        # s3 connection
        self.s3 = S3FileUploader()
        # request data
        self.university = req.university
        self.logo = req.logo
        self.file_extension = req.file_extension
        self.content_type = constants.IMAGE + req.file_extension

    @catch_errors
    def execute(self):
        self.university.logo = self.s3.upload(file=self.logo, extension=self.file_extension,
                                              content_type=self.content_type)
        university_saved = self.repo_university.register_university(self.university)
        self.res = Response(university=university_saved)
        self.success = RegisterUniversitySuccess


class RegisterUniversitySuccess(Success):
    code = info_success_logic.UNIVERSITY_SAVED_OK_CODE
    message = info_success_logic.UNIVERSITY_SAVED_OK_MESSAGE
