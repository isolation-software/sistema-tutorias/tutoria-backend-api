from src.core.domain.entities.additional_information import AdditionalInformation
from src.core.domain.entities.administrator import Administrator
from src.core.domain.entities.coordinator import Coordinator
from src.core.domain.entities.general_administrator import GeneralAdministrator
from src.core.domain.entities.group import Group
from src.core.domain.entities.history_indicators import HistoryIndicators
from src.core.domain.entities.indicator import Indicator
from src.core.domain.entities.schedule import Schedule
from src.core.domain.entities.session import Session
from src.core.domain.entities.sessions_serie import SessionsSerie
from src.core.domain.entities.student import Student
from src.core.domain.entities.support_unity import SupportUnity
from src.core.domain.entities.tutor import Tutor
from src.core.domain.entities.tutorship import Tutorship
from src.core.domain.entities.unit import Unit
from src.core.domain.entities.university import University
from src.core.domain.entities.user import User

