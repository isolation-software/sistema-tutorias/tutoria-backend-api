from uuid import uuid4

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import Coordinator
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class CoordinatorModel(Base):
    __tablename__ = cons.TABLE_NAME_COORDINATOR

    id_coordinator = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_user        = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_USER}.{cons.TABLE_ID_USER}"))

    user = relationship("UserModel")

    def to_entity(self):
        return Coordinator(
            idx=self.id_coordinator,
            user=self.user.to_entity()
        )
