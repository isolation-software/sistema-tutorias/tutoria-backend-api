from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class SaveIndividualSessionUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_serie=None, repo_session=None, repo_tutor=None):
        super().__init__(errors, req)
        self.repo_serie = repo_serie or self.RepoFactory.SessionsSerieRepo()
        self.repo_session = repo_session or self.RepoFactory.SessionRepo(session=self.repo_serie.session)
        self.repo_tutor = repo_tutor or self.RepoFactory.TutorRepo(session=self.repo_serie.session)

        self.id_tutorship = req.id_tutorship
        self.session = req.session
        self.id_student = req.id_student
        self.id_assigned_tutor = req.id_assigned_tutor

    @catch_errors
    def execute(self):
        serie, id_unit = self.repo_serie.get_serie_of_tutorship(self.id_tutorship, self.id_student)
        self.session.sessions_serie = serie
        tutor_per_unit = self.repo_tutor.get_tutor_per_unit(self.id_assigned_tutor, id_unit)
        self.session.tutor = tutor_per_unit
        session_saved = self.repo_session.update_individual_session(self.session)
        self.res = Response(session=session_saved)
        self.success = SaveIndividualSessionSuccess


class SaveIndividualSessionSuccess(Success):
    code = info_success_logic.INDIVIDUAL_SESSION_SAVED_OK_CODE
    message = info_success_logic.INDIVIDUAL_SESSION_SAVED_OK_MESSAGE
