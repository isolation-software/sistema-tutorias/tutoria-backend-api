import uuid

import boto3
from botocore import UNSIGNED
from botocore.client import Config

from src.config.config_s3 import S3Config
from src.helpers import constants


class S3FileUploader:
    def __init__(self):
        self.client = boto3.client('s3', config=Config(signature_version=UNSIGNED))

    def upload(self, file, content_type, extension):
        key = uuid.uuid4().hex + constants.DOT + extension
        self.client.put_object(Key=key, Bucket=S3Config.BUCKET_NAME, Body=file, ContentType=content_type)
        file_url = S3Config.BUCKET_URL + key
        return file_url
