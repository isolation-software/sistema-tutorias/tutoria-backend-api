from src.controllers.common import Controller
from src.core.domain.entities import Tutor, Student
from src.core.domain.use_cases.appointments import ListAppointmentsUseCase
from src.core.domain.use_cases.common import Request, Response
from src.core.domain.use_cases.sessions import ListSessionsForTutorUseCase
from src.core.domain.use_cases.tutorship import RegisterTutorshipMomentUseCase, ListTutorshipHistoryUseCase, \
    RegisterIndividualTutorshipUseCase, ListMomentTutorshipSessionForStudentUseCase, GetTutorshipUseCase, \
    ListTutorshipByUnitUseCase
from src.controllers.tutorship.deserializer_tutorship import TutorshipDeserializer
from src.core.domain.use_cases.tutorship.delete_student_individual_tutorship import \
    DeleteStudentIndividualTutorshipUseCase
from src.core.domain.use_cases.tutorship.delete_tutor_individual_tutorship import DeleteTutorIndividualTutorshipUseCase
from src.core.domain.use_cases.tutorship.list_students_notin_individual_tutorship import \
    ListStudentsIndividualTutorshipUseCase
from src.core.domain.use_cases.tutorship.list_tutors_individual_tutorship_by_student import \
    ListTutorsIndividualTutorshipByStudentUseCase
from src.core.domain.use_cases.tutorship.list_tutors_individual_tutorship_per_student import \
    ListTutorsIndividualTutorshipPerStudentUseCase
from src.core.domain.use_cases.tutorship.list_tutors_notin_individual_tutorship import \
    ListTutorsIndividualTutorshipUseCase
from src.core.domain.use_cases.tutorship.register_students_individual_tutorship import \
    RegisterStudentsIndividualTutorshipUseCase
from src.core.domain.use_cases.tutorship.register_tutors_individual_tutorship import \
    RegisterTutorsIndividualTutorshipUseCase
from src.core.domain.use_cases.tutorship.update_tutorship import UpdateTutorshipUseCase
from src.helpers import constants
from src.helpers.serializer import str_to_id
from src.controllers.session.serializer_session import SessionSerializer
from src.controllers.tutorship.serializer_tutorship import TutorshipSerializer
from src.serializers import AppointmentSerializer


class TutorshipController(Controller):
    get_tutorship_uc = GetTutorshipUseCase
    put_tutorship_uc = UpdateTutorshipUseCase
    list_tutorship_by_unit_uc = ListTutorshipByUnitUseCase
    register_tutorship_moment_uc = RegisterTutorshipMomentUseCase
    register_individual_tutorship_uc = RegisterIndividualTutorshipUseCase
    register_students_individual_tutorship_uc = RegisterStudentsIndividualTutorshipUseCase
    register_tutors_individual_tutorship_uc = RegisterTutorsIndividualTutorshipUseCase
    list_students_individual_tutorship_uc = ListStudentsIndividualTutorshipUseCase
    list_tutors_individual_tutorship_uc = ListTutorsIndividualTutorshipUseCase

    tutorship_serializer = TutorshipSerializer
    tutorship_deserializer = TutorshipDeserializer

    @classmethod
    def register_tutorship_moment(cls, json):
        tutor, student, start_time, unit = TutorshipDeserializer.moment_tutorship_data(json)
        req = Request(tutor=tutor, student=student, start_time=start_time, unit=unit)
        return cls.build_response(
            use_case=cls.register_tutorship_moment_uc,
            req=req,
            serialize_response=cls.tutorship_serializer.serialize_register_tutorship_moment_response
        )

    @staticmethod
    def list_tutorship_sessions_by_tutor(id_tutor, search, show_all=False):
        tutor = Tutor(idx=str_to_id(id_tutor))
        uc_list_sessions = ListSessionsForTutorUseCase(req=Request(tutor=tutor, search=search, show_all=show_all))
        uc_list_sessions.execute()

        if not uc_list_sessions.has_errors():
            sessions = uc_list_sessions.res.sessions
            serialized = SessionSerializer.session_list(sessions)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_TUTORSHIP_OK_MESSAGE,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_TUTORSHIP_ERROR_MESSAGE,
                            data={})

    @staticmethod
    def list_moment_tutorship_sessions_for_student(id_student):
        student = Student(idx=str_to_id(id_student))
        use_case_list_moment_tutorship_for_student = ListMomentTutorshipSessionForStudentUseCase(
            req=Request(student=student))
        use_case_list_moment_tutorship_for_student.execute()

        if not use_case_list_moment_tutorship_for_student.has_errors():
            sessions = use_case_list_moment_tutorship_for_student.res.sessions
            serialized = SessionSerializer.session_list_per_tutorship_per_student(sessions)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_SESSIONS_OK_MESSAGE,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_SESSIONS_ERROR_MESSAGE,
                            data={})

    @staticmethod
    def list_tutorship_for_student(id_student, search):
        student = Student(idx=str_to_id(id_student))
        use_case_list_tutorship_for_student = ListTutorshipHistoryUseCase(req=Request(student=student, search=search))
        use_case_list_tutorship_for_student.execute()

        if not use_case_list_tutorship_for_student.has_errors():
            tutorships = use_case_list_tutorship_for_student.res.tutorships
            serialized = TutorshipSerializer.tutorship_list_per_student(tutorships)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_TUTORSHIP_OK_MESSAGE,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_TUTORSHIP_ERROR_MESSAGE,
                            data={})

    @classmethod
    def register_individual_tutorship(cls, json):
        unit_id, tutorship, tutor_ids, student_ids = cls.tutorship_deserializer \
            .deserialize_json_register_individual_tutorship(json)
        req = Request(unit_id=unit_id, tutorship=tutorship, tutor_ids=tutor_ids, student_ids=student_ids)
        return cls.build_response(use_case=cls.register_individual_tutorship_uc, req=req)

    @classmethod
    def get_tutorship(cls, data_id_tutorship):
        id_tutorship = str_to_id(data_id_tutorship)
        req = Request(id_tutorship=id_tutorship)
        return cls.build_response(cls.get_tutorship_uc, req, cls.tutorship_serializer.get_tutorship)

    @classmethod
    def put_tutorship(cls, id_tutorship, data):
        id_tutorship = str_to_id(id_tutorship)
        tutorship = cls.tutorship_deserializer.desearialize_put_tutorship(data)
        req = Request(id_tutorship=id_tutorship, tutorship=tutorship)
        return cls.build_response(cls.put_tutorship_uc, req)

    @classmethod
    def list_tutorship_for_unit(cls, data_id_unit, search):
        id_unit = str_to_id(data_id_unit)
        req = Request(id_unit=id_unit, search=search)
        return cls.build_response(
            use_case=cls.list_tutorship_by_unit_uc,
            req=req,
            serialize_response=cls.tutorship_serializer.serialize_list_tutorship_for_unit_response
        )

    @classmethod
    def register_students_individual_tutorship(cls, id_tutorship, json):
        students_id = cls.tutorship_deserializer.deserialize_json_register_students_individual_tutorship(json)
        req = Request(id_tutorship=id_tutorship, students_id=students_id)
        return cls.build_response(
            use_case=cls.register_students_individual_tutorship_uc,
            req=req)

    @classmethod
    def register_tutors_individual_tutorship(cls, id_tutorship, json):
        id_unit, tutor_ids = cls.tutorship_deserializer.deserialize_json_register_tutors_individual_tutorship(json)
        req = Request(id_tutorship=id_tutorship, tutor_ids=tutor_ids, id_unit=id_unit)
        return cls.build_response(
            use_case=cls.register_tutors_individual_tutorship_uc,
            req=req)

    @classmethod
    def list_students_individual_tutorship(cls, id_tutorship, search, limit, which):
        id_tutorship = str_to_id(id_tutorship)
        req = Request(id_tutorship=id_tutorship, search=search, limit=limit, filter=which)
        return cls.build_response(
            use_case=cls.list_students_individual_tutorship_uc,
            req=req,
            serialize_response=cls.tutorship_serializer.serialize_list_students_individual_tutorship_response
        )

    @classmethod
    def list_tutors_individual_tutorship(cls, id_tutorship, search, limit, which):
        id_tutorship = str_to_id(id_tutorship)
        req = Request(id_tutorship=id_tutorship, search=search, limit=limit, filter=which)
        return cls.build_response(
            use_case=cls.list_tutors_individual_tutorship_uc,
            req=req,
            serialize_response=cls.tutorship_serializer.serialize_list_tutors_individual_tutorship_response
        )

    list_tutors_individual_tutorship_per_student_uc = ListTutorsIndividualTutorshipPerStudentUseCase
    list_tutors_individual_tutorship_by_student_uc = ListTutorsIndividualTutorshipByStudentUseCase

    @classmethod
    def list_tutors_individual_tutorship_per_student(cls,id_tutorship, id_student, is_coordinator):
        id_tutorship = str_to_id(id_tutorship)
        id_student = str_to_id(id_student)
        req = Request(id_tutorship=id_tutorship, id_student=id_student)
        if is_coordinator.lower() == "true":
            return cls.build_response(
                use_case=cls.list_tutors_individual_tutorship_per_student_uc,
                req=req,
                serialize_response=cls.tutorship_serializer.serialize_list_tutors_individual_tutorship_response
            )
        else:
            return cls.build_response(
                use_case=cls.list_tutors_individual_tutorship_by_student_uc,
                req=req,
                serialize_response=cls.tutorship_serializer.serialize_list_tutors_individual_tutorship_response
            )
        
    list_appointments_uc = ListAppointmentsUseCase
    appointment_serializer = AppointmentSerializer

    @classmethod
    def list_appointments(cls, id_student, search, limit):
        id_student = str_to_id(id_student)
        req = Request(id_student=id_student, search=search, limit=limit)
        return cls.build_response(
            use_case=cls.list_appointments_uc,
            req=req,
            serialize_response=cls.appointment_serializer.serialize_list_appointment_response
        )

    delete_student_individual_tutorship_uc = DeleteStudentIndividualTutorshipUseCase

    @classmethod
    def delete_student_individual_tutorship(cls, id_tutorship, id_student):
        id_tutorship = str_to_id(id_tutorship)
        id_student = str_to_id(id_student)
        req = Request(id_tutorship=id_tutorship, id_student=id_student)
        return cls.build_response(
            use_case=cls.delete_student_individual_tutorship_uc,
            req=req,
        )

    delete_tutor_individual_tutorship_uc = DeleteTutorIndividualTutorshipUseCase

    @classmethod
    def delete_tutor_individual_tutorship(cls, id_tutorship, id_tutor):
        id_tutorship = str_to_id(id_tutorship)
        id_tutor = str_to_id(id_tutor)
        req = Request(id_tutorship=id_tutorship, id_tutor=id_tutor)
        return cls.build_response(
            use_case=cls.delete_tutor_individual_tutorship_uc,
            req=req,
        )
