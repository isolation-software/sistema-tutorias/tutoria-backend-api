from uuid import uuid4

from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID

from src.core.domain.entities.valid_email import ValidEmail
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class ValidEmailModel(Base):
    __tablename__ = cons.TABLE_NAME_VALID_EMAIL

    id_valid_email = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_university = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_UNIVERSITY}.{cons.TABLE_ID_UNIVERSITY}"))
    prefix = Column(String(cons.STRING_SHORT_LENGTH))
    domain = Column(String(cons.STRING_SHORT_LENGTH))
    sponsored_tld = Column(String(cons.STRING_SHORT_LENGTH))
    top_level_domain = Column(String(cons.STRING_SHORT_LENGTH))

    def to_entity(self):
        return ValidEmail(
            idx=self.id_valid_email,
            prefix=self.prefix,
            domain=self.domain,
            sponsored_tld=self.sponsored_tld,
            top_level_domain=self.top_level_domain
        )