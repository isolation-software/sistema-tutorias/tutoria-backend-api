from uuid import uuid4

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID

from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class SetPerStudentModel(Base):
    __tablename__ = cons.TABLE_NAME_SET_PER_STUDENT

    id_set_per_student = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_session_set = Column(UUID(as_uuid=True),
                            ForeignKey(f"{cons.TABLE_NAME_SESSION_SET}.{cons.TABLE_ID_SESSION_SET}"))
    id_student = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_STUDENT}.{cons.TABLE_ID_STUDENT}"))