from flask_restful import Resource
from flask import request

from src.controllers.reports import ReportController
from src.helpers import constants


class ReportTutorshipsResource(Resource):
    def get(self, idunit):
        fechaIni = request.args.get('fechaIni', constants.EMPTY_STRING)
        fechaFin = request.args.get('fechaFin', constants.EMPTY_STRING)
        tutorship_response = ReportController.get_tutorships_report(idunit, fechaIni, fechaFin)
        return tutorship_response


class ReportTutorshipsMomentResource(Resource):
    def get(self, idunit):
        fechaIni = request.args.get('fechaIni', constants.EMPTY_STRING)
        fechaFin = request.args.get('fechaFin', constants.EMPTY_STRING)
        tutorship_moment_response = ReportController.get_tutorships_moment_report(idunit, fechaIni, fechaFin)
        return tutorship_moment_response


class ReportTutorsResource(Resource):
    def get(self, idunit):
        fechaIni = request.args.get('fechaIni', constants.EMPTY_STRING)
        fechaFin = request.args.get('fechaFin', constants.EMPTY_STRING)
        tutors_response = ReportController.get_tutors_report(idunit, fechaIni, fechaFin)
        return tutors_response
