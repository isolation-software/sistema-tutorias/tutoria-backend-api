import dateutil.parser

from src.core.domain.entities import Tutor, Unit
from src.core.domain.entities.availability_date import AvailabilityDate
from src.helpers.serializer import str_to_id, replicate_dates_from_list


class AvailabilityTimeDeserializer:

    @staticmethod
    def availability_dates(json, id_tutor):
        unit = Unit(idx=str_to_id(json.get("idUnidad")))
        return unit, [AvailabilityDate(
            time=dateutil.parser.parse(date),
            state=True,
            tutor=Tutor(idx=str_to_id(id_tutor)),
            approved=json.get("aprobado", False)
        ) for date in json.get("bloques")] if json.get("bloques") else None

    @staticmethod
    def replicate_dates(json, id_tutor):
        unit = Unit(idx=str_to_id(json.get("idUnidad")))
        origin = json["bloques"]
        start_date = json["rangoFechas"]["fechaInicio"]
        end_date = json["rangoFechas"]["fechaFin"]
        replicated_dates= replicate_dates_from_list(start_date, end_date, origin)
        return unit, [AvailabilityDate(
            time=date,
            state=True,
            tutor=Tutor(idx=str_to_id(id_tutor)),
            approved=json.get("aprobado", False)
        ) for date in replicated_dates]
