from src.config.config_general import EnvironConfig


class FlaskConfig:
    _config = EnvironConfig.API_CONFIG

    DEBUG  = _config['API_DEBUG']
    PORT   = _config['API_PORT']
    HOST   = _config['API_HOST']
    SECRET_KEY = _config['API_SECRET_KEY']
    EXP_TIME = int(_config['API_EXPIRATION_TIME'])
    CORS_HEADERS = _config['API_CORS_HEADERS']
