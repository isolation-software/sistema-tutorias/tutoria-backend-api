from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import StudentNotDeleteInSystemError
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class DeleteStudentIndividualTutorshipUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_sessions_serie=None):
        super().__init__(errors, req)
        self.repo_sessions_serie = repo_sessions_serie or self.RepoFactory.SessionsSerieRepo()
        self.id_tutorship = req.id_tutorship
        self.id_student = req.id_student

    @catch_errors
    def execute(self):
        is_possible = self.repo_sessions_serie.delete_student_individual_tutorship(self.id_tutorship, self.id_student)
        if not is_possible:
            raise StudentNotDeleteInSystemError()
        self.res = Response()
        self.success = DeleteStudentIndividualTutorshipSuccess


class DeleteStudentIndividualTutorshipSuccess(Success):
    code = info_success_logic.DELETE_STUDENT_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.DELETE_STUDENT_INDIVIDUAL_TUTORSHIP_OK_MESSAGE
