from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListSupportUnitsUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_support_unit=None):
        super().__init__(errors, req)
        # repos
        self.repo_support_unit = repo_support_unit or self.RepoFactory.SupportUnitRepo()
        # request data

    @catch_errors
    def execute(self):
        support_units = self.repo_support_unit.read_support_unit()
        self.res = Response(support_units=support_units)
        self.success = ListSupportUnitsSuccess


class ListSupportUnitsSuccess(Success):
    code = info_success_logic.LIST_SUPPORT_UNITS_OK_CODE
    message = info_success_logic.LIST_SUPPORT_UNITS_OK_MESSAGE
