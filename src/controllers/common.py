from src.config import LoggerFactory
from src.core.domain.use_cases.common import Response
from src.helpers import constants


class Controller:

    logger = LoggerFactory.get_logger(__name__)

    @classmethod
    def build_response(cls, use_case, req, serialize_response=None):
        """
        Method that executes the usecase passed with the requests built before, and with the usecase
        execution response, it will instantiate the reponse that will sent to the user as a rest response.
        :param use_case: UseCase class that will be used to instantiate the usecase and execute it
        :param req: Request object, it will be passed to the usecase
        :param serialize_response: serializer method used to serialize the usecase response to json data
        :return: Response object to send to the Rest Resource
        """
        cls.logger.debug(f"Controller common - ejecutando usecase {use_case.__name__}")
        uc = use_case(req=req)
        uc.execute()
        if not uc.has_errors():
            cls.logger.debug("Controller common - No errores en la ejecución de usecase")
            success = uc.get_success()
            cls.logger.debug(f"Controller common - exitoso: {success.code} {success.message}")
            data = serialize_response(uc.res) if serialize_response else {}
            cls.logger.debug("Controller common - enviando respuesta")
            return Response(status=constants.OK,
                            code=success.code,
                            message=success.message,
                            data=data)
        else:
            cls.logger.debug("Controller common - Hubo errores en la ejecución de usecase")
            error = uc.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message)
