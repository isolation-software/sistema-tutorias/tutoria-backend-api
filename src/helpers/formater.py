import datetime
import pytz
from dateutil.tz import tzutc

from src.helpers import constants


def convert_to_peru_time(datetime_obj, with_hour=False, str_format=False):

    # UTC-5 (Lima) timezone
    tz = pytz.timezone(constants.PERU_TIMEZONE)
    # Localise
    print(datetime_obj)
    datetime_obj = pytz.utc.localize(datetime_obj.astimezone(tzutc()).replace(tzinfo=None), is_dst=None).astimezone(tz)

    if with_hour:
        return datetime_obj.strftime("%d/%m/%Y %H:%M:%S")
    else:
        return datetime_obj.strftime("%d/%m/%Y")


def no_double_space(sentence):
    return " ".join(str(sentence).split())
