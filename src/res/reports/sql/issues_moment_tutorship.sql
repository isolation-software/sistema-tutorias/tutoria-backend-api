SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'ACADÉMICA' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Académica%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'ACADÉMICO-ADMINISTRATIVO' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Académico – administrativo%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'VOCACIONAL' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Vocacional%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'PERSONAL' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Personal%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'FAMILIAR' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Familiar%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'INDIVIDUAL' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Individual%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'ECONÓMICO' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Económico%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A
UNION ALL
SELECT A.TIPO, A.CANTIDAD_SESIONES
FROM(
    SELECT 'PSICOLÓGICO' AS TIPO, COUNT(*) AS CANTIDAD_SESIONES
    FROM "MT_INDICATOR" MI
    INNER JOIN "MT_SESSION" MS on MI.id_session = MS.id_session
    INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
    INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
    WHERE UPPER(MT.name) LIKE '%MOMENTO%' AND MS.state = 'Atendido' AND MI.indicator_6 LIKE ('%Psicológico%')
    AND MT.id_unit = :id_unit
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
)A;