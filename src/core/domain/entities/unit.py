from src.helpers.serializer import id_to_str


class Unit:

    def __init__(self, idx=None, name=None, code=None, typex=None, coordinator=None, father_unit=None):
        self.idx = idx
        self.name  = name
        self.code  = code
        self.typex = typex
        self.coordinator = coordinator
        self.father_unit = father_unit
        
        
    def to_dict(self):
        return dict(
            idx=id_to_str(self.idx),
            name=self.name,
            code=self.code,
            typex=self.typex.value
        )
