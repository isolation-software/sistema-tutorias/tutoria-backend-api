from src.config.config_general import EnvironConfig


class S3Config:
    _config = EnvironConfig.S3_CONFIG

    BUCKET_NAME = _config['BUCKET_NAME']
    BUCKET_URL  = _config["BUCKET_URL"]
    ACL_STR     = _config["ACL_STR"]
