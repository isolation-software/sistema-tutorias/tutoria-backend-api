from src.config import LoggerFactory
from src.controllers.common import Controller
from src.core.domain.use_cases.common import Response, Request
from src.core.domain.use_cases.students import ListStudentPerUnitUseCase
from src.core.domain.use_cases.units import ListUnitsUseCase, GetUnitUseCase, ListFacultiesUseCase, \
    RegisterFacultyUseCase, RegisterProgramUseCase, UpdateUnitUseCase
from src.core.domain.entities import Unit, Tutor
from src.deserializers import UnitDeserializer
from src.helpers import constants
from src.helpers.serializer import str_to_id
from src.serializers.serializer_student import StudentSerializer
from src.serializers.serializer_unit import UnitSerializer


class UnitController(Controller):
    logger = LoggerFactory.get_logger(__name__)

    get_uc = GetUnitUseCase
    register_program_uc = RegisterProgramUseCase

    unit_serializer = UnitSerializer
    unit_deserializer = UnitDeserializer

    @classmethod
    def list_units(cls, id_tutor=None):
        cls.logger.debug("Iniciando la ejecución del controlador para el listado de unidades")
        tutor = Tutor(idx=str_to_id(id_tutor)) if id_tutor else None
        uc_list_units = ListUnitsUseCase(req=Request(tutor=tutor))
        uc_list_units.execute()

        if not uc_list_units.has_errors():
            cls.logger.debug(
                "El caso de no uso posee errores por listado de unidades, iniciando la respuesta con estado ok")
            units = uc_list_units.res.units
            serialized = UnitSerializer.serialize_units_few_data(units)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_UNITS_OK_MESSAGE,
                            data=serialized)
        else:
            cls.logger.debug(
                "El caso de uso posee errores por listado de unidades, iniciando la respuesta con estado ok")
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_UNITS_ERROR_MESSAGE,
                            data={})

    @classmethod
    def request_unit(cls, id_unit_param):
        cls.logger.debug("Iniciando la ejecución del controlador para la solicitud de una unidad")
        unit = Unit(idx=str_to_id(id_unit_param))
        uc_get_unit = cls.get_uc(req=Request(unit=unit))
        uc_get_unit.execute()

        if not uc_get_unit.has_errors():
            cls.logger.debug("El caso de no uso posee errores por obtener unidad, iniciando la respuesta con estado ok")
            success = uc_get_unit.get_success()
            unit = uc_get_unit.res.unit
            programs = uc_get_unit.res.programs
            data = cls.unit_serializer.serialize_units_and_programs_all_data(unit, programs)
            return Response(status=constants.OK,
                            code=success.code,
                            message=success.message,
                            data=data)
        else:
            cls.logger.debug(
                "El caso de uso posee errores por obtener unidad, iniciando la respuesta con estado de error")
            error = uc_get_unit.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message)

    @staticmethod
    def register_faculty(json):
        faculty = UnitDeserializer.faculty_data(json)
        uc_register_faculty = RegisterFacultyUseCase(req=Request(faculty=faculty))
        uc_register_faculty.execute()

        if not uc_register_faculty.has_errors():
            success = uc_register_faculty.get_success()
            return Response(status=constants.OK,
                            code=success.code,
                            message=success.message,
                            data={})
        else:
            error = uc_register_faculty.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message,
                            data={})

    @classmethod
    def register_unit_program(cls, data):
        cls.logger.debug("Iniciando la ejecución del controlador para la solicitud de una unidad")
        program = cls.unit_deserializer.deserialize_unit_for_program_register(data)
        uc_reg_program = cls.register_program_uc(req=Request(program=program))
        uc_reg_program.execute()

        if not uc_reg_program.has_errors():
            cls.logger.debug(
                "El caso de uso no posee errores por registrar un programa, iniciando la respuesta con estado ok")
            success = uc_reg_program.get_success()
            return Response(status=constants.OK,
                            code=success.code,
                            message=success.message,
                            data=data)
        else:
            cls.logger.debug("Errores encontrados al registrar un programa, iniciando la respuesta con estado de error")
            error = uc_reg_program.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message)

    @staticmethod
    def list_faculties(search, limit):
        uc_list_faculty = ListFacultiesUseCase(req=Request(search=search, limit=limit))
        uc_list_faculty.execute()

        if not uc_list_faculty.has_errors():
            faculties = uc_list_faculty.res.faculties
            serialized = UnitSerializer.serialize_units_few_data(faculties)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_UNITS_OK_MESSAGE,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_UNITS_ERROR_MESSAGE,
                            data={})

    update_unit_uc = UpdateUnitUseCase

    @classmethod
    def update_unit(cls, id_unit, json):
        unit = UnitDeserializer.update_unit_data(id_unit, json)
        req = Request(unit=unit)

        return cls.build_response(
            use_case=cls.update_unit_uc,
            req=req
        )

    list_student_per_unit_uc = ListStudentPerUnitUseCase
    student_serializer = StudentSerializer

    @classmethod
    def list_student_per_unit(cls, id_unit, search, limit):
        req = Request(id_unit=id_unit, search=search, limit=limit)

        return cls.build_response(
            use_case=cls.list_student_per_unit_uc,
            req=req,
            serialize_response=cls.student_serializer.student_list_unit
        )