from src.config import LoggerFactory, FlaskConfig
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import TokenRegisterInvalid, TokenRegisterExpired
from src.core.domain.use_cases.common import UseCase, Response, Success
import jwt

from src.helpers import info_success_logic
from src.helpers.security import hash_password


class ConfirmationRegisterUserUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_user=None):
        super().__init__(errors, req)
        self.user = None
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        self.token = req.token
        self.password = req.password

    @catch_errors
    def execute(self):
        try:
            decoded = jwt.decode(self.token, FlaskConfig.SECRET_KEY)
        except jwt.DecodeError:
            raise TokenRegisterInvalid
        except jwt.ExpiredSignatureError:
            raise TokenRegisterExpired
        email = decoded['correo']
        hashed, salt = hash_password(self.password)
        user_updated = self.repo_user.update_password_user(email, hashed, salt)
        self.res = Response(user=user_updated)
        self.success = ConfirmationRegisterUserSuccess


class ConfirmationRegisterUserSuccess(Success):
    code = info_success_logic.REGISTER_PASSWORD_USER_OK_CODE
    message = info_success_logic.REGISTER_PASSWORD_USER_OK_MESSAGE
