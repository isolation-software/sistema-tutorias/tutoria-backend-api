from uuid import uuid4

from sqlalchemy import Column, ForeignKey, DateTime, Boolean
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities.availability_date import AvailabilityDate
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class AvailabilityDateModel(Base):
    __tablename__ = cons.TABLE_NAME_AVAILABILITY_DATE

    id_availability_date = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    time = Column(DateTime(timezone=True), nullable=False)
    state = Column(Boolean, nullable=False)
    id_tutor_per_unit = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_TUTOR_PER_UNIT}.{cons.TABLE_ID_TUTOR_PER_UNIT}"))
    id_session = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_SESSION}.{cons.TABLE_ID_SESSION}"))
    approved = Column(Boolean, nullable=False)

    session = relationship("SessionModel")
    tutor_per_unit = relationship("TutorPerUnitModel")

    def to_entity(self):
        return AvailabilityDate(
            idx=self.id_availability_date,
            time=self.time,
            state=self.state,
            tutor=self.tutor_per_unit.tutor.to_entity() if self.tutor_per_unit and self.tutor_per_unit.tutor else None,
            unit=self.tutor_per_unit.unit.to_entity() if self.tutor_per_unit and self.tutor_per_unit.unit else None,
            session=self.session.to_entity() if self.session else None,
            approved=self.approved
        )
