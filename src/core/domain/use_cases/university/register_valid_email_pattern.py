from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterValidEmailPatternsUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_valid_email=None):
        super().__init__(errors, req)
        # repos
        self.repo_valid_email = repo_valid_email or self.RepoFactory.ValidEmailRepo()
        # request data
        self.emails = req.emails

    @catch_errors
    def execute(self):
        emails_saved = self.repo_valid_email.register_valid_email_pattern(self.emails)
        self.res = Response(emails_saved=emails_saved)
        self.success = RegisterValidEmailPatternsSuccess


class RegisterValidEmailPatternsSuccess(Success):
    code = info_success_logic.VALID_EMAIL_SAVED_OK_CODE
    message = info_success_logic.VALID_EMAIL_SAVED_OK_MESSAGE
