
class Student:

    def __init__(self, idx=None, user=None, unit=None):
        self.idx = idx
        self.user = user
        self.unit = unit

    @classmethod
    def from_dict(cls, dictionary):
        student = cls()
        student.idx  = dictionary.get('idx', None)
        student.user = dictionary.get('user', None)
        student.unit = dictionary.get('unit', None)
        return student

    def __eq__(self, other):
        return self.idx == other.idx
