from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Success
from src.helpers import info_success_logic, constants


class DeleteAdditionalInfoUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_additional_info=None):
        super().__init__(errors, req)
        # repos
        self.repo_additional_info = repo_additional_info or self.RepoFactory.AdditionalInfoRepo()
        # request data
        self.additional_info = req.additional_info

    @catch_errors
    def execute(self):
        self.repo_additional_info.delete_additional_info(self.additional_info.idx)
        self.success = DeleteAdditionalInfo


class DeleteAdditionalInfo(Success):
    code = info_success_logic.ADDITIONAL_INFO_DELETE_OK_CODE
    message = info_success_logic.ADDITIONAL_INFO_DELETE_OK_MESSAGE
