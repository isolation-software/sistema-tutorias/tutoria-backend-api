from flask import Blueprint
from flask_restful import Api

from src.app.api.restful.bp_administrator import AdministratorResource, AdministratorGetResource
from src.app.api.restful.bp_report import ReportTutorshipsResource, ReportTutorshipsMomentResource, ReportTutorsResource
from src.app.api.restful.bp_university import UniversityResource, UniversityEmailResource
from src.app.api.restful.bp_coordinator import CoordinatorsResource, CoordinatorAuthResource, CoordinatorResource
from src.app.api.restful.bp_support_unit import SupportUnitResource
from src.app.api.restful.bp_unit import UnitsResource, UnitResource, UnitProgramsResource, UnitFacultyResource, \
    UnitStudentResource
from src.app.api.restful.bp_tutor import TutorsResource, TutorScheduleResource, TutorScheduleReplicateResource, \
    TutorResource
from src.app.api.restful.bp_session import SessionsResource, SessionResource, SessionStatesResource, \
    SessionsIndividualTutorshipResource, AppointmentRecordResource, SessionsIndividualStudentTutorshipResource, \
    SessionIndicatorResource
from src.app.api.restful.bp_student import (
    StudentResource,
    StudentsResource,
    StudentAuthResource,
    StudentAdditionalInfoResource,
    StudentAdditionalInfosResource
)
from src.app.api.restful.bp_tutorship import TutorshipsMomentResource, StudentsTutorshipsMomentResource, \
    IndividualTutorshipResource, TutorshipsResource, TutorshipResource, SessionIndividualTutorshipResource, \
    AllTutorshipsResource, IndividualTutorshipStudentsResource, IndividualTutorshipTutorsResource, \
    IndividualTutorshipAvailabilityResource, AppointmentStudentResource, IndividualTutorshipStudentResource, \
    IndividualTutorshipTutorResource
from src.app.api.restful.bp_user import UserValidationStudentResource, UserValidationCoordinatorResource, \
    UserRolesResource, UserValidationTutorResource, UserResource, UserRegister, UserConfirmation, UserValidateLogin, \
    UserValidateGoogleLogin
from src.config import LoggerFactory

logger = LoggerFactory.get_logger(__name__)


def _add_student_related_reources(api):
    api.add_resource(StudentResource, '/alumnos/<string:id_student>')
    api.add_resource(StudentsResource, '/alumnos')
    api.add_resource(StudentAuthResource, '/alumnnos/auth')
    api.add_resource(StudentAdditionalInfoResource, '/alumnos/<string:id_student>/info')
    api.add_resource(StudentAdditionalInfosResource, '/alumnos/<string:id_student>/info/<string:id_additional_info>')


def _add_tutorship_related_reources(api):
    api.add_resource(TutorshipsMomentResource, "/tutorias/momento/sesiones")
    api.add_resource(StudentsTutorshipsMomentResource, '/tutorias/momento/tutores/<string:id_tutor>/alumnos')
    api.add_resource(TutorshipsResource, '/tutorias')
    api.add_resource(TutorshipResource, '/tutorias/<string:id_tutorship>')
    api.add_resource(IndividualTutorshipResource, '/tutorias/individual')
    api.add_resource(SessionIndividualTutorshipResource, '/tutorias/individual/<string:id_tutorship>/<string:id_student>/sesiones/<string:id_session>')
    api.add_resource(SessionsIndividualTutorshipResource, '/tutorias/individual/<string:id_tutorship>/sesiones')
    api.add_resource(SessionsIndividualStudentTutorshipResource, '/tutorias/individual/<string:id_tutorship>/<string:id_student>/sesiones')
    api.add_resource(AllTutorshipsResource, '/tutorias/todos')
    api.add_resource(IndividualTutorshipStudentsResource, '/tutorias/individual/<string:id_tutorship>/alumnos')
    api.add_resource(IndividualTutorshipStudentResource, '/tutorias/individual/<string:id_tutorship>/alumnos/<string:id_student>')
    api.add_resource(IndividualTutorshipTutorsResource, '/tutorias/individual/<string:id_tutorship>/tutores')
    api.add_resource(IndividualTutorshipTutorResource, '/tutorias/individual/<string:id_tutorship>/tutores/<string:id_tutor>')
    api.add_resource(IndividualTutorshipAvailabilityResource, '/tutorias/individual/<string:id_tutorship>/alumnos/<string:id_student>/tutores')
    api.add_resource(AppointmentStudentResource, '/citas/<string:id_student>')
    api.add_resource(AppointmentRecordResource, '/citas/historial')


def _add_session_related_reources(api):
    api.add_resource(SessionsResource, '/sesiones')
    api.add_resource(SessionResource, '/sesiones/<string:id_session>')
    api.add_resource(SessionStatesResource, '/sesiones/estados')
    api.add_resource(SessionIndicatorResource, '/sesiones/indicadores')


def _add_unit_related_reources(api):
    api.add_resource(UnitsResource, '/unidades')
    api.add_resource(UnitResource, '/unidades/<string:id_unit>')
    api.add_resource(UnitProgramsResource, '/unidades/programas')
    api.add_resource(UnitFacultyResource, '/unidades/facultad')
    api.add_resource(UnitStudentResource, '/unidades/<string:id_unit>/alumnos')


def _add_user_related_reources(api):
    api.add_resource(UserValidationStudentResource, '/usuarios/validacion/alumno')
    api.add_resource(UserValidationCoordinatorResource, '/usuarios/validacion/coordinador')
    api.add_resource(UserValidationTutorResource, '/usuarios/validacion/tutor')
    api.add_resource(UserRolesResource, '/usuarios/roles')
    api.add_resource(UserResource, '/usuarios/<string:idx>')
    api.add_resource(UserRegister, '/usuarios/registro')
    api.add_resource(UserConfirmation, '/usuarios/confirmar')
    api.add_resource(UserValidateLogin, '/usuarios/login')
    api.add_resource(UserValidateGoogleLogin, '/usuarios/login/google')


def _add_administrator_related_resources(api):
    api.add_resource(AdministratorResource, '/administrador')
    api.add_resource(AdministratorGetResource, '/administrador/<string:id_administrator>')


def _add_reports_related_resources(api):
    api.add_resource(ReportTutorshipsResource, '/reportes/<string:idunit>/tutorias')
    api.add_resource(ReportTutorshipsMomentResource, '/reportes/<string:idunit>/tutoriasalmomento')
    api.add_resource(ReportTutorsResource, '/reportes/<string:idunit>/tutores')


def _add_coordinator_related_reources(api):
    api.add_resource(CoordinatorsResource, '/coordinadores')
    api.add_resource(CoordinatorAuthResource, '/coordinadores/auth')
    api.add_resource(CoordinatorResource, '/coordinadores/<string:id_coordinator>')


def _add_tutor_related_reources(api):
    api.add_resource(TutorsResource, '/tutores')
    api.add_resource(TutorScheduleResource, '/tutores/<string:id_tutor>/disponibilidad')
    api.add_resource(TutorScheduleReplicateResource, '/tutores/<string:id_tutor>/disponibilidad/replicar')
    api.add_resource(TutorResource, '/tutores/<string:id_tutor>')


def _add_support_unit_related_resources(api):
    api.add_resource(SupportUnitResource, '/unidades-apoyo')


def _add_university_related_resources(api):
    api.add_resource(UniversityResource, '/universidad')
    api.add_resource(UniversityEmailResource, '/universidad/correos')


def _register_tutorias_resources(api):
    logger.info("Registrando los recursos de la api")
    _add_student_related_reources(api)
    _add_tutorship_related_reources(api)
    _add_session_related_reources(api)
    _add_unit_related_reources(api)
    _add_user_related_reources(api)
    _add_coordinator_related_reources(api)
    _add_tutor_related_reources(api)
    _add_support_unit_related_resources(api)
    _add_university_related_resources(api)
    _add_administrator_related_resources(api)
    _add_reports_related_resources(api)


def generate_tutorias_backend_blueprint():
    logger.info("Generando la api para el servicio de tutorias")
    api_bp = Blueprint('api', __name__)
    api = Api(api_bp)
    _register_tutorias_resources(api)
    logger.info("Api generada correctamente")
    return api_bp
