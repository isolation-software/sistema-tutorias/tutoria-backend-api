from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.core.repository.s3 import S3FileUploader
from src.helpers import info_success_logic, constants


class RegisterAdditionalInfoUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_additional_info=None):
        super().__init__(errors, req)
        # repos
        self.repo_additional_info = repo_additional_info or self.RepoFactory.AdditionalInfoRepo()
        # s3 connection
        self.s3 = S3FileUploader()
        # request data
        self.additional_info = req.additional_info
        self.file = req.file
        self.file_extension = req.file_extension
        self.content_type = constants.APPLICATION + req.file_extension

    @catch_errors
    def execute(self):

        self.additional_info.archive = self.s3.upload(file=self.file, extension=self.file_extension,
                                                      content_type=self.content_type)
        additional_info_saved = self.repo_additional_info.register_additional_info(self.additional_info)
        self.res = Response(additional_info=additional_info_saved)
        self.success = RegisterAdditionalInfoSuccess


class RegisterAdditionalInfoSuccess(Success):
    code = info_success_logic.ADDITIONAL_INFO_SAVED_OK_CODE
    message = info_success_logic.ADDITIONAL_INFO_SAVED_OK_MESSAGE
