import datetime
import jwt

from src.config import LoggerFactory, FlaskConfig
from src.core.domain.entities import User
from src.core.domain.errors.handler import catch_errors
from src.core.domain.errors.logic_errors import UserNotFoundInSystemError
from src.core.domain.use_cases.common import UseCase, Success, Response
from src.core.repository.email import EmailSender
from src.helpers import info_success_logic


class SendEmailRegisterUserUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_user=None):
        super().__init__(errors, req)
        self.user = None
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        self.email = req.email

    @catch_errors
    def execute(self):
        user = self.repo_user.read_user_by_email_if_exists(User(email=self.email))
        if not user:
            raise UserNotFoundInSystemError(email=self.email)
        is_activated = self.repo_user.is_activated_user(self.email)
        if not is_activated:
            names = user.fullname()
            token_expired_time = datetime.datetime.utcnow() + datetime.timedelta(minutes=60)
            token = jwt.encode({'usuario': names, 'correo': user.email, 'exp': token_expired_time,
                                'expFormat': token_expired_time.isoformat()}, FlaskConfig.SECRET_KEY)
            token_utf_8 = token.decode('utf-8')
            EmailSender.send_welcome(user, token_utf_8)
        else:
            pass
        self.res = Response(user=user)
        self.success = SendEmailRegisterUserSuccess


class SendEmailRegisterUserSuccess(Success):
    code = info_success_logic.SEND_EMAIL_REGITER_USER_OK_CODE
    message = info_success_logic.SEND_EMAIL_REGITER_USER_OK_MESSAGE
