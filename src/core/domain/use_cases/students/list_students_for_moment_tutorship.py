from src.config import LoggerFactory
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import constants, info_success_logic


class ListStudentsForMomentTutorshipUseCase(UseCase):
    """Class that list all students whose units are the same
    in which the tutor gives tutorships, filtering by student
    name, code or email
    """

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_student=None, repo_tutor=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        self.repo_unit = repo_tutor or self.RepoFactory.UnitRepo(self.repo_student.session)
        # request data
        self.tutor = req.tutor
        self.search = req.search or constants.EMPTY_STRING
        self.limit  = req.limit  or constants.DEFAULT_LIMIT

    @catch_errors
    def execute(self):
        self.logger.debug("UseCase - Iniciando con la búsqueda de unidades a las que pertenece el tutor")
        units = self.repo_unit.list_units_per_tutor(self.tutor)
        self.logger.debug(f"UseCase - Cantidad de unidades encontradas: {len(units)}")
        students = self.repo_student.list_students_of_units(units, self.search)
        self.logger.debug(f"UseCase - Cantidad de alumnos encontrados: {len(students)}")
        self.res = Response(students=students)
        self.success = GetStudentsForMomentTutorshipSuccess


class GetStudentsForMomentTutorshipSuccess(Success):
    code = info_success_logic.STUDENTS_FOR_MOMENT_TUTORSHIP_SEARCH_OK_CODE
    message = info_success_logic.STUDENTS_FOR_MOMENT_TUTORSHIP_SEARCH_OK_MESSAGE
