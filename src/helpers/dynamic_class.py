
class DynamicClass:
    """ Auxiliary class that doen't need to define attributes statically.
        It's used to generate dinamically attributes from parameters of kwargs.
        Example:
            class PackageClass(DynamicClass):
                pass

            pack_1 = PackageClass(name="Packaging", description="No description")
            pack_2 = PackageClass(direction="Av. Universitaria 900")

            print(pack_1.name)          # Packaging
            print(pack_2.name)          # Error
            print(pack_1.direction)     # Error
            print(pack_2.direction)     # Av. Universitaria 900

    """
    def __init__(self, **kwargs):
        self.attributes = kwargs.keys()
        for attribute in self.attributes:
            self.__setattr__(attribute, kwargs[attribute])

    def __repr__(self):
        return f"<{type(self).__name__} {[(attr, self.__getattribute__(attr)) for attr in self.attributes]}>"

    def to_dict(self):
        return {k: self.__getattribute__(k) for k in self.attributes}
