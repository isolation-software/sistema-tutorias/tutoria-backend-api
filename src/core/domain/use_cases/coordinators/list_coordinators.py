from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListCoordinatorsUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_coordinator=None):
        super().__init__(errors, req)
        self.repo_coordinator = repo_coordinator or self.RepoFactory.CoordinatorRepo()
        self.search_params = req.search, req.page, req.pagesize

    @catch_errors
    def execute(self):
        coordinators = self.repo_coordinator.list_coordinators(self.search_params)
        self.res = Response(coordinators=coordinators)
        self.success = ListCoordinatorsSuccess


class ListCoordinatorsSuccess(Success):
    code = info_success_logic.SEARCH_COORDINATORS_OK_CODE
    message = info_success_logic.SEARCH_COORDINATORS_OK_MESSAGE
