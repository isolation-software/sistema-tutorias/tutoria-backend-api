from src.controllers.common import Controller
from src.core.domain.entities import Tutor, Student, AdditionalInformation
from src.core.domain.use_cases.common import Request, Response
from src.core.domain.use_cases.students import RegisterStudentUseCase, ListStudentsUseCase, \
    ListStudentsForMomentTutorshipUseCase, RequestDataStudentUseCase,\
    GetAdditionalInfoUseCase, RegisterAdditionalInfoUseCase, DeleteAdditionalInfoUseCase
from src.core.domain.use_cases.students.register_bulk_students import RegisterBulkStudentUseCase
from src.deserializers.deserializer_student import StudentDeserializer
from src.helpers import constants
from src.helpers.serializer import str_to_id
from src.serializers.serializer_student import StudentSerializer


class StudentController(Controller):
    student_deserializer = StudentDeserializer
    student_serializer = StudentSerializer
    register_uc = RegisterStudentUseCase
    register_bulk_student_uc = RegisterBulkStudentUseCase

    @classmethod
    def register_student(cls, json, bulk_param):
        if not bulk_param:
            student = cls.student_deserializer.student(json)
            uc_student = cls.register_uc(req=Request(student=student, bulk_param=bulk_param))
            uc_student.execute()
            # TODO(JGT-2): improve, better messages and codes to map in some file .properties
            if not uc_student.has_errors():
                return Response(status=constants.OK,
                                code=constants.CODE_REGISTER_OK,
                                message=constants.REGISTER_STUDENT_OK_MESSAGE)
            else:
                error = uc_student.get_last_error()
                return Response(status=constants.ERROR,
                                code=error.code,
                                message=error.message)
        else:
            unit, students = cls.student_deserializer.students(json)
            req = Request(students=students,unit=unit)
            return cls.build_response(
                use_case=cls.register_bulk_student_uc,
                req=req,
                serialize_response=cls.student_serializer.serialize_register_bulk_student_response
            )


    @staticmethod
    def list_students(search, limit):
        # TODO(JGT-3): improve filters by unit when listing students
        uc_list_students = ListStudentsUseCase(req=Request(search=search, limit=limit))
        uc_list_students.execute()

        if not uc_list_students.has_errors():
            students = uc_list_students.res.students
            # serialize response from use-case
            serialized = StudentSerializer.student_list(students)
            return Response(status=constants.OK,
                            code=constants.CODE_SEARCH_OK,
                            message=constants.SEARCH_STUDENTS_OK_MESSAGE,
                            data=serialized)
        else:
            return Response(status=constants.ERROR,
                            code=constants.CODE_SEARCH_ERROR,
                            message=constants.SEARCH_STUDENTS_ERROR_MESSAGE,
                            data={})

    list_students_for_moment_tutorship_uc = ListStudentsForMomentTutorshipUseCase

    @classmethod
    def list_students_for_moment_tutorship(cls, id_tutor, search, limit):
        cls.logger.debug("Controller - iniciando el listar alumnos para una tutoría del momento")
        tutor = Tutor(idx=str_to_id(id_tutor))
        req = Request(tutor=tutor, search=search, limit=limit)
        return cls.build_response(
            use_case=cls.list_students_for_moment_tutorship_uc,
            req=req,
            serialize_response=cls.student_serializer.list_students_for_moment_tutorship)

    request_data_student_uc = RequestDataStudentUseCase

    @classmethod
    def get_student(cls, id_student):
        id_student = str_to_id(id_student)
        req = Request(id_student=id_student)

        return cls.build_response(
            use_case=cls.request_data_student_uc,
            req=req,
            serialize_response=cls.student_serializer.serialize_student_id
        )

    get_additional_info_uc = GetAdditionalInfoUseCase

    @classmethod
    def get_additional_info(cls, id_student):
        return cls.build_response(
            use_case=cls.get_additional_info_uc,
            req=Request(student=Student(idx=str_to_id(id_student))),
            serialize_response=cls.student_serializer.additional_info
        )

    register_additional_info_uc = RegisterAdditionalInfoUseCase

    @classmethod
    def register_additional_info(cls, id_student, file, json):
        additional_info, file_extension = cls.student_deserializer.info_file(json, id_student)
        return cls.build_response(
            use_case=cls.register_additional_info_uc,
            req=Request(file=file, additional_info=additional_info, file_extension=file_extension),
            serialize_response=cls.student_serializer.info_file
        )

    delete_additional_info_uc = DeleteAdditionalInfoUseCase

    @classmethod
    def delete_additional_info(cls, id_additional_info):
        return cls.build_response(
            use_case=cls.delete_additional_info_uc,
            req=Request(additional_info=AdditionalInformation(idx=str_to_id(id_additional_info))))
