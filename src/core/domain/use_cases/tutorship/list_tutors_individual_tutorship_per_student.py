from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListTutorsIndividualTutorshipPerStudentUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.id_tutorship = req.id_tutorship
        self.id_student = req.id_student

    @catch_errors
    def execute(self):
        tutorship = self.repo_tutorship.get_tutorship(id_tutorship=self.id_tutorship)
        tutors = self.repo_tutorship.list_tutors_individual_tutorship_per_student(tutorship, self.id_student)
        self.res = Response(tutors=tutors)
        self.success = ListTutorsIndividualTutorshipPerStudentSuccess()


class ListTutorsIndividualTutorshipPerStudentSuccess(Success):
    code = info_success_logic.LIST_TUTORS_INDIVIDUAL_TUTORSHIP_PER_STUDENT_OK_CODE
    message = info_success_logic.LIST_TUTORS_INDIVIDUAL_TUTORSHIP_PER_STUDENT_OK_MESSAGE