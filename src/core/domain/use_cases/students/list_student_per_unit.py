from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListStudentPerUnitUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        # request data
        self.id_unit = req.id_unit
        self.search = req.search
        self.limit = req.limit

    @catch_errors
    def execute(self):
        students = self.repo_student.read_students_from_unit_and_unit_children(self.id_unit, self.search, self.limit)
        # last_saved_students = self.repo_student.read_students_per_unit(self.id_unit, self.search, self.limit)

        self.res = Response(students=students)
        self.success = ListStudentPerUnitSuccess


class ListStudentPerUnitSuccess(Success):
    code = info_success_logic.LIST_STUDENT_PER_UNIT_OK_CODE
    message = info_success_logic.LIST_STUDENT_PER_UNIT_OK_MESSAGE
