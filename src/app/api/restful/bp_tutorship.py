from flask_restful import Resource
from flask import request

from src.app.auth.handler import jwt_required
from src.controllers import TutorshipController, StudentController
from src.controllers.session.controller_session import SessionController
from src.helpers import constants


class TutorshipsMomentResource(Resource):
    @jwt_required
    def post(self):
        data = request.get_json()
        response = TutorshipController.register_tutorship_moment(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def get(self):
        id_student = request.args.get('idAlumno', None)
        response = TutorshipController.list_moment_tutorship_sessions_for_student(id_student)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class StudentsTutorshipsMomentResource(Resource):
    @jwt_required
    def get(self, id_tutor):
        search = request.args.get('search')
        limit  = request.args.get('limit', default=constants.DEFAULT_LIMIT, type=int)
        response = StudentController.list_students_for_moment_tutorship(id_tutor, search, limit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class TutorshipsResource(Resource):
    @jwt_required
    def get(self):
        id_student = request.args.get('idAlumno', None)
        search = request.args.get('search', None)
        response = TutorshipController.list_tutorship_for_student(id_student, search)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class TutorshipResource(Resource):
    @jwt_required
    def get(self, id_tutorship):
        response = TutorshipController.get_tutorship(id_tutorship)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def put(self, id_tutorship):
        data = request.get_json()
        response = TutorshipController.put_tutorship(id_tutorship=id_tutorship, data=data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class IndividualTutorshipResource(Resource):
    @jwt_required
    def post(self):
        data = request.get_json()
        response = TutorshipController.register_individual_tutorship(data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class SessionIndividualTutorshipResource(Resource):
    @jwt_required
    def put(self, id_tutorship, id_student, id_session):
        data = request.get_json()
        response = SessionController.save_individual_session(id_tutorship, id_student, id_session, data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST
        
    @jwt_required
    def post(self):
        json = request.get_json()
        response = SessionController.register_session(json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class AllTutorshipsResource(Resource):
    @jwt_required
    def get(self):
        id_unit = request.args.get('idUnidad')
        search  = request.args.get('search', constants.EMPTY_STRING)
        response = TutorshipController.list_tutorship_for_unit(data_id_unit=id_unit, search=search)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class IndividualTutorshipStudentsResource(Resource):
    @jwt_required
    def post(self, id_tutorship):
        data = request.get_json()
        response = TutorshipController.register_students_individual_tutorship(id_tutorship, data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def get(self,id_tutorship):
        search = request.args.get('search', "")
        limit = request.args.get('limit', 10)
        which = request.args.get('which', "all")
        response = TutorshipController.list_students_individual_tutorship(id_tutorship, search, limit, which)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class IndividualTutorshipStudentResource(Resource):
    @jwt_required
    def delete(self, id_tutorship, id_student):
        response = TutorshipController.delete_student_individual_tutorship(id_tutorship, id_student)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class IndividualTutorshipTutorsResource(Resource):
    @jwt_required
    def post(self, id_tutorship):
        data = request.get_json()
        response = TutorshipController.register_tutors_individual_tutorship(id_tutorship, data)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def get(self, id_tutorship):
        search = request.args.get('search',"")
        limit = request.args.get('limit',10)
        which = request.args.get('which',"all")
        response = TutorshipController.list_tutors_individual_tutorship(id_tutorship, search, limit, which)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class IndividualTutorshipTutorResource(Resource):
    @jwt_required
    def delete(self, id_tutorship, id_tutor):
        response = TutorshipController.delete_tutor_individual_tutorship(id_tutorship, id_tutor)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class IndividualTutorshipAvailabilityResource(Resource):
    @jwt_required
    def get(self, id_tutorship, id_student):
        is_coordinator = request.args.get('esCoord', "true", type=str)
        response = TutorshipController.list_tutors_individual_tutorship_per_student(id_tutorship, id_student, is_coordinator)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class AppointmentStudentResource(Resource):

    @jwt_required
    def get(self, id_student):
        search = request.args.get('search',"")
        limit = request.args.get('limit', 10)
        response = TutorshipController.list_appointments(id_student, search, limit)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST




