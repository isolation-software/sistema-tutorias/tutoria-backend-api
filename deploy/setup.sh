#!/usr/bin/env bash

# ARCHIVO PARA SETUP DE HERRAMIENTAS A USAR EN EL PROYECTO
# - git
# - python 3.8
# - pip3, pip para python 3
# - pipenv
# - nginx

############################### CONFIRMACION DE PIP3
if command -v git &> /dev/null
    then
        echo "git encontrado, continua el despliegue"
    else
        echo "git no encontrado, deteniendo el despliegue"
        exit
fi

############################### CONFIRMACION DE PYTHON3
if [[ $(python --version 2>&1) =~ 3\.8 ]] || [[ $(python3 --version 2>&1) =~ 3\.8 ]]
    then
        echo "python 3.8 encontrado, continua el despliegue"
    else
        echo "python 3.8 no encontrado, deteniendo el despliegue"
        exit
fi

############################### CONFIRMACION DE PIP3
if ! command -v pip3 &> /dev/null
    then
        echo "pip3 no encontrado, se inicia su instalacion"
        sudo apt install python3-pip
        echo "pip3 instalado, continua el despliegue"
    else
        echo "pip3 encontrado, continua el despliegue"
fi

############################### PREPARACION DE AMBIENTE VIRTUAL DE PYTHON
if ! command -v pipenv &> /dev/null
    then
        echo "pipenv no encontrado, se inicia su instalacion"
        pip3 install pipenv
        echo "pipenv instalado, continua el despliegue"
    else
        echo "pipenv encontrado, continua el despliegue"
fi

