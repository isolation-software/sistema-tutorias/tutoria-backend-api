from datetime import datetime as dt
from datetime import timedelta

from dateutil.relativedelta import relativedelta
from sqlalchemy import and_

import src.helpers.constants as cons

from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import AvailabilityDateModel, TutorModel, TutorPerUnitModel, SessionModel, \
    StudentModel, SessionSetModel, SetPerStudentModel
from src.core.repository.errors.handler import catch_repo_errors
from src.helpers import constants


class AvailabilityDateRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)
    # date.AddDays( -(int)availability_dates[cons.LAST].time.date().DayOfWeek - 6 )

    @catch_repo_errors
    def register_availability_time(self, availability_dates, tutor, unit):
        self.logger.debug("Repo - Iniciando actualización de disponibilidad a un tutor")

        rows_id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit)\
            .filter(TutorPerUnitModel.id_tutor == tutor.idx).all()

        id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit)\
            .filter(and_(
                TutorPerUnitModel.id_tutor == tutor.idx,
                TutorPerUnitModel.id_unit == unit.idx
            )).one_or_none()

        first_date = availability_dates[cons.FIRST].time.date()
        last_date = availability_dates[cons.LAST].time.date()
        first = first_date - timedelta(days=first_date.weekday())
        last = last_date - timedelta(days=last_date.weekday()) + timedelta(days=7)
        self.session.query(AvailabilityDateModel).filter(and_(
            AvailabilityDateModel.id_tutor_per_unit == id_tutor_per_unit,
            AvailabilityDateModel.state == True,
            AvailabilityDateModel.time >= dt.combine(first, dt.min.time()),
            AvailabilityDateModel.time <= dt.combine(last, dt.min.time())
        )).delete()

        booked_hours = self.session.query(AvailabilityDateModel)\
            .filter(and_(
                AvailabilityDateModel.id_session != None,
                AvailabilityDateModel.id_tutor_per_unit.in_(rows_id_tutor_per_unit)
            ))

        booked_hours = [hour.time.replace(microsecond=0) for hour in booked_hours]
        aux_availability = []

        for date in availability_dates:
            if date.time.replace(microsecond=0) not in booked_hours:
                aux_availability.append(date)

        availability_dates = aux_availability

        if availability_dates:
            availability_dates_rows = [AvailabilityDateModel(
                time=date.time,
                state=date.state,
                id_tutor_per_unit=id_tutor_per_unit,
                approved=date.approved
            ) for date in availability_dates]
            self.session.bulk_save_objects(availability_dates_rows)
        self.session.commit()
        self.logger.debug("Repo - Terminando actualización de disponibilidad a un tutor")

    @catch_repo_errors
    def save_availability_time(self, availability_dates, tutor, unit):
        self.logger.debug("Repo - Iniciando registro de disponibilidad a un tutor")

        rows_id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit) \
            .filter(TutorPerUnitModel.id_tutor == tutor.idx).all()

        id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit) \
            .filter(and_(
                TutorPerUnitModel.id_tutor == tutor.idx,
                TutorPerUnitModel.id_unit == unit.idx
            )).one_or_none()

        rows_availability_date = self.session.query(AvailabilityDateModel) \
            .filter(
            and_(
                AvailabilityDateModel.id_tutor_per_unit.in_(rows_id_tutor_per_unit),
                AvailabilityDateModel.time >= dt.combine(availability_dates[cons.FIRST].time.date(), dt.min.time()),
                AvailabilityDateModel.time <= dt.combine(availability_dates[cons.LAST].time.date(), dt.min.time())
                + relativedelta(days=+1)
            )
        ).all()
        self.logger.debug(f"Repo - Disponibilidad ya registrada durante el periodo: {len(rows_availability_date)}")
        self.logger.debug(f"Repo - Intención de registrar {len(availability_dates)} horas")

        assgined_dates = [row.time.replace(microsecond=0) for row in rows_availability_date]
        self.logger.debug(f"Repo - {availability_dates[0].approved}")

        aux_availability_dates = availability_dates[:]
        availability_dates = []

        for date in aux_availability_dates:
            if date.time.replace(microsecond=0) not in assgined_dates:
                availability_dates.append(date)

        self.logger.debug(f"Repo - Se va a registrar {len(availability_dates)} horas")

        availability_dates_rows = [AvailabilityDateModel(
            time=date.time,
            state=date.state,
            id_tutor_per_unit=id_tutor_per_unit,
            approved=date.approved
        ) for date in availability_dates]
        self.session.bulk_save_objects(availability_dates_rows)
        self.session.commit()
        self.logger.debug("Repo - Terminando registro de disponibilidad a un tutor")

    @catch_repo_errors
    def list_availability_time_for_tutor(self, tutor, start_date, end_date):

        rows_id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit) \
            .filter(TutorPerUnitModel.id_tutor == tutor.idx).all()

        rows_availability_date = self.session.query(AvailabilityDateModel) \
            .filter(and_(
                AvailabilityDateModel.id_tutor_per_unit.in_(rows_id_tutor_per_unit),
                AvailabilityDateModel.time >= start_date,
                AvailabilityDateModel.time <= end_date
            )).all()

        return [sr.to_entity() for sr in rows_availability_date]

    @catch_repo_errors
    def list_availability_time_for_tutor_only_approved(self, tutor, start_date, end_date):

        rows_id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit) \
            .filter(TutorPerUnitModel.id_tutor == tutor.idx).all()

        rows_availability_date = self.session.query(AvailabilityDateModel) \
            .filter(
            and_(
                AvailabilityDateModel.id_tutor_per_unit.in_(rows_id_tutor_per_unit),
                AvailabilityDateModel.approved == True,
                AvailabilityDateModel.time >= start_date,
                AvailabilityDateModel.time <= end_date
            )
        ).all()

        return [sr.to_entity() for sr in rows_availability_date]

    @catch_repo_errors
    def get_student_from_session(self, session):

        row_student = self.session.query(StudentModel) \
            .join(SessionModel, SessionModel.id_session == session.idx)\
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionModel.id_session_set)\
            .first()

        return row_student.to_entity()

    @catch_repo_errors
    def book_session_on_schedule(self, session, unit):
        booked_period = session.start_time
        final_period = session.end_time or session.start_time
        schedule = []
        tutor_id = session.tutor.idx if session.tutor and session.tutor.idx else\
            self.session.query(TutorModel.id_tutor)\
                .join(TutorPerUnitModel, TutorPerUnitModel.id_tutor == TutorModel.id_tutor)\
                .join(SessionModel, SessionModel.id_tutor_per_unit == TutorPerUnitModel.id_tutor_per_unit)\
                .filter(SessionModel.id_session == session.idx)

        id_tutor_per_unit = self.session.query(TutorPerUnitModel.id_tutor_per_unit) \
            .filter(and_(
                TutorPerUnitModel.id_tutor == tutor_id,
                TutorPerUnitModel.id_unit == unit.idx
            )).one_or_none()

        while booked_period < final_period:
            schedule.append(booked_period)
            booked_period += constants.NEXT_PERIOD

        row_schedule = self.session.query(AvailabilityDateModel) \
            .filter(and_(
                AvailabilityDateModel.id_tutor_per_unit == id_tutor_per_unit,
                AvailabilityDateModel.time.in_(schedule)
            )).all()

        for period in row_schedule:
            period.id_session = session.idx
            period.state = False

        self.session.commit()

    @catch_repo_errors
    def delete_session_on_schedule(self, session):

        row_schedule = self.session.query(AvailabilityDateModel) \
            .filter(AvailabilityDateModel.id_session == session.idx).all()

        for period in row_schedule:
            period.id_session = None
            period.state = True

        self.session.commit()
