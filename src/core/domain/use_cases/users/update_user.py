from src.config import LoggerFactory
from src.core.domain.entities.enums import Roles
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class UpdateUserUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_user=None, repo_student=None, repo_tutor=None):
        super().__init__(errors, req)
        # repos
        self.repo_user = repo_user or self.RepoFactory.UserRepo()
        self.repo_student = repo_student or self.RepoFactory.StudentRepo(session=self.repo_user.session)
        self.repo_tutor = repo_tutor or self.RepoFactory.TutorRepo(session=self.repo_user.session)
        # request data
        self.user = req.user
        self.unit = req.unit
        self.role = req.role
        self.idx = req.idx
        self.has_access_to_additional_information = req.has_access_to_additional_information

    @catch_errors
    def execute(self):
        self.logger.debug(f"Use Case - iniciando la actualización del {self.role.value.lower()} con id {self.idx}")
        user_updated = self.repo_user.update_user(self.user, self.role, self.idx)
        student_updated = self.repo_student.update_student_unit(self.idx, self.unit) \
            if self.role is Roles.STUDENT else None
        tutor_updated = self.repo_tutor.update_tutor_permission(self.idx, self.has_access_to_additional_information) \
            if self.role is Roles.TUTOR else None
        self.res = Response(user=user_updated, student=student_updated, tutor=tutor_updated)
        self.success = UpdateUserSuccess
        self.logger.debug(f"Use Case - terminando la actualización del {self.role.value.lower()} con id {self.idx}")


class UpdateUserSuccess(Success):
    code = info_success_logic.USER_UPDATE_OK_CODE
    message = info_success_logic.USER_UPDATE_OK_MESSAGE
