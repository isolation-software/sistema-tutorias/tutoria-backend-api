
class Tutorship:

    def __init__(self, idx=None, name=None, description=None, sessions_number=None, session_series=None,
                 is_individual=None, is_obligatory=None, is_tutor_permanent=None,is_tutor_assigned=None,
                 students=None, tutors=None, unit=None):
        self.idx = idx
        self.name = name
        self.description = description
        self.sessions_number = sessions_number
        self.session_series = session_series
        self.is_individual = is_individual
        self.is_obligatory = is_obligatory
        self.is_tutor_permanent = is_tutor_permanent
        self.is_tutor_assigned = is_tutor_assigned
        self.students = students
        self.tutors = tutors
        self.unit = unit
