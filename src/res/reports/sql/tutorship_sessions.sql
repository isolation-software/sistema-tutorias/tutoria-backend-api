SELECT DISTINCT MU.name AS UNIDAD, MT.name AS TUTORIA, COALESCE(B.ATENDIDAS_TUTORIA,0) AS ATENDIDAS,
                COALESCE(A.RESPONDIDAS_TUTORIA,0) AS RESPONDIDAS,
                COALESCE(C.PROMEDIO_SATISFACCION,0) AS PROM_SATISFACCION
FROM "MT_SESSION" MS
INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
INNER JOIN "MT_UNIT" MU on MT.id_unit = MU.id_unit
LEFT OUTER JOIN (
SELECT MT.name AS TUTORIA,MU.name AS UNIDAD,COUNT(*) AS RESPONDIDAS_TUTORIA
FROM "MT_SESSION" MS
INNER JOIN "MT_SESSION_SET" MSS on MS.id_session_set = MSS.id_session_set
INNER JOIN "MT_TUTORSHIP" MT on MSS.id_tutorship = MT.id_tutorship
INNER JOIN "MT_UNIT" MU on MT.id_unit = MU.id_unit
INNER JOIN "MT_INDICATOR" MI on MS.id_session = MI.id_session
WHERE (MU.id_unit = :id_unit OR MU.id_father_unit = :id_unit)
        AND MS.state = 'Atendido' AND UPPER(MT.name) NOT LIKE ('%MOMENTO%')
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
GROUP BY MU.name, MT.name) A ON MT.name = A.TUTORIA AND MU.name = A.UNIDAD
LEFT OUTER JOIN (
SELECT MU.name AS UNIDAD, MT.name AS TUTORIA, count(*) AS ATENDIDAS_TUTORIA
FROM "MT_TUTORSHIP" MT
INNER JOIN "MT_UNIT" MU on MT.id_unit = MU.id_unit
LEFT JOIN "MT_SESSION_SET" MSS on MT.id_tutorship = MSS.id_tutorship
LEFT JOIN "MT_SESSION" MS on MSS.id_session_set = MS.id_session_set
WHERE (MU.id_unit = :id_unit
OR MU.id_father_unit = :id_unit)
AND MS.state = 'Atendido'
AND UPPER(MT.name) NOT LIKE ('%MOMENTO%')
AND MS.start_time>=:start_time AND MS.start_time<=:end_time
GROUP BY MU.name, MT.name) B ON MU.name = B.UNIDAD AND MT.name = B.TUTORIA
LEFT OUTER JOIN (
SELECT A.UNIDAD, A.TUTORIA, A.PROMEDIO_INDICADOR1 AS PROMEDIO_SATISFACCION
FROM
    (SELECT MU.name AS UNIDAD, MT.name AS TUTORIA, AVG(M.indicator_1) AS PROMEDIO_INDICADOR1
    FROM "MT_TUTORSHIP" MT
    INNER JOIN "MT_UNIT" MU on MT.id_unit = MU.id_unit
    INNER JOIN "MT_SESSION_SET" MSS on MT.id_tutorship = MSS.id_tutorship
    INNER JOIN "MT_SESSION" MS on MSS.id_session_set = MS.id_session_set
    INNER JOIN "MT_INDICATOR" M on MS.id_session = M.id_session
    WHERE (MU.id_unit = :id_unit
    OR MU.id_father_unit = :id_unit)
    AND MS.state = 'Atendido'
    AND UPPER(MT.name) NOT LIKE ('%MOMENTO%')
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time
    GROUP BY MU.name , MT.name) A
    )C ON MU.name = C.UNIDAD AND MT.name = C.TUTORIA
WHERE (MU.id_unit = :id_unit OR MU.id_father_unit = :id_unit)
        AND MS.state = 'Atendido' AND UPPER(MT.name) NOT LIKE ('%MOMENTO%')
    AND MS.start_time>=:start_time AND MS.start_time<=:end_time;
