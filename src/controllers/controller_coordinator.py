from src.core.domain.use_cases.common import Request, Response
from src.core.domain.use_cases.coordinators import ListCoordinatorsUseCase, RegisterCoordinatorUseCase
from src.core.domain.use_cases.coordinators.request_data_coordinator import RequestDataCoordinatorUseCase
from src.deserializers import CoordinatorDeserializer
from src.helpers import constants
from src.helpers.serializer import str_to_id
from src.serializers import CoordinatorSerializer
from src.controllers.common import Controller


class CoordinatorController(Controller):
    coordinator_serializer = CoordinatorSerializer
    coordinator_deserializer = CoordinatorDeserializer
    list_uc = ListCoordinatorsUseCase

    @classmethod
    def list_coordinators(cls, search, page, pagesize):
        uc_list_coordinators = cls.list_uc(req=Request(search=search, page=page, pagesize=pagesize))
        uc_list_coordinators.execute()

        if not uc_list_coordinators.has_errors():
            success = uc_list_coordinators.get_success()
            coordinators = uc_list_coordinators.res.coordinators
            data = cls.coordinator_serializer.serialize_coordinators(coordinators)
            return Response(status=constants.OK,
                            code=success.code,
                            message=success.message,
                            data=data)
        else:
            error = uc_list_coordinators.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message)

    register_uc = RegisterCoordinatorUseCase

    @classmethod
    def register_coordinator(cls, json):
        coordinator = cls.coordinator_deserializer.coordinator(json)
        uc_coordinator = cls.register_uc(req=Request(coordinator=coordinator))
        uc_coordinator.execute()

        if not uc_coordinator.has_errors():
            return Response(status=constants.OK,
                            code=constants.CODE_REGISTER_OK,
                            message=constants.REGISTER_COORDINATOR_OK_MESSAGE)
        else:
            error = uc_coordinator.get_last_error()
            return Response(status=constants.ERROR,
                            code=error.code,
                            message=error.message)

    request_data_coordinator_uc = RequestDataCoordinatorUseCase

    @classmethod
    def get_coordinator(cls, id_coordinator):
        id_coordinator = str_to_id(id_coordinator)
        req = Request(id_coordinator=id_coordinator)

        return cls.build_response(
            use_case=cls.request_data_coordinator_uc,
            req=req,
            serialize_response=cls.coordinator_serializer.serialize_coordinator_id
        )
