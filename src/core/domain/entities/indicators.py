class Indicators:

    def __init__(self, idx=None, indicator1=None, indicator2=None, indicator3=None, indicator4=None, indicator5=None,
                 indicator6=None, session=None):
        self.idx = idx
        self.indicator1 = indicator1
        self.indicator2 = indicator2
        self.indicator3 = indicator3
        self.indicator4 = indicator4
        self.indicator5 = indicator5
        self.indicator6 = indicator6
        self.session = session