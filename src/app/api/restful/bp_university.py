import base64

from flask_restful import Resource
from flask import request

from src.app.auth.handler import jwt_required
from src.controllers.university import UniversityController
from src.helpers import constants


class UniversityResource(Resource):
    @jwt_required
    def get(self):
        response = UniversityController.get_university()
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def post(self):
        json = request.get_json()
        logo = base64.b64decode(json.get("logo")) if json.get("logo") else None
        response = UniversityController.register_university(logo, json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def put(self):
        json = request.get_json()
        logo = base64.b64decode(json.get("logo")) if json.get("logo") else None
        response = UniversityController.update_university(logo, json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST


class UniversityEmailResource(Resource):
    @jwt_required
    def post(self):
        json = request.get_json()
        response = UniversityController.register_valid_email_patterns(json)
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

    @jwt_required
    def get(self):
        response = UniversityController.get_valid_email_patterns()
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST

