from src.core.domain.use_cases.common import UseCase


class ExistUserUseCase(UseCase):

    def __init__(self, errors=None, repo_user=None):
        super().__init__(errors)
        self.repo_user = repo_user

    def send_request(self, req):
        pass

    def get_response(self):
        pass

    def execute(self):
        pass
