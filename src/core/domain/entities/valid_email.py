from src.helpers import constants


class ValidEmail:
    def __init__(self, idx=None, university=None, prefix=None, domain=None, sponsored_tld=None, top_level_domain=None):
        self.idx = idx,
        self.university = university,
        self.prefix = prefix,
        self.domain = domain,
        self.sponsored_tld = sponsored_tld,
        self.top_level_domain = top_level_domain

    def pattern(self, default_str_format=constants.EMAIL_WITH_STLD_PATTERN_FORMAT,
                alt_str_format=constants.EMAIL_WITHOUT_STLD_PATTERN_FORMAT):
        return default_str_format.format(
            prefix=self.prefix if self.prefix else constants.EMPTY_STRING,
            domain=self.domain,
            sponsored_tld=self.sponsored_tld,
            tld=self.top_level_domain
        ) if self.sponsored_tld else \
            alt_str_format.format(
                prefix=self.prefix if self.prefix else constants.EMPTY_STRING,
                domain=self.domain,
                tld=self.top_level_domain
            )
