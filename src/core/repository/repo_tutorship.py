from sqlalchemy import and_, or_

from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import TutorshipModel, SessionSetModel, SetPerStudentModel, \
    StudentModel, TutorPerTutorshipModel, TutorPerUnitModel, TutorModel, UserModel, UnitModel, SessionModel
from src.core.repository.errors.handler import catch_repo_errors
from src.core.repository.errors.sqla_errors import TutorshipNotFoundError, NoTutorshipMomentForUnitError
from src.helpers import constants


class TutorshipRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def read_moment_tutorship(self, unit):
        search = "%{}%".format(constants.TUTORSHIP_MOMENT_NAME)
        tutorship = self.session.query(TutorshipModel) \
            .filter(and_(TutorshipModel.name.ilike(search),
                         TutorshipModel.id_unit == unit.idx)) \
            .first()
        if not tutorship:
            raise NoTutorshipMomentForUnitError()
        return tutorship.to_entity()

    @catch_repo_errors
    def update_tutorship(self, tutorship):
        row = self.session.query(TutorshipModel).filter(TutorshipModel.id_tutorship == tutorship.idx).one_or_none()
        if row:
            row.name = tutorship.name
            row.description = tutorship.description
            row.sessions_number = tutorship.sessions_number
            row.is_individual = tutorship.is_individual
            row.is_tutor_permanent = tutorship.is_tutor_permanent
            row.is_obligatory = tutorship.is_obligatory
            row.is_tutor_assigned = tutorship.is_tutor_assigned
        self.session.commit()
        return row.to_entity()

    @catch_repo_errors
    def get_names_by_session_ids(self, sessions):
        session_set_ids = [session.sessions_serie.idx for session in sessions]
        tutorship_rows = self.session \
            .query(TutorshipModel) \
            .join(SessionSetModel, SessionSetModel.id_tutorship == TutorshipModel.id_tutorship) \
            .filter(SessionSetModel.id_session_set.in_(session_set_ids)) \
            .order_by(SessionSetModel.id_session_set.asc()) \
            .all()

        return [sr.to_entity() for sr in tutorship_rows]

    @catch_repo_errors
    def read_tutorship_history(self, student, search):
        search = f"%{search}%"
        self.logger.debug(f"Repo - Iniciando búsqueda de historial para el alumno con id {student.idx}")
        row_tutorships = self.session \
            .query(TutorshipModel) \
            .join(SessionSetModel, SessionSetModel.id_tutorship == TutorshipModel.id_tutorship) \
            .join(SetPerStudentModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .join(StudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(UnitModel, UnitModel.id_unit == TutorshipModel.id_unit)\
            .filter(and_(
                StudentModel.id_student == student.idx,
                TutorshipModel.name.ilike(search)
            )).all()
        self.logger.debug(f"Número de tutorías: {len(row_tutorships)}")

        return [sr.to_entity() for sr in row_tutorships]

    @catch_repo_errors
    def read_tutorship_by_session_set_id(self, id_session_set):
        tutorship = self.session \
            .query(TutorshipModel) \
            .join(SessionSetModel, SessionSetModel.id_tutorship == TutorshipModel.id_tutorship) \
            .filter(SessionSetModel.id_session_set == id_session_set).first()

        return tutorship.to_entity()

    @catch_repo_errors
    def save_individual_tutorship(self, unit_id, tutorship):
        row_tutorship = TutorshipModel(name=tutorship.name,
                                       description=tutorship.description,
                                       sessions_number=tutorship.sessions_number,
                                       id_unit=unit_id,
                                       is_individual=tutorship.is_individual,
                                       is_tutor_permanent=tutorship.is_tutor_permanent,
                                       is_obligatory=tutorship.is_obligatory,
                                       is_tutor_assigned=tutorship.is_tutor_assigned
                                       )
        self.session.add(row_tutorship)
        self.session.commit()
        self.session.refresh(row_tutorship)
        tutorship.idx = row_tutorship.id_tutorship
        return tutorship

    @catch_repo_errors
    def save_tutors_for_tutorship(self, tutor_ids, tutorship, unit_id):
        tutor_per_unit_rows = self.session \
            .query(TutorPerUnitModel) \
            .filter(and_(
            TutorPerUnitModel.id_tutor.in_(tutor_ids),
            TutorPerUnitModel.id_unit == unit_id
        )).all()
        tutor_per_tutorship_rows = [TutorPerTutorshipModel(
            id_tutorship=tutorship.idx,
            id_tutor_per_unit=t_x_u_row.id_tutor_per_unit)
            for t_x_u_row in tutor_per_unit_rows]
        self.session.bulk_save_objects(tutor_per_tutorship_rows)
        self.session.commit()

    @catch_repo_errors
    def get_tutorship(self, id_tutorship):
        tutorship_row = self.session.query(TutorshipModel) \
            .filter(TutorshipModel.id_tutorship == id_tutorship) \
            .one_or_none()
        if tutorship_row is None:
            raise TutorshipNotFoundError()
        return tutorship_row.to_entity()

    @catch_repo_errors
    def get_tutorships_by_unit(self, unit, search=""):
        _search = "%{}%".format(search)
        tutorship_rows = self.session \
            .query(TutorshipModel) \
            .filter(and_(
                TutorshipModel.id_unit == unit.idx,
                TutorshipModel.name.ilike(_search)
            )).all()
        return [tutorship_row.to_entity() for tutorship_row in tutorship_rows]

    @catch_repo_errors
    def get_unit_by_tutorship(self, id_tutorship):
        unit_row = self.session.query(UnitModel) \
            .join(TutorshipModel, UnitModel.id_unit == TutorshipModel.id_unit) \
            .filter(TutorshipModel.id_tutorship == id_tutorship) \
            .one_or_none()
        if unit_row is None:
            raise TutorshipNotFoundError()
        else:
            return unit_row.to_entity()

    @catch_repo_errors
    def list_tutors_individual_tutorship(self, id_tutorship, id_unit, search, limit, filter):
        search = "%{}%".format(search)
        tutor_rows = self.session.query(TutorModel) \
            .join(UserModel, TutorModel.id_user == UserModel.id_user) \
            .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
            .join(TutorPerTutorshipModel,
                  TutorPerUnitModel.id_tutor_per_unit == TutorPerTutorshipModel.id_tutor_per_unit) \
            .filter(
            and_(
                or_(
                    UserModel.names.ilike(search),
                    UserModel.father_lastname.ilike(search),
                    UserModel.mother_lastname.ilike(search),
                    UserModel.code.ilike(search)
                ),
                TutorPerTutorshipModel.id_tutorship == id_tutorship
            ),
        ).all()
        if filter == "all":
            if tutor_rows:
                return [row.to_entity() for row in tutor_rows]
            else:
                return None
        else:
            ids_tutor = [row.id_tutor for row in tutor_rows]
            rows_notin_tutorship = self.session.query(TutorModel) \
                .join(UserModel, TutorModel.id_user == UserModel.id_user) \
                .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor).filter(
                and_(
                    or_(
                        UserModel.names.ilike(search),
                        UserModel.father_lastname.ilike(search),
                        UserModel.mother_lastname.ilike(search),
                        UserModel.code.ilike(search)
                    ),
                    TutorPerUnitModel.id_unit == id_unit,
                    TutorPerUnitModel.id_tutor.notin_(ids_tutor)
                )
            ).limit(limit).all()
            if rows_notin_tutorship:
                return [row.to_entity() for row in rows_notin_tutorship]
            else:
                return None

    @catch_repo_errors
    def list_tutors_individual_tutorship_per_student(self, tutorship, id_student):
        variable_tutor = not tutorship.is_tutor_permanent
        tutor_rows = self.session.query(TutorModel) \
            .join(UserModel, TutorModel.id_user == UserModel.id_user) \
            .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
            .join(TutorPerTutorshipModel,
                  TutorPerUnitModel.id_tutor_per_unit == TutorPerTutorshipModel.id_tutor_per_unit) \
            .filter(
            TutorPerTutorshipModel.id_tutorship == tutorship.idx).all()
        if variable_tutor:
            if tutor_rows:
                return [row.to_entity() for row in tutor_rows]
            else:
                return None
        else:
            row_last_meet = self.session.query(SessionModel) \
                .join(SessionSetModel, SessionModel.id_session_set == SessionSetModel.id_session_set) \
                .join(SetPerStudentModel, SessionSetModel.id_session_set == SetPerStudentModel.id_session_set) \
                .filter(
                and_(
                    SessionSetModel.id_tutorship == tutorship.idx,
                    SetPerStudentModel.id_student == id_student,
                    SessionModel.state == "Atendido"
                )
            ).order_by(SessionModel.start_time.desc()).first()
            if row_last_meet:
                id_tutor_per_unit = row_last_meet.id_tutor_per_unit
                fixed_tutor = self.session.query(TutorModel) \
                    .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor) \
                    .filter(
                    TutorPerUnitModel.id_tutor_per_unit == id_tutor_per_unit
                ).one_or_none()
                if fixed_tutor:
                    return [fixed_tutor.to_entity()]
                else:
                    return None
            else:
                if tutor_rows:
                    return [row.to_entity() for row in tutor_rows]
                else:
                    return None

    @catch_repo_errors
    def get_tutors_of_student(self, id_tutorship, id_student):
        rows_tutors = self.session.query(TutorModel)\
            .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor)\
            .join(SessionModel, TutorPerUnitModel.id_tutor_per_unit == SessionModel.id_tutor_per_unit)\
            .join(SessionSetModel, SessionModel.id_session_set == SessionSetModel.id_session_set)\
            .join(SetPerStudentModel, SessionSetModel.id_session_set == SetPerStudentModel.id_session_set)\
            .filter(
            and_(
                SessionSetModel.id_tutorship == id_tutorship,
                SetPerStudentModel.id_student == id_student,
                SessionModel.state == 'Atendido'
            )
        ).all()
        if rows_tutors:
            return [rt.to_entity() for rt in rows_tutors]
        else:
            return []

    @catch_repo_errors
    def get_tutors_of_tutorship(self, id_tutorship):
        rows_tutor = self.session.query(TutorModel)\
            .join(TutorPerUnitModel, TutorModel.id_tutor == TutorPerUnitModel.id_tutor)\
            .join(TutorPerTutorshipModel, TutorPerUnitModel.id_tutor_per_unit == TutorPerTutorshipModel.id_tutor_per_unit)\
            .filter(
            TutorPerTutorshipModel.id_tutorship == id_tutorship
        ).all()
        if rows_tutor:
            return [rt.to_entity() for rt in rows_tutor]
        else:
            return []

    @catch_repo_errors
    def list_tutors_individual_tutorship_by_student(self, tutorship, tutors_of_student, tutors_of_tutorship):
        if tutors_of_student:
            is_assigned = tutorship.is_tutor_assigned
            if is_assigned:
                return tutors_of_student
            else:
                is_fixed = tutorship.is_tutor_permanent
                if is_fixed:
                    return [tutors_of_student[0]]
                else:
                    if tutors_of_tutorship:
                        return tutors_of_tutorship
                    else:
                        return []
        else:
            is_assigned = tutorship.is_tutor_assigned
            if is_assigned:
                return []
            else:
                if tutors_of_tutorship:
                    return tutors_of_tutorship
                else:
                    return []

    @catch_repo_errors
    def delete_tutor_individual_tutorship(self, id_tutorship, id_tutor, id_unit):
        row_tutor_per_unit = self.session.query(TutorPerUnitModel).filter(
            and_(
                TutorPerUnitModel.id_tutor == id_tutor,
                TutorPerUnitModel.id_unit == id_unit
            )
        ).one_or_none()
        if row_tutor_per_unit:
            row_session_count = self.session.query(SessionModel)\
                .join(SessionSetModel, SessionModel.id_session_set == SessionSetModel.id_session_set)\
                .filter(
                and_(
                    SessionModel.id_tutor_per_unit == row_tutor_per_unit.id_tutor_per_unit,
                    SessionSetModel.id_tutorship == id_tutorship)
            ).count()
            if row_session_count != constants.ZERO:
                return False
            else:
                self.session.query(TutorPerTutorshipModel).filter(
                    and_(
                        TutorPerTutorshipModel.id_tutor_per_unit == row_tutor_per_unit.id_tutor_per_unit,
                        TutorPerTutorshipModel.id_tutorship == id_tutorship
                    )
                    ).delete()
                self.session.commit()
                return True
        return False

    @catch_repo_errors
    def register_moment_tutorship_in_unit(self, id_unit):
        tutorship = TutorshipModel(
            name=constants.TUTORSHIP_MOMENT_NAME,
            description=constants.TUTORSHIP_MOMENT_DESCRIPTION,
            sessions_number=constants.TUTORSHIP_MOMENT_SESSIONS_NUMBER,
            is_individual=constants.TUTORSHIP_MOMENT_IS_INDIVIDUAL,
            is_tutor_permanent=constants.TUTORSHIP_MOMENT_IS_TUTOR_PERMANENT,
            is_obligatory=constants.TUTORSHIP_MOMENT_IS_OBLIGATORY,
            is_tutor_assigned=constants.TUTORSHIP_MOMENT_IS_TUTOR_ASSIGNED,
            id_unit=id_unit
        )
        self.session.add(tutorship)
        self.session.commit()
        self.session.refresh(tutorship)
        return tutorship.to_entity()
