from src.helpers import constants


class User:

    def __init__(self, idx=None, names=None, father_lastname=None, mother_lastname=None, email=None,
                 phone_number=None, code=None, password=None, is_activate=None, picture_url=None):
        self.idx = idx

        self.names = names
        self.mother_lastname = mother_lastname
        self.father_lastname = father_lastname

        self.email = email
        self.phone_number = phone_number
        self.code = code
        self.password = password
        self.is_activate = is_activate
        self.picture_url = picture_url

    def fullname(self, str_format=constants.FULLNAMES_FORMAT):
        """ method that returns the user fullname (names, father and mother's lastnames).
            The first parameter should have the following fields:
            - names: to put the user names
            - father: to put the father lastname
            - mother: to put the mother lastname

            Examples:
                str_format = "{names} {father} {mother}"
                str_format = "{father} {mother}, {names}"

            By default, the current format used can be found in constants.FULLNAMES_FORMAT
        """
        return str_format.format(
            names=self.names,
            father=self.father_lastname,
            mother=self.mother_lastname)
