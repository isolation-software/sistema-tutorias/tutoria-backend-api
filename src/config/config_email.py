import contextlib
import email.message
import smtplib

from src.config import LoggerFactory
from src.config.config_general import EmailContentFiles, EnvironConfig


class _EmailContent:

    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, hostname, email_support):
        self._hostname = hostname
        self._email_support = email_support
        self._welcome                  = self._get_content(EmailContentFiles.PATH_WELCOME_CONFIRM)
        self._satisfaction             = self._get_content(EmailContentFiles.PATH_SATISFACTION)
        self._not_found                = self._get_content(EmailContentFiles.PATH_NOT_FOUND)
        self._pending_appointment      = self._get_content(EmailContentFiles.PATH_PENDING_APPOINTMENT)
        self._cancel_appointment       = self._get_content(EmailContentFiles.PATH_CANCEL_APPOINTMENT)
        self._cancel_appointment_tutor = self._get_content(EmailContentFiles.PATH_CANCEL_APPOINTMENT_TUTOR)
        self._modify_appointment       = self._get_content(EmailContentFiles.PATH_MODIFY_APPOINTMENT)

    @classmethod
    def _get_content(cls, path):
        with open(path, mode='r', encoding='UTF-8') as content_file:
            return content_file.read()

    def welcome(self, username='', token=''):
        return self._welcome\
            .replace('{{username}}', username)\
            .replace('{{frontend_hostname}}', self._hostname)\
            .replace('{{token}}', token)

    def not_found(self, email_not_found=''):
        # reemplazo de {{email}} dos veces, revisar html para mayor entendimiento
        return self._not_found\
            .replace('{{email}}', email_not_found, 2)\
            .replace('{{email_support}}', self._email_support)

    def pending_appointment(self, student_name, tutorship_name, tutor_name, session_date, unit_name):
        return self._pending_appointment\
            .replace('{{student_name}}', student_name)\
            .replace('{{tutorship_name}}', tutorship_name)\
            .replace('{{tutor_name}}', tutor_name)\
            .replace('{{session_date}}', session_date)\
            .replace('{{unit_name}}', unit_name)

    def satisfaction(self, student_name, tutorship_name, tutor_name, session_date, unit_name, token):
        return self._satisfaction\
            .replace('{{student_name}}', student_name)\
            .replace('{{tutorship_name}}', tutorship_name)\
            .replace('{{tutor_name}}', tutor_name)\
            .replace('{{session_date}}', session_date)\
            .replace('{{unit_name}}', unit_name)\
            .replace('{{frontend_hostname}}', self._hostname)\
            .replace('{{token}}', token)

    def cancel_appointment(self, student_name, tutorship_name, tutor_name, session_date, unit_name):
        return self._cancel_appointment\
            .replace('{{student_name}}', student_name)\
            .replace('{{tutorship_name}}', tutorship_name) \
            .replace('{{tutor_name}}', tutor_name) \
            .replace('{{session_date}}', session_date)\
            .replace('{{unit_name}}', unit_name)

    def cancel_appointment_tutor(self, tutorship_name, tutor_name, session_date, unit_name):
        return self._cancel_appointment_tutor\
            .replace('{{tutorship_name}}', tutorship_name) \
            .replace('{{tutor_name}}', tutor_name) \
            .replace('{{session_date}}', session_date)\
            .replace('{{unit_name}}', unit_name)

    def modify_appointment(self, student_name, tutorship_name, tutor_name, session_date, unit_name):
        return self._modify_appointment\
            .replace('{{student_name}}', student_name)\
            .replace('{{tutorship_name}}', tutorship_name)\
            .replace('{{tutor_name}}', tutor_name)\
            .replace('{{session_date}}', session_date)\
            .replace('{{unit_name}}', unit_name)


class EmailFactory:
    logger  = LoggerFactory.get_logger(__name__)
    content = None

    available    = None
    _username    = None
    _credentials = None
    _serverport  = None
    _serverhost  = None

    @classmethod
    def config(cls):
        cls.logger.info('Iniciando configuración de envío de correos')
        config = EnvironConfig.EMAIL_CONFIG
        if not config:
            cls.logger.error('El archivo de configuración de correos no ha sido cargado correctamente')
        cls._username    = config['EMAIL_ADDRESS']
        cls._credentials = config['EMAIL_CREDENTIALS']
        cls._serverport  = config['EMAIL_PORT']
        cls._serverhost  = config['EMAIL_HOST']
        cls.available    = config['EMAIL_AVAILABLE'].lower() == 'true'
        cls.content = _EmailContent(hostname=config['EMAIL_FRONTEND_HOSTNAME'], email_support=config['EMAIL_SUPPORT'])
        cls.logger.debug(f'Correo registrado: {cls._username}')
        cls.logger.info('Correo y credenciales registrados exitosamente')
        if not cls.available:
            cls.logger.warn('El envío de correos se encuentra inhabilitado, revise las configuraciones')
        else:
            cls.logger.info('El envío de correos se encuentra habilitado')

    @classmethod
    @contextlib.contextmanager
    def get_connection(cls):
        server = smtplib.SMTP(host=cls._serverhost, port=cls._serverport)
        try:
            server.starttls()
            server.login(user=cls._username, password=cls._credentials)
            yield server
        finally:
            server.quit()

    @classmethod
    def prepare_message(cls, subject, content):
        msg = email.message.Message()
        msg['Subject'] = subject
        msg.add_header('Content-Type', 'text/html')
        msg.set_payload(content)
        return msg.as_string().encode('utf-8')

    @classmethod
    def get_username(cls):
        return cls._username


EmailFactory.config()
