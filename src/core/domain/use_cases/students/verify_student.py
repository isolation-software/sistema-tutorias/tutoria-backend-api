from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response


class VerifyStudentUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_student=None):
        super().__init__(errors, req)
        # repos
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        # request data
        self.email = req.email

    @catch_errors
    def execute(self):
        student_verified = self.repo_student.verify_student(self.email)
        self.res = Response(student_verified=student_verified)
