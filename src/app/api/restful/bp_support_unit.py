from flask_restful import Resource

from src.app.auth.handler import jwt_required
from src.controllers.support_unit.controller_support_unit import SupportUnitController
from src.helpers import constants


class SupportUnitResource(Resource):
    @jwt_required
    def get(self):
        response = SupportUnitController.list_support_units()
        if response.status is constants.OK:
            return response.to_dict(), constants.HTTP_OK
        else:
            return response.to_dict(), constants.HTTP_BAD_REQUEST