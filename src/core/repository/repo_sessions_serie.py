
import uuid
from sqlalchemy.orm.exc import MultipleResultsFound

from sqlalchemy import and_, or_

from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import SessionSetModel, SetPerStudentModel, SessionModel, StudentModel, \
    UserModel
from src.core.repository.database.models import TutorshipModel
from src.core.domain.entities import SessionsSerie
from src.core.repository.errors import MultipleResultsError
from src.core.repository.errors.handler import catch_repo_errors
from src.helpers import constants


class SessionsSerieRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def register_serie_without_sessions(self, tutorship, student):
        row_set_session = SessionSetModel(id_tutorship=tutorship.idx)
        self.session.add(row_set_session)
        self.session.commit()
        self.session.refresh(row_set_session)

        row_set_per_student = SetPerStudentModel(id_session_set=row_set_session.id_session_set, id_student=student.idx)
        self.session.add(row_set_per_student)
        self.session.commit()

        return SessionsSerie(idx=row_set_session.id_session_set)

    @catch_repo_errors
    def read_or_make_series(self, students, tutorship):
        try:
            ids_session_set = self.session.query(SetPerStudentModel.id_session_set)\
                .join(SessionSetModel, SessionSetModel.id_session_set == SetPerStudentModel.id_session_set)\
                .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship)\
                .filter(and_(
                    SetPerStudentModel.id_student.in_(students),
                    TutorshipModel.id_tutorship == tutorship.idx
            )).all()
            row_set_session = self.session.query(SessionSetModel)\
                .filter(SessionSetModel.id_session_set.in_(ids_session_set)).one_or_none()
        except MultipleResultsFound as e:
            self.logger.error(exc_info=True)
            raise MultipleResultsError(model=SessionSetModel, cause=e)

        if row_set_session:
            return row_set_session.to_entity()
        else:
            row_set_session = SessionSetModel(id_tutorship=tutorship.idx)
            self.session.add(row_set_session)
            self.session.commit()
            self.session.refresh(row_set_session)

            rows_set_per_student = [SetPerStudentModel(id_session_set=row_set_session.id_session_set,
                                                       id_student=student) for student in students]

            if len(students) > 1:
                self.session.bulk_save_objects(rows_set_per_student)
            else:
                self.session.add(rows_set_per_student[constants.FIRST])

            self.session.commit()

        return SessionsSerie(idx=row_set_session.id_session_set)

    @catch_repo_errors
    def register_series_per_student(self, tutorship, student_ids):
        sets_rows = [SessionSetModel(id_tutorship=tutorship.idx) for _ in range(len(student_ids))]
        self.session.bulk_save_objects(sets_rows)
        self.session.commit()
        sets_rows = self.session \
            .query(SessionSetModel) \
            .filter(SessionSetModel.id_tutorship == tutorship.idx) \
            .all()
        set_per_student_rows = [SetPerStudentModel(
            id_session_set=set_row.id_session_set,
            id_student=student_id)
            for set_row, student_id in zip(sets_rows, student_ids)]
        self.session.bulk_save_objects(set_per_student_rows)
        self.session.commit()

    @catch_repo_errors
    def get_serie_of_tutorship(self, id_tutorship, id_student):
        set_row = self.session.query(SessionSetModel). \
            join(SetPerStudentModel, SessionSetModel.id_session_set ==
                 SetPerStudentModel.id_session_set).filter(
            and_(
                SetPerStudentModel.id_student == id_student,
                SessionSetModel.id_tutorship == id_tutorship
            )
        ).one_or_none()
        return SessionsSerie(idx=set_row.id_session_set), set_row.tutorship.id_unit

    @catch_repo_errors
    def get_series_with_session_by_tutorship_for_students(self, id_tutorship):
        sessions_set_rows = self.session \
            .query(SessionSetModel) \
            .filter(SessionSetModel.id_tutorship == id_tutorship) \
            .order_by(SessionSetModel.id_session_set.asc()) \
            .all()
        session_rows = self.session \
            .query(SessionModel) \
            .filter(SessionModel.id_session_set.in_([row.id_session_set for row in sessions_set_rows])) \
            .order_by(SessionModel.id_session_set.asc(), SessionModel.id_session.asc()) \
            .all()
        sessions_sets = []
        for session_row, sessions_set_row in zip(session_rows, sessions_set_rows):
            sessions_set = sessions_set_row.to_entity()
            sessions_set.sessions.append(session_row.to_entity())
            sessions_sets.append(sessions_set)
        return sessions_sets

    @catch_repo_errors
    def register_series_per_additional_student(self, id_tutorship, student_ids):
        uuids_rows = [uuid.uuid4() for _ in range(len(student_ids))]
        sets_rows = [SessionSetModel(id_session_set=id_session_set, id_tutorship=id_tutorship) for id_session_set in
                     uuids_rows]
        self.session.bulk_save_objects(sets_rows)
        self.session.commit()
        set_per_student_rows = [SetPerStudentModel(
            id_session_set=set_row.id_session_set,
            id_student=student_id)
            for set_row, student_id in zip(sets_rows, student_ids)]
        self.session.bulk_save_objects(set_per_student_rows)
        self.session.commit()

    @catch_repo_errors
    def list_students_individual_tutorship(self, id_tutorship, unit, search, limit, filter, children_units):
        search = "%{}%".format(search)
        rows_student = self.session.query(StudentModel) \
            .join(UserModel, StudentModel.id_user == UserModel.id_user)\
            .join(SetPerStudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(SessionSetModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .filter(
            and_(
                or_(
                    UserModel.names.ilike(search),
                    UserModel.father_lastname.ilike(search),
                    UserModel.mother_lastname.ilike(search),
                    UserModel.code.ilike(search)
                ),
                SessionSetModel.id_tutorship == id_tutorship
            )
            ).all()
        if filter == "all":
            if rows_student:
                return [row.to_entity() for row in rows_student]
            else:
                return None
        else:
            self.logger.debug(f"Repo - iniciando la lista de alumnos que no estan en la tutoria")
            self.logger.debug(f"Repo - buscando en la unidad {unit.father_unit.idx}")
            ids_children_units = [children_unit.idx for children_unit in children_units ]
            ids_students = [row.id_student for row in rows_student]
            rows_notin_tutorship = self.session.query(StudentModel)\
                .join(UserModel, StudentModel.id_user == UserModel.id_user)\
                .filter(
                and_(
                    or_(
                        UserModel.names.ilike(search),
                        UserModel.father_lastname.ilike(search),
                        UserModel.mother_lastname.ilike(search),
                        UserModel.code.ilike(search)
                    ),
                    or_(
                        StudentModel.id_unit == unit.idx,
                        StudentModel.id_unit.in_(ids_children_units)
                    ),
                    StudentModel.id_student.notin_(ids_students)
                )
            ).limit(limit).all()
            if rows_notin_tutorship:
                return [row.to_entity() for row in rows_notin_tutorship]
            else:
                return None

    @catch_repo_errors
    def delete_student_individual_tutorship(self, id_tutorship, id_student):
        row_session_set = self.session.query(SessionSetModel)\
            .join(SetPerStudentModel, SessionSetModel.id_session_set == SetPerStudentModel.id_session_set)\
            .filter(
            and_(
                SetPerStudentModel.id_student == id_student,
                SessionSetModel.id_tutorship == id_tutorship
            )
        ).first()
        if row_session_set:
            id_session_serie = row_session_set.id_session_set
            row_session_model_count = self.session.query(SessionModel)\
                .filter(
                    SessionModel.id_session_set == id_session_serie
                ).count()
            if row_session_model_count != constants.ZERO:
                return False
            else:
                self.session.query(SetPerStudentModel).filter(SetPerStudentModel.id_session_set == id_session_serie).delete()
                self.session.commit()
                self.session.query(SessionSetModel).filter(SessionSetModel.id_session_set == id_session_serie).delete()
                self.session.commit()
                return True
        else:
            return False
