from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class ListSessionsByStudentAndTutorUseCase(UseCase):
    """ Class that lists all sessions of a student and
    by tutor.
    """

    def __init__(self, errors=None, req=None, repo_session=None):
        super().__init__(errors, req)
        # repos
        self.repo_session = repo_session or self.RepoFactory.SessionRepo()
        # request data
        self.student = req.student
        self.tutor = req.tutor

    @catch_errors
    def execute(self):
        sessions = self.repo_session.get_sessions_by_student_and_tutor(self.student, self.tutor)
        self.res = Response(sessions=sessions)
        self.success = ListSessionsByStudentAndTutorSuccess


class ListSessionsByStudentAndTutorSuccess(Success):
    code = info_success_logic.SESSION_SEARCH_OK_CODE
    message = info_success_logic.SESSION_SEARCH_OK_MESSAGE
