from uuid import uuid4

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from src.core.domain.entities import SessionsSerie
from src.core.repository.database.connection import Base
from src.helpers import constants as cons


class SessionSetModel(Base):
    __tablename__ = cons.TABLE_NAME_SESSION_SET

    id_session_set = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, unique=True, nullable=False)
    id_tutorship = Column(UUID(as_uuid=True), ForeignKey(f"{cons.TABLE_NAME_TUTORSHIP}.{cons.TABLE_ID_TUTORSHIP}"))

    tutorship = relationship("TutorshipModel")

    def to_entity(self):
        return SessionsSerie(
            idx=self.id_session_set,
            tutorship=self.tutorship.to_entity(),
            sessions=[],
            students=[],
            action_plan=[]
        )