from src.core.domain.entities import Tutorship
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterTutorsIndividualTutorshipUseCase(UseCase):
    def __init__(self, errors=None, req=None, repo_tutorship=None):
        super().__init__(errors, req)
        self.repo_tutorship = repo_tutorship or self.RepoFactory.TutorshipRepo()
        self.id_tutorship = req.id_tutorship
        self.tutor_ids = req.tutor_ids
        self.id_unit = req.id_unit

    @catch_errors
    def execute(self):
        tutorship = Tutorship(idx=self.id_tutorship)
        self.repo_tutorship.save_tutors_for_tutorship(self.tutor_ids, tutorship, self.id_unit)
        self.res = Response()
        self.success = RegisterIndividualTutorshipSuccess


class RegisterIndividualTutorshipSuccess(Success):
    code = info_success_logic.REGISTER_TUTORS_INDIVIDUAL_TUTORSHIP_OK_CODE
    message = info_success_logic.REGISTER_TUTORS_INDIVIDUAL_TUTORSHIP_OK_MESSAGE