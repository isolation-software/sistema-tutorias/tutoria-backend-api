from src.config import EmailFactory, LoggerFactory
from src.helpers.formater import convert_to_peru_time


class EmailSender:

    logger = LoggerFactory.get_logger(__name__)
    email_username = EmailFactory.get_username()

    @classmethod
    def send_welcome(cls, user, token):
        """ Para enviar un mensaje de bienvenida y confirmación de correo, es necesario de su correo y nombres completos
            además de un token que nos ayude a identificar que el usuario que confirma es el que recibió el correo:
            - user: objecto entity User
            - token: string de 64 letras generado aleatoriamente y guardado en BD para validar confirmación
        """
        subject = "Sistema de Tutoría - Bienvenido (Confirmación de usuario)"
        content = EmailFactory.content.welcome(username=user.fullname(), token=token)
        cls._send(subject=subject, content=content, to_send=user.email)
        cls.logger.debug(f'token: {token}')
        cls.logger.info(f'Envío de correo "{user.email}" - bienvenida y completar registro')

    @classmethod
    def send_email_not_found(cls, email):
        """ Este mensaje solo debe ser enviado si un usuario que desea registrarse no es encontrado en el sistema.
            - email: correo electrónico del usuario no encontrado
        """
        subject = "Sistema de Tutoría - Correo no encontrado"
        content = EmailFactory.content.not_found(email_not_found=email)
        cls._send(subject=subject, content=content, to_send=email)
        cls.logger.info(f'Envío de correo "{email}" - usuario no encontrado')

    @classmethod
    def send_pending_appointment(cls, student, tutorship, tutor, session):
        """ Este mensaje es enviado cuando se registra exitosamente una cita entre un alumno y un tutor en una
            tutoría, con una fecha en específico y la cita ya se encuentra en el sistema como PENDIENTE.
            - student: se usará student.user.fullname & student.user.email
            - tutorship: se usará tutorship.name
            - session: se usará session.start_time, start_time no puede ser None
            - tutor: se usará tutor.user.fullname & tutor.user.email
        """
        subject = "Sistema de Tutoría - Cita registrada"
        start_time_peru = convert_to_peru_time(datetime_obj=session.start_time, with_hour=True, str_format=True)
        content = EmailFactory.content.pending_appointment(
            student_name=student.user.fullname(),
            tutorship_name=tutorship.name,
            tutor_name=tutor.user.fullname(),
            session_date=start_time_peru,
            unit_name=tutorship.unit.name)
        cls._send(subject=subject, content=content, to_send=student.user.email)
        cls.logger.info(f'Envío de correo "{student.user.email}" - cita pendiente registrada')

    @classmethod
    def send_cancel_appointment(cls, student, tutorship, tutor, session):
        """ Este mensaje es enviado cuando se registra exitosamente una cita entre un alumno y un tutor en una
            tutoría, con una fecha en específico y la cita ya se encuentra en el sistema como PENDIENTE.
            - student: se usará student.user.fullname & student.user.email
            - tutorship: se usará tutorship.name
            - session: se usará session.start_time, start_time no puede ser None
            - tutor: se usará tutor.user.fullname & tutor.user.email
        """
        subject = "Sistema de Tutoría - Cita cancelada"
        start_time_peru = convert_to_peru_time(datetime_obj=session.start_time, with_hour=True, str_format=True)
        content = EmailFactory.content.cancel_appointment(
            student_name=student.user.fullname(),
            tutorship_name=tutorship.name,
            tutor_name=tutor.user.fullname(),
            session_date=start_time_peru,
            unit_name=tutorship.unit.name)
        cls._send(subject=subject, content=content, to_send=student.user.email)
        cls.logger.info(f'Envío de correo "{student.user.email}" - cita cancelada')

    @classmethod
    def send_cancel_appointment_tutor(cls, tutorship, tutor, session):
        """ Este mensaje es enviado cuando se registra exitosamente una cita entre un alumno y un tutor en una
            tutoría, con una fecha en específico y la cita ya se encuentra en el sistema como PENDIENTE.
            - tutorship: se usará tutorship.name
            - session: se usará session.start_time, start_time no puede ser None
            - tutor: se usará tutor.user.fullname & tutor.user.email
        """
        subject = "Sistema de Tutoría - Cita cancelada"
        start_time_peru = convert_to_peru_time(datetime_obj=session.start_time, with_hour=True, str_format=True)
        content = EmailFactory.content.cancel_appointment_tutor(
            tutorship_name=tutorship.name,
            tutor_name=tutor.user.fullname(),
            session_date=start_time_peru,
            unit_name=tutorship.unit.name)
        cls._send(subject=subject, content=content, to_send=tutor.user.email)
        cls.logger.info(f'Envío de correo "{tutor.user.email}" - cita cancelada')

    @classmethod
    def send_satisfaction(cls, student, tutorship, tutor, session, token):
        subject = "Sistema de Tutoría - Encuesta de Satisfacción"
        start_time_peru = convert_to_peru_time(datetime_obj=session.start_time, with_hour=True, str_format=True)
        content = EmailFactory.content.satisfaction(
            student_name=student.user.fullname(),
            tutorship_name=tutorship.name,
            tutor_name=tutor.user.fullname(),
            session_date=start_time_peru,
            unit_name=tutorship.unit.name,
            token=token
        )
        cls._send(subject=subject, content=content, to_send=student.user.email)
        cls.logger.info(f'Envío de correo "{student.user.email}" - cita atendida, encuesta de satisfacción')

    @classmethod
    def _send(cls, subject, content, to_send):
        cls.logger.debug(f'A punto de enviar correo a "{to_send}" con tema "{subject}"')
        message = EmailFactory.prepare_message(subject=subject, content=content)
        if EmailFactory.available:
            with EmailFactory.get_connection() as server:
                server.sendmail(from_addr=cls.email_username, to_addrs=to_send, msg=message)
