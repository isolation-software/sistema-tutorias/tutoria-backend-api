from src.helpers.serializer import id_to_str


class AdministratorSerializer:

    @staticmethod
    def serialize_administrator_id(response):
        administrator = response.administrator
        return {
            "idTutor": id_to_str(administrator.idx),
            "idUsuario": id_to_str(administrator.user.idx),
            "correo": administrator.user.email,
            "codigo": administrator.user.code,
            "nombres": administrator.user.names,
            "apellidoPat": administrator.user.father_lastname,
            "apellidoMat": administrator.user.mother_lastname,
            "telefono": administrator.user.phone_number

        }