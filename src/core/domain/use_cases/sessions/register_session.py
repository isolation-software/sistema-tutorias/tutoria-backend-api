from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.core.repository.email import EmailSender
from src.helpers import info_success_logic


class RegisterSessionUseCase(UseCase):

    def __init__(self, errors=None, req=None, repo_serie=None, repo_session=None, repo_availability_date=None, repo_user=None):
        super().__init__(errors, req)
        # repos
        self.repo_serie = repo_serie or self.RepoFactory.SessionsSerieRepo()
        self.repo_session = repo_session or self.RepoFactory.SessionRepo(self.repo_serie.session)
        self.repo_user = repo_user or self.RepoFactory.UserRepo(self.repo_serie.session)
        self.repo_availability_date = repo_availability_date or self.RepoFactory.\
            AvailabilityDateRepo(self.repo_serie.session)

        # request data
        self.students = req.students
        self.session = req.session
        self.tutorship = req.tutorship
        self.unit = req.unit

    @catch_errors
    def execute(self):
        serie = self.repo_serie.read_or_make_series(self.students, self.tutorship)
        self.session.sessions_serie = serie
        session_saved = self.repo_session.save_session(self.session, self.unit)
        self.session = session_saved
        self.repo_availability_date.book_session_on_schedule(self.session, self.unit)

        self.res = Response(session=session_saved,
                            students=self.students,
                            action_plan=None,
                            tutorship=self.tutorship,
                            unit=self.unit)

        # envio de emails cuando se registra una cita
        students_aux = self.repo_user.list_users(self.students)
        for student in students_aux:
            EmailSender.send_pending_appointment(student=student,
                                                 tutorship=self.session.sessions_serie.tutorship,
                                                 tutor=session_saved.tutor,
                                                 session=session_saved)
        self.success = RegisterSessionSuccess


class RegisterSessionSuccess(Success):
    code = info_success_logic.SESSION_SAVED_OK_CODE
    message = info_success_logic.SESSION_SAVED_OK_MESSAGE
