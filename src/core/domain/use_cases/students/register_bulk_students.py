from src.config import LoggerFactory
from src.core.domain.errors.handler import catch_errors
from src.core.domain.use_cases.common import UseCase, Response, Success
from src.helpers import info_success_logic


class RegisterBulkStudentUseCase(UseCase):
    logger = LoggerFactory.get_logger(__name__)

    def __init__(self, errors=None, req=None, repo_student=None, repo_user=None, repo_unit=None):
        super().__init__(errors, req)
        self.repo_student = repo_student or self.RepoFactory.StudentRepo()
        self.repo_user = repo_user or self.RepoFactory.UserRepo(self.repo_student.session)
        self.repo_unit = repo_unit or self.RepoFactory.UnitRepo(self.repo_student.session)
        self.students = req.students
        self.unit = req.unit

    @catch_errors
    def execute(self):
        unit_children = self.repo_unit.read_children_units_by_father_id(self.unit)
        unit_code = [u.code for u in unit_children]
        unit_id = [u.idx for u in unit_children]
        if not unit_children:
            unit = self.repo_unit.get_unit_by_id(self.unit.idx)
            unit_code.append(unit.code)
            unit_id.append(unit.idx)
        students_register_filter_unit_code = []
        student_not_register_filter_unit_code = []
        for s in self.students:
            if s.unit.code in unit_code:
                s.unit.idx = unit_id[unit_code.index(s.unit.code)]
                students_register_filter_unit_code.append(s)
            else:
                student_not_register_filter_unit_code.append(s)
        users = self.repo_user.get_users()
        id_user_exist = [u.idx for u in users]
        email_user_exists = [u.email for u in users]
        users_register_filter_email = []
        users_not_register_filter_email = []
        for s in students_register_filter_unit_code:
            if s.user.email in email_user_exists:
                s.user.idx = id_user_exist[email_user_exists.index(s.user.email)]
                users_register_filter_email.append(s)
            else:
                users_not_register_filter_email.append(s)
        students_register_filter_user = []
        students_not_register_filter_user = []
        students = self.repo_student.get_students()
        id_students_exists = [s.user.idx for s in students]
        for s in users_register_filter_email:
            if s.user.idx in id_students_exists:
                students_not_register_filter_user.append(s)
            else:
                students_register_filter_user.append(s)
        self.repo_student.save_students_without_user(students_register_filter_user)
        students_register_filter_code = []
        students_not_register_filter_code = []
        students = self.repo_student.get_students()
        code_students_exists = [s.user.code for s in students]
        for s in users_not_register_filter_email:
            if s.user.code in code_students_exists:
                students_not_register_filter_code.append(s)
            else:
                students_register_filter_code.append(s)
        self.repo_student.save_students_with_user(students_register_filter_code)
        # student_not_register_filter_unit_code no pertenece a la unidad ni undiad hijas si tuviera
        # students_not_register_filter_user ya es alumno
        # students_not_register_filter_code codigo ya ingreado
        self.res = Response(students_not_in_unit=student_not_register_filter_unit_code,
                            students_already_exists=students_not_register_filter_user,
                            students_conflict_code=students_not_register_filter_code)
        self.success = RegisterBulkStudentSuccess


class RegisterBulkStudentSuccess(Success):
    code = info_success_logic.REGISTER_BULK_STUDENT_OK_CODE
    message = info_success_logic.REGISTER_BULK_STUDENT_OK_MESSAGE
