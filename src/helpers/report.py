from flask import send_file
from openpyxl.chart import BarChart, Reference, Series
from openpyxl import Workbook
import tempfile

from src.helpers.constants import STATIC

wb = Workbook()
ws = wb.active
#Example
ws.append(["Tutoria","Estado"])
ws.append(["Tutoria al momento","Aceptado"])
ws.append(["Tutoria al momento","Aceptado"])
ws.append(["Tutoria al momento","Aceptado"])

values = Reference(ws, min_col=2, min_row=2, max_col=2, max_row=10)
chart = BarChart()
chart.add_data(values)
ws.add_chart(chart, "K02")

with tempfile.NamedTemporaryFile() as tmp:
    wb.save(tmp.name)
    tmp.seek(0)
    stream = tmp.read()

send_file(stream)

