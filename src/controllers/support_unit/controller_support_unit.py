from src.controllers.common import Controller
from src.controllers.support_unit.serializer_support_unit import SupportUnitSerializer
from src.core.domain.use_cases.support_unit import ListSupportUnitsUseCase


class SupportUnitController(Controller):

    list_support_units_uc = ListSupportUnitsUseCase
    support_unit_serializer = SupportUnitSerializer

    @classmethod
    def list_support_units(cls):

        return cls.build_response(
            use_case=cls.list_support_units_uc,
            req=None,
            serialize_response=cls.support_unit_serializer.serialize_list_support_units_response
        )
