import uuid

from sqlalchemy import or_, and_
from sqlalchemy.exc import IntegrityError

from src.config import LoggerFactory
from src.core.repository.common import Repository
from src.core.repository.database.models import UserModel, StudentModel, SetPerStudentModel, SessionSetModel, \
    TutorshipModel, UnitModel
from src.core.repository.errors.handler import catch_repo_errors
from src.core.repository.errors.sqla_errors import UserIntegrityError
from src.helpers import constants
from src.helpers.formater import no_double_space


class StudentRepo(Repository):
    logger = LoggerFactory.get_logger(__name__)

    @catch_repo_errors
    def list_students_of_units(self, units, search=constants.EMPTY_STRING, limit=constants.DEFAULT_LIMIT):
        search = "%{}%".format(search)
        rows = self.session.query(StudentModel) \
            .join(UserModel, UserModel.id_user == StudentModel.id_user) \
            .filter(StudentModel.id_unit.in_([u.idx for u in units])) \
            .filter(or_(UserModel.names.ilike(search),
                        UserModel.code.ilike(search))) \
            .order_by(UserModel.names.asc(), UserModel.code.asc()) \
            .limit(limit) \
            .all()
        return [s.to_entity() for s in rows]

    @catch_repo_errors
    def save_student_with_user(self, student):
        row_student = StudentModel(id_unit=student.unit.idx)
        row_user = UserModel(
            email=student.user.email,
            code=student.user.code,
            names=student.user.names,
            father_lastname=student.user.father_lastname,
            mother_lastname=student.user.mother_lastname
        )

        try:
            self.session.add(row_user)
            self.session.flush()
        except IntegrityError as e:
            self.session.rollback()
            param = ''
            value = ''
            existing_email = self.session.query(UserModel).filter(UserModel.email == row_user.email).one_or_none()
            if not existing_email:
                existing_code = self.session.query(UserModel).filter(UserModel.code == row_user.code).one_or_none()
                if existing_code:
                    param = "código"
                    value = row_user.code
            else:
                param = "correo"
                value = row_user.email

            raise UserIntegrityError(param, value, e)
        else:
            self.session.commit()
            self.session.refresh(row_user)

            row_student.id_user = row_user.id_user
            self.session.add(row_student)
            self.session.commit()
            self.close()

    @catch_repo_errors
    def save_student_without_user(self, student):
        row_user = self.session.query(UserModel).filter(UserModel.email == student.user.email).one()
        row_student = StudentModel(id_unit=student.unit.idx, id_user=row_user.id_user)
        self.session.add(row_student)
        self.session.commit()
        self.session.close()

    @catch_repo_errors
    def read_last_saved_students(self, search="", limit=10):
        search = "%{}%".format(search)
        row_students = self.session.query(StudentModel).join(UserModel).order_by(UserModel.register_date.desc()).filter(
            or_(
                UserModel.names.ilike(search),
                UserModel.father_lastname.ilike(search),
                UserModel.mother_lastname.ilike(search),
                UserModel.code.ilike(search)
            )
        ).limit(limit).all()
        self.logger.debug(f"list students: {len(row_students)} {row_students}")
        return [sr.to_entity() for sr in row_students]

    @catch_repo_errors
    def list_students_by_unit(self, unit, search=""):
        self.logger.debug("Iniciando con la búsqueda en BD de alumnos por unidad en repositorio")
        search = "%{}%".format(search)
        row_students = self.session.query(StudentModel).join(UserModel).filter(
            and_(
                StudentModel.id_unit == unit.idx,
                or_(
                    UserModel.code.ilike(search),
                    UserModel.email.ilike(search),
                    UserModel.names.ilike(search)
                )
            )
        ).all()
        self.logger.debug("Terminando con la búsqueda en BD de alumnos por unidad en repositorio")
        return [sr.to_entity() for sr in row_students]

    @catch_repo_errors
    def get_names_by_session_ids(self, sessions):
        id_session_sets = [session.sessions_serie.idx for session in sessions]
        row_students = self.session \
            .query(StudentModel) \
            .join(SetPerStudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(SessionSetModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .filter(SessionSetModel.id_session_set.in_(id_session_sets)) \
            .order_by(SessionSetModel.id_session_set.asc()) \
            .all()
        self.logger.debug(f"Repo - obteniendo alumnos: {row_students}")
        return [sr.to_entity() for sr in row_students]

    @catch_repo_errors
    def read_student_by_session_serie(self, id_session_set):
        self.logger.debug(f"Repo - iniciando búsqueda de alumno por serie de sesión: id => {id_session_set}")
        student_row = self.session \
            .query(StudentModel) \
            .join(SetPerStudentModel, StudentModel.id_student == SetPerStudentModel.id_student) \
            .join(SessionSetModel, SetPerStudentModel.id_session_set == SessionSetModel.id_session_set) \
            .filter(SessionSetModel.id_session_set == id_session_set).first()
        # TODO(JGT): Mejorar en método first por one_or_none y manejar los posibles casos de error
        #   al encontrar más de un alumno en dicha serie de sesión (error que se debe controlar)
        #   o no se encuentre ninguno (error por parte del usuario, id no correcto)
        return student_row.to_entity()

    @catch_repo_errors
    def get_students_by_tutorship(self, id_tutorship):
        students_rows = self.session.query(StudentModel) \
            .join(SetPerStudentModel, SetPerStudentModel.id_student == StudentModel.id_student) \
            .join(SessionSetModel, SessionSetModel.id_session_set == SetPerStudentModel.id_session_set) \
            .join(TutorshipModel, TutorshipModel.id_tutorship == SessionSetModel.id_tutorship) \
            .filter(TutorshipModel.id_tutorship == id_tutorship) \
            .order_by(SessionSetModel.id_session_set.asc()) \
            .all()
        students = [sr.to_entity() for sr in students_rows]
        return len(students), students

    @catch_repo_errors
    def update_student_unit(self, id_student, unit):
        self.logger.debug(f"Repo - iniciando cambio de unidad de alumno con id {id_student}")
        student_row = self.session.query(StudentModel).filter(StudentModel.id_student == id_student).one_or_none()
        student_row.id_unit = unit.idx
        self.session.commit()
        self.logger.debug(f"Repo - Terminando cambio de unidad de alumno con id {id_student}")

        return student_row.to_entity()

    @catch_repo_errors
    def read_data_student_id(self, id_student):
        row_student = self.session.query(StudentModel).join(UserModel).filter(
            StudentModel.id_student == id_student
        ).first()
        return row_student.to_entity()

    @catch_repo_errors
    def read_students_per_unit(self, id_unit, search="", limit=10):
        search = "%{}%".format(search)
        row_students = self.session.query(StudentModel).join(UserModel).order_by(UserModel.names.desc()).filter(
            and_(
                or_(
                    UserModel.names.ilike(search),
                    UserModel.father_lastname.ilike(search),
                    UserModel.mother_lastname.ilike(search),
                    UserModel.code.ilike(search)
                ),
                StudentModel.id_unit == id_unit
            )

        ).limit(limit).all()
        self.logger.debug(f"list students: {len(row_students)} {row_students}")
        return [sr.to_entity() for sr in row_students]

    @catch_repo_errors
    def read_students_child(self, id_unit, search="", limit=10):
        search = "%{}%".format(search)
        row_units = self.session.query(UnitModel.id_unit).filter(UnitModel.id_father_unit == id_unit)
        row_students = self.session.query(StudentModel) \
            .join(UserModel, UserModel.id_user == StudentModel.id_user).filter(
            and_(
                StudentModel.id_unit.in_(row_units),
                or_(
                    UserModel.names.like(search),
                    UserModel.father_lastname.like(search),
                    UserModel.mother_lastname.like(search),
                    UserModel.code.like(search)
                )
            )

        ).all()
        self.logger.debug(f"list students: {len(row_students)} {row_students}")
        return [sr.to_entity() for sr in row_students]

    @catch_repo_errors
    def read_students_from_unit_and_unit_children(self, id_unit, search=constants.EMPTY_STRING,
                                                  limit=constants.DEFAULT_LIMIT):
        search = "%{}%".format(no_double_space(search))
        row_students = self.session.query(StudentModel) \
            .join(UnitModel, UnitModel.id_unit == StudentModel.id_unit) \
            .join(UserModel, UserModel.id_user == StudentModel.id_user) \
            .filter(and_(
                or_(UnitModel.id_unit == id_unit,
                    UnitModel.id_father_unit == id_unit),
                or_(
                    UserModel.names.ilike(search),
                    UserModel.father_lastname.ilike(search),
                    UserModel.mother_lastname.ilike(search),
                    UserModel.code.like(search)
                ))) \
            .order_by(UserModel.register_date.desc(), UserModel.names.asc(), UserModel.code.desc())\
            .limit(limit)\
            .all()
        return [row.to_entity() for row in row_students]

    @catch_repo_errors
    def get_students(self):
        students = self.session.query(StudentModel).all()
        return [student.to_entity() for student in students]

    @catch_repo_errors
    def save_students_without_user(self, students):
        row_students = [StudentModel(id_unit=student.unit.idx, id_user=student.user.idx) for student in students]
        self.session.bulk_save_objects(row_students)
        self.session.commit()

    @catch_repo_errors
    def save_students_with_user(self, students):
        uuids_rows = [uuid.uuid4() for _ in range(len(students))]
        i = 0
        for s in students:
            s.user.idx = uuids_rows[i]
            i += 1
        users = [s.user for s in students]
        units = [s.unit for s in students]
        user_rows = [UserModel(
            id_user=user.idx,
            email=user.email,
            code=user.code,
            names=user.names,
            father_lastname=user.father_lastname,
            mother_lastname=user.mother_lastname,
            phone_number=user.phone_number
        ) for user in users]
        self.session.bulk_save_objects(user_rows)
        self.session.commit()
        students_rows = [StudentModel(
            id_user=user.idx,
            id_unit=unit.idx)
            for user, unit in zip(users, units)]
        self.session.bulk_save_objects(students_rows)
        self.session.commit()
