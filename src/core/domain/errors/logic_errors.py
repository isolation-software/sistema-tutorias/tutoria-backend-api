from src.helpers import info_exception_logic


class LogicError(Exception):

    def __init__(self, code, message):
        self.code = code
        self.message = message
        super().__init__(self.message)


class StudentAlreadyRegisteredError(LogicError):
    def __init__(self, student):
        self.code = info_exception_logic.STUDENT_CANNOT_REGISTER_ALREADY_EXISTS_CODE
        self.message = info_exception_logic.STUDENT_CANNOT_REGISTER_ALREADY_EXISTS_MESSAGE.format(
            names=student.user.names,
            lastnames=student.user.father_lastname,
            email=student.user.email)
        super().__init__(self.code, self.message)


class CoordinatorAlreadyRegisteredError(LogicError):
    def __init__(self, coordinator):
        self.code = info_exception_logic.COORDINATOR_CANNOT_REGISTER_ALREADY_EXISTS_CODE
        self.message = info_exception_logic.COORDINATOR_CANNOT_REGISTER_ALREADY_EXISTS_MESSAGE.format(
            names=coordinator.user.names,
            lastnames=coordinator.user.father_lastname,
            email=coordinator.user.email)
        super().__init__(self.code, self.message)


class UserNotFoundInSystemError(LogicError):
    def __init__(self, email):
        self.code = info_exception_logic.USER_NOT_FOUND_IN_SYSTEM_EXCEPTION_CODE
        self.message = info_exception_logic.USER_NOT_FOUND_IN_SYSTEM_EXCEPTION_MESSAGE.format(
            email=email)
        super().__init__(self.code, self.message)


class TutorAlreadyRegisteredError(LogicError):
    def __init__(self, tutor):
        self.code = info_exception_logic.TUTOR_CANNOT_REGISTER_ALREADY_EXISTS_CODE
        self.message = info_exception_logic.TUTOR_CANNOT_REGISTER_ALREADY_EXISTS_MESSAGE.format(
            names=tutor.user.names,
            lastnames=tutor.user.father_lastname,
            email=tutor.user.email)
        super().__init__(self.code, self.message)


class RegisterIndividualWhenGroupError(LogicError):
    def __init__(self):
        self.code = info_exception_logic.REGISTER_INDIVIDUAL_TUTORSHIP_WHEN_GROUP_ERROR
        self.message = info_exception_logic.REGISTER_INDIVIDUAL_TUTORSHIP_WHEN_GROUP_MESSAGE
        super().__init__(self.code, self.message)


class TimeAlreadyTakenError(LogicError):
    def __init__(self, cause=None):
        self.cause = cause
        self.code = info_exception_logic.TIME_ALREADY_TAKEN_ERROR
        self.message = info_exception_logic.TIME_ALREADY_TAKEN_MESSAGE
        super().__init__(self.code, self.message)


class TokenRegisterInvalid(LogicError):
    def __init__(self):
        self.code = info_exception_logic.REGISTER_TOKEN_INVALID_CODE
        self.message = info_exception_logic.REGISTER_TOKEN_INVALID_MESSAGE
        super().__init__(self.code, self.message)


class TokenRegisterExpired(LogicError):
    def __init__(self):
        self.code = info_exception_logic.REGISTER_TOKEN_EXPIRED_CODE
        self.message = info_exception_logic.REGISTER_TOKEN_EXPIRED_MESSAGE
        super().__init__(self.code, self.message)


class UnauthenticatedUserAccount(LogicError):
    def __init__(self, email):
        self.code = info_exception_logic.UNAUTHENTICATED_USER_ACCOUNT_CODE
        self.message = info_exception_logic.UNAUTHENTICATED_USER_ACCOUNT_MESSAGE.format(
            email=email)
        super().__init__(self.code, self.message)


class IncorrectPassword(LogicError):
    def __init__(self):
        self.code = info_exception_logic.INCORRECT_PASSWORD_CODE
        self.message = info_exception_logic.INCORRECT_PASSWORD_MESSAGE
        super().__init__(self.code, self.message)


class StudentNotDeleteInSystemError(LogicError):
    def __init__(self):
        self.code = info_exception_logic.DELETE_STUDENT_INDIVIDUAL_TUTORSHIP_OK_CODE
        self.message = info_exception_logic.DELETE_STUDENT_INDIVIDUAL_TUTORSHIP_OK_MESSAGE
        super().__init__(self.code, self.message)


class TutorNotDeleteInSystemError(LogicError):
    def __init__(self):
        self.code = info_exception_logic.DELETE_TUTOR_INDIVIDUAL_TUTORSHIP_OK_CODE
        self.message = info_exception_logic.DELETE_TUTOR_INDIVIDUAL_TUTORSHIP_OK_MESSAGE
        super().__init__(self.code, self.message)


class IndicatorAlreadyRegister(LogicError):
    def __init__(self):
        self.code = info_exception_logic.REGISTER_INDICATOR_OK_CODE
        self.message = info_exception_logic.REGISTER_INDICATOR_OK_MESSAGE
        super().__init__(self.code, self.message)


class GoogleAuthenticationAccountError(LogicError):
    def __init__(self, cause=None):
        self.cause = cause
        self.code = info_exception_logic.GENERIC_LOGIC_EXCEPTION_CODE
        self.message = info_exception_logic.GENERIC_LOGIC_EXCEPTION_MESSAGE
        super().__init__(self.code, self.message)


class GenericLogicError(LogicError):
    def __init__(self, cause=None):
        self.cause = cause
        self.code = info_exception_logic.GENERIC_LOGIC_EXCEPTION_CODE
        self.message = info_exception_logic.GENERIC_LOGIC_EXCEPTION_MESSAGE
        super().__init__(self.code, self.message)
